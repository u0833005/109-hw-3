

class player {
    constructor(fn, age, ger, hor) {
        this.fn = fn;
        this.age = age;
        this.ger = ger;
        this.hor = hor;
    }
    toString() {
        const promise = new Promise((resolve, reject) => {
            setTimeout(() => {
                if(this.age<18){
                    console.log(`correntdata`);
                    resolve(`${this.fn},${this.age},${this.ger},${this.hor}`);
                }
                else{
                    console.log(`errordata`);
                    reject(`${this.fn},${this.age},${this.ger},${this.hor}`);
                }
            }, 3000);
        });
        promise.then((value) => {
            console.log(`->${value}`);
            // expected output: "foo"
        },(error) => {
            console.log(`->${error}`);
        });
    }
}
let a = new player("leo", 20, "boy", "gray");
let b = new player("shelly", 16, "girl", "black");
b.toString();
a.toString();