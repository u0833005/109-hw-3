let ans = [50];
for(let i=0;i<50;i++) {
    ans[i] = Math.floor(Math.random()*100+1);
}
console.log(ans);
let max = Math.max(...ans);
console.log(`最大值${max}`);

let min = Math.min(...ans);
console.log(`最小值${min}`);

ans.splice(6,1);
console.log(`刪掉第七個元素`);
console.log(ans);

ans.splice(ans.indexOf(max),1);
console.log(`刪掉最大的元素`);
console.log(ans);

let nans = ans.filter(function (x){
    return x % 2 !== 0;
});
console.log(`濾掉偶數元素`);
console.log(nans);

nnans = nans.filter(function (x){
    return x % 7 !== 0;
});
console.log(`濾掉七的倍數元素`);
console.log(nnans);

nnans.sort((a,b) => a - b);
console.log(`排序`);
console.log(nnans);

nnans.splice(0,3);
console.log(`刪掉最小的三個元素`);
console.log(nnans);
