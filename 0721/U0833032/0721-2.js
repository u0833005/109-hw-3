class Player {
    constructor(fullName, age, gender, hairColor) {
        this.fullName = fullName;
        this.age = age;
        this.gender = gender;
        this.hairColor = hairColor;
    }
    toString() {
        const promise1 = new Promise((resolve, reject) => {
            setTimeout(() => {
                if (this.age >= 18) {
                    resolve(`fullName=${this.fullName}, age=${this.age},gender=${this.gender}, hairColor=${this.hairColor}`);
                }else{
                    reject("error");
                }
            }, 3000);
        });    
            promise1.then((value) => {
                console.log(value);
            }, (error) => {
                console.log(error);
            });
    }    
}

    let john = new Player("john", 20, "male", "black");
    let leo = new Player("leo", 20, "male", "black");
    john.toString()
    leo.toString()
