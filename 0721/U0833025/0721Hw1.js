/*使用網頁控制台*/
/*有用的method:
  slice=>複製
  splice(position,n)=>刪除從特定位置起N個
  sort=>排序 */
let array=Array(50);
for(let i=0;i<50;i++){ array[i]=(Math.floor)(Math.random()*100)}
console.log(array);
console.log("排序前:"+array);
//排序-----------------------------------------------------------------------------------------------------------
let sort=array.slice(); //複製
sort.sort((a, b) => a - b);
console.log(sort);
/*for(let j=0; j<50;j++){ //氣泡排序
    for(let i=0;i<50-1-j;i++){ 
        if(sort[i]>sort[i+1]){
            let temp=0; temp=sort[i]; sort[i]=sort[i+1]; sort[i+1]=temp;
        }
    }
}
console.log("排序後:"+sort);*/
//最大值&最小值----------------------------------------------------------------------------------------------------
let maxmin=sort.slice();    //複製排序後
let max=0,min=100;
for(let i=0;i<50;i++){
    if(max<maxmin[i]) max=maxmin[i];    
    if(min>maxmin[i]) min=maxmin[i];
}
console.log("最大值:"+max);
console.log("最小值:"+min);
//刪掉最大值-------------------------------------------------------------------------------------------------------
let removelast = sort.slice();  //複製排序後
let last = removelast.pop();    //排序後最大值在後
console.log("刪掉最大值:"+last);
console.log(removelast);
//刪掉最小3個值----------------------------------------------------------------------------------------------------
let removeItems = sort.slice(); //複製排序後
let Items = removeItems.splice(0, 3);   //排序後最小值在前，刪除前3個
console.log("刪掉最小3個值:"+Items);
console.log(removeItems);
//刪掉第7個--------------------------------------------------------------------------------------------------------
let removeItem7 = sort.slice(); //複製排序後
let Item7 = removeItem7.splice(6, 1);   ////排序後第7個
let removedItem7 = array.slice();   //複製
let Items7 = removedItem7.splice(6, 1);
console.log("(排序後)刪掉第7個:"+Item7);
console.log(removeItem7);
console.log("(未排序)刪掉第7個:"+Items7);
console.log(removedItem7);
//濾掉偶數---------------------------------------------------------------------------------------------------------
let filtereven = sort.slice();  //複製排序後
let even=filtereven.filter((x)=>{return x%2!=0;})   //刪除偶數
console.log("過濾偶數:"+even);
//濾掉7倍數--------------------------------------------------------------------------------------------------------
let filterseven = sort.slice(); //複製排序後
let seven=filterseven.filter((x)=>{return x%7!=0;}) //刪除7倍數
console.log("過濾7倍數:"+seven);
