let myarray=Array();

for(let i=0;i<50;i++)
{
    myarray.push(Math.floor(Math.random()*100)+1);
}
console.log("原陣列為");
console.log(myarray);
console.log(`陣列中最大值為${Math.max(...myarray)}`);
console.log(`陣列中最小值為${Math.min(...myarray)}\n`);
myarray.splice(6,1);
console.log("刪除第7元素後的陣列為");
console.log(myarray);
console.log(`陣列長度為${myarray.length}\n`);
myarray.splice(myarray.indexOf(Math.max(...myarray)),1)
console.log("刪除最大值後的陣列為");
console.log(myarray);
console.log(`陣列長度為${myarray.length}\n`);
myarray=myarray.filter((element)=>
{
    return element%2;
})
console.log("過濾掉偶數後的陣列為");
console.log(myarray);
console.log(`陣列長度為${myarray.length}\n`);
myarray=myarray.filter((element)=>
{
    return element%7;
})
console.log("過濾掉7的倍數後的陣列為");
console.log(myarray);
console.log(`陣列長度為${myarray.length}\n`);
myarray.sort();
console.log("排序後的陣列為");
console.log(myarray);
console.log(`陣列長度為${myarray.length}\n`);
myarray.splice(0,3);
console.log("刪除最小的3個元素後的振列為");
console.log(myarray);
console.log(`陣列長度為${myarray.length}\n`);