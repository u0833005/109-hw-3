class Player
{
    constructor(fullName,age,gender,hairColor)
    {
        this.fullName=fullName;
        this.age=age;
        this.gender=gender;
        this.hairColor=hairColor;
    }
    toString()
    {
        return `Full Name is ${this.fullName} and age is ${this.age}.`;
    }
}
alice=new Player("Alice",56,"female","red");
bob=new Player("Bob",17,"male","blue");
console.log(alice.toString());
console.log(bob.toString());

let player=alice;
mypromise=new Promise((resolve,reject)=>
{
    if(player.gender=="male")
        setTimeout(resolve,3000,player);
    else
        reject("回傳失敗");
})
.then(
    (player)=>
    {
        console.log(player.toString());
    },
    (error)=>
    {
        console.log(error);
    });