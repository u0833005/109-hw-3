const express=require('express');
const jsonServer=require("json-server");

const server=jsonServer.create();
const router=jsonServer.router('todoList.json');
const middlewares=jsonServer.defaults();

port=3000;

server.use(middlewares);
server.use(router);
server.use(express.static('public'));
server.listen(port,()=>
{
    console.log("json server is running");
})