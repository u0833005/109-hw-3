public class P7 {
    @FunctionalInterface
    interface MyCalc{
        void exec(int a, int b);
    }
    public static void main(String[] args) {
        MyCalc calc = new MyCalc() {
            @Override
            public void exec(int a, int b) {
                System.out.println("calc:"+(a+b));
            }
        };
        calc.exec(1,2);

        MyCalc calc2 = (a,b)-> System.out.println("calc2:"+(a+b));
        calc2.exec(2,3);
    }
}
