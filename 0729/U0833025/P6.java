public class P6 {
    static int count = 1;
    static class Ticketing implements Runnable{
        @Override
        public void run(){
            while(count <= 10){
                try {
                    Thread.sleep(1000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                Ticket();
            }
        }
    }
    public static synchronized void Ticket(){
        if(count <= 10){
            System.out.println(Thread.currentThread().getName()+"賣了第"+count+"張票");
            count++;
        }else {
            System.out.println("Sold Out!!!!");
        }
    }
    public static void main(String[] args) {
        Ticketing counter = new Ticketing();

        Thread a = new Thread(counter,String.valueOf("a"));
        a.start();
        Thread b = new Thread(counter,String.valueOf("b"));
        b.start();
        Thread c = new Thread(counter,String.valueOf("c"));
        c.start();
    }

}
