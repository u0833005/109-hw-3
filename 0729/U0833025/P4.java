public class P4 {
        static class Counter{
            private int c;
            public Counter(){
                this.c = 0;
            }
            public void increase(){
                c++;
            }
        }
        static class TH1 extends Thread{
            @Override
            public void run(){
                for(int i = 0; i < 5; i++){
                    synchronized (counter){
                        counter.increase();
                        System.out.println(getName()+":"+counter.c);
                    }
                    try{
                        Thread.sleep(500);  //休息0.5秒
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        }
        static class TH2 extends Thread{
            @Override
            public void run(){
                for(int i = 0; i < 5; i++){
                    synchronized (counter){
                        counter.increase();
                        System.out.println(getName()+":"+counter.c);
                    }
                    try{
                        Thread.sleep(500);
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        }
        static Counter counter = new Counter();
        public static void main(String[] args) {

            TH1 th1 = new TH1();
            TH2 th2 = new TH2();
            th1.start();
            th2.start();

        }
}
