import java.util.Scanner;

public class P5 {
    static class Clock extends Thread{
        public int time;
        public String current = "r";
        public boolean run = true;
        @Override
        public void run(){
            while(run){
                switch (current){
                    case "p":
                        try {
                            sleep(1000);
                        }catch (InterruptedException e){
                            e.printStackTrace();
                        }
                        break;
                    case "s":
                        run = false;
                        break;
                    case "r":
                        try {
                            sleep(1000);
                        }catch (InterruptedException e){
                            e.printStackTrace();
                        }
                        time++;
                        System.out.println(time);
                        break;
                }
            }
        }
    }

    public static void main(String[] args) {
        String input;
        Scanner sc = new Scanner(System.in);

        Clock c = new Clock();
        c.start();

        while (c.run){
            input = sc.nextLine();
            c.current = input;
            if(input.equals("s")){
                c.interrupt();
                break;
            }
        }

    }
}
