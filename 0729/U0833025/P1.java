public class P1 {   //練習1(相加)
    static class T1 extends Thread{

        private int c;
        private int a;
        private int r;

        public T1(int a,int c){
            this.c = c;
            this.a = a;
            this.r = 0;
        }
        @Override
        public void run(){
            for(int i=0; i<c ;i++){
                r+=a;
            }
            System.out.println(getName()+":結束");
        }
        public int getResult(){
            return this.r;
        }
    }
    static class T2 extends Thread{

        private int c;
        private int a;
        private int r;

        public T2(int a,int c){
            this.c = c;
            this.a = a;
            this.r = 0;
        }
        @Override
        public void run(){
            try {
                t1.join();
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            while ( c > 0){
                r+=a;
                c--;
            }
            System.out.println(getName()+":結束");
        }
        public int getResult(){
            return this.r;
        }
    }
    static class Add extends Thread{

        private int a;
        private int b;
        private int r;

        public Add(int a,int b){
            this.a = a;
            this.b = b;
            this.r = 0;
        }
        @Override
        public void run(){

            this.r = this.a + this.b;
        }
        public int getResult(){
            return this.r;
        }
    }
    static T1 t1 = new T1(3,10);
    static T2 t2 = new T2(5,6);
    public static void main(String[] args) throws InterruptedException {
            t1.start();
            t2.start();

            t1.join();
            t2.join();

            Add add = new Add(t1.getResult(),t2.getResult());
            add.start();
            add.join();
            System.out.println("t1:"+t1.getResult()+",t2:"+t2.getResult()+",add:"+add.getResult());
    }
}
