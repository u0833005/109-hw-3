import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class P11 {
    @FunctionalInterface
    interface Change{
        String getValue(String s);
    }
    public static String getValue (String str, Change change){
        return change.getValue(str);
    }
    public static void main(String[] args) {
        String str1 = getValue("hello world",str -> str.toUpperCase());
        System.out.println("轉換為大寫:"+str1);
        String str2 = getValue("hello world",str -> str.substring(1, 4));
        System.out.println("擷取後字串:"+str2);
    }

}
