public class P2 {     //練習2(按值遞增)
    static class Add{
        public int counter;

        public Add(){
            this.counter = 0;
        }

        public void increase(){
            try{
                Thread.sleep(500);
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            counter++;
        }
    }

    public static void main(String[] args) {
            Add add = new Add();

            for(int i = 0; i < 10; i++){
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        synchronized (add){
                            add.increase();
                            System.out.println(Thread.currentThread().getName()+":"+add.counter);
                        }
                    }
                }).start();
            }

    }
}
