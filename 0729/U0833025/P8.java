public class P8 {
        @FunctionalInterface
        interface B{
            void printString(String s);
        }
        interface C{
            double doComputeWork(float x,float y);
        }

        static class lambdaA {
            public lambdaA() {
                B b = this::printOnce;
                b.printString("hi");
            }
            public static void printTwice(String s) {
                System.out.print(s);
                System.out.println(s);
            }

            public void printOnce(String s) {
                System.out.println(s);
            }

            public static void main(String[] args) {
                B b = lambdaA::printTwice;
                b.printString("hello");
                new lambdaA();

                C c = Math::pow;
                b.printString(String.valueOf(c.doComputeWork(2.5f, 2)));
            }


        }
}
