public class P12 {
    private String a;

    static class A extends Thread{

        private int c;
        private int a;
        private int r;

        public A(int a,int c){
            this.c = c;
            this.a = a;
            this.r = 0;
        }
        @Override
        public void run(){
            for(int i=0; i<c ;i++){
                r+=a;
            }
            System.out.println(getName()+":結束");
        }
        public int getResult(){
            return this.r;
        }
    }
    static class B extends Thread{

        private int c;
        private int a;
        private int r;

        public B(int a,int c){
            this.c = c;
            this.a = a;
            this.r = 0;
        }
        @Override
        public void run(){
            try {
                aa.join();
            }catch (InterruptedException e){
                e.printStackTrace();
            }
            while ( c > 0){
                r+=a;
                c--;
            }
            System.out.println(getName()+":結束");
        }
        public int getResult(){
            return this.r;
        }
    }
    @FunctionalInterface
    interface Add{
        int PrintAdd(int a,int b);
    }
    public int PrintAdd(Thread a, Thread b){
        return aa.r + bb.r;
    }
    static A aa = new A(3,10);
    static B bb = new B(5,6);
    public static void main(String[] args) throws InterruptedException {
        aa.start();
        bb.start();

        aa.join();
        bb.join();

        Add result = (int a,int b)->{
            return a + b;
        };
        System.out.println("A:"+aa.getResult()+",B:"+bb.getResult()+",result:"+result.PrintAdd(aa.getResult(),bb.getResult()));
    }
}
