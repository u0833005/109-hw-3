public class P3 {       //練習3(synchronized)

    static class Student{
        private String name;
        private int age;

        public Student(String name, int age){
            this.name = name;
            this.age = age;
        }

        public String getName(){
            return this.name;
        }

        public int getAge(){
            return this.age;
        }
    }

    public static void main(String[] args) {
        Student a = new Student("Tom",15);
        Student b = new Student("May",16);

        Thread x = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (a){
                    System.out.println(a.getName()+"is"+a.getAge()+"years old.");
                    synchronized (b){
                        System.out.println(b.getName()+"is"+b.getAge()+"years old.");
                    }
                }
            }
        });

        Thread y = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (b){
                    System.out.println(b.getName()+"is"+b.getAge()+"years old.");
                    synchronized (a){
                        System.out.println(a.getName()+"is"+a.getAge()+"years old.");
                    }
                }
            }
        });

        x.start();
        y.start();
    }
}
