import java.util.ArrayList;

public class P9 {
    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("1");
        arrayList.add("2");
        arrayList.add("3");
        arrayList.add("4");
        arrayList.add("5");
        arrayList.forEach(n-> System.out.println(n));
        arrayList.stream().filter(s->Integer.valueOf(s) < 3).forEach(s-> System.out.print(s));
        System.out.println();
        arrayList.forEach(System.out::print);
    }
}
