public class Practice {
    static class TestThread extends Thread{
        private int i = 0;
        @Override
        public void run(){
            while(i<10){
                System.out.println(i++);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
    }

    static class AThread implements Runnable{
        @Override
        public void run(){
            int a = 1;
            for(int i = 1; i<=10;i++){
                System.out.println(a);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
    }
    public static void main(String[] args) {
        Thread thread = new Thread(){
            @Override
            public void run(){
                int a = 1;
                for(int i = 1; i<=10;i++){
                    a*=i;
                }
                System.out.println(a);
            }
        };
        thread.start();
        System.out.println("helloworld");

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("hi");
            }
        }).start();

        TestThread t = new TestThread();
        t.start();
    }
}
