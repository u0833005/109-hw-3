public class P2 {

    static class  Add{
        public int counter;

        public Add(){
            counter=0;
        }

    public void  increase(){
        try{
            Thread.sleep(500);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
            counter++;
        }
    }

    public static void main(String[] args) {
        Add add =new  Add();
        for (int i=0;i<10;i++){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    add.increase();
                    System.out.println(Thread.currentThread().getName()+":"+add.counter);
                }
            }).start();
        }

    }
}
