public class P3 {
    static  class Student{
        private String name;
        private int age;

        public  Student(String name,int age){
            this.name=name;
            this.age=age;
        }
        public String getName(){
            return  this.name;
        }
        public int getage(){
            return  this.age;
        }
    }

    public static void main(String[] args) {
        Student a=new Student("Tom",18);
        Student b =new Student("Jerry",19);

        Thread x=new Thread (new Runnable(){
            @Override
            public  void run(){
                synchronized (a){
                    System.out.println(a.getName()+" is "+a.getage()+" years old");

                    synchronized (b){
                        System.out.println(b.getName()+" is "+b.getage()+" years old");
                    }
                }
            }
        });

        Thread y=new Thread (new Runnable(){
            @Override
            public  void run(){
                synchronized (b){
                    System.out.println(b.getName()+" is "+b.getage()+" years old");

                    synchronized (a){
                        System.out.println(a.getName()+" is "+a.getage()+" years old");
                    }
                }
            }
        });
        x.start();
        y.start();
    }
}
