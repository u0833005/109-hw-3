public class p4 {
    static class Ticket
    {
        private int nums;
        public Ticket()
        {
            this.nums=0;
        }
    }
    static class Counter extends Thread
    {
        Ticket myTicket;
        public Counter(Ticket myTicket,String name)
        {
            this.myTicket=myTicket;
            setName(name);
        }
        public void run()
        {
            while(myTicket.nums<100)
            {
                synchronized (this.myTicket)
                {
                    System.out.println(getName() + "賣了第" + (++this.myTicket.nums) + "張票");
                }
                try {
                    sleep(300);
                }
                catch (InterruptedException e)
                {
                    System.out.println(e);
                }
            }
            System.out.println("Sold out!");
        }
    }
    public static void main(String[] args) {
        Ticket myTicket=new Ticket();
        Counter a=new Counter(myTicket,"a");
        Counter b=new Counter(myTicket,"b");
        Counter c=new Counter(myTicket,"c");
        a.start();
        b.start();
        c.start();
    }
}
