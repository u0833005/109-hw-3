public class p2 {

    static class Add {
        public int counter;

        public Add() {
            counter= 0;
        }
        public void increase(){
            try{
                Thread.sleep(500);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            counter++;
        }
    }
    public static void main(String[] args) {
        Add add =new Add();

        for(int i=0;i<100;i++){
            new Thread(new Runnable(){        //匿名類別，Runnable要配run()方法
                @Override
                public void run(){
                    synchronized (add){
                        add.increase();
                        System.out.println(Thread.currentThread().getName()+" : "+add.counter);
                    }
                }
            }).start();
        }
    }
}
