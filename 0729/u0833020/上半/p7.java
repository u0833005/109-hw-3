import java.util.Scanner;

public class p7 {

    static class Timer extends Thread{
        public int time = 0;
        public String current = "r";
        public boolean run = true;

        @Override
        public void run(){
            while(run) {
                switch (current){
                    case "p":
                        try{
                            sleep(1000);
                        }catch(InterruptedException e){
                            e.printStackTrace();
                        }
                        break;

                    case "s":
                            run =false;
                            break;
                    case "r":
                        try{
                            sleep(1000);
                        }catch(InterruptedException e){
                            e.printStackTrace();
                        }
                        time++;
                        System.out.println(time);


                }
            }
        }

    }

    public static void main(String[] args) {
        String input;
        Scanner sc = new Scanner(System.in);

        Timer t = new Timer();
        t.start();

        while(t.run){
            input = sc. nextLine();
            t.current = input;
            if(input.equals("s")){
                t.interrupt();
                break;
            }
        }
    }
}
