public class p3 {
    static class Student{
        private String name;
        private int age;

        public  Student(String name,int age){
            this.name= name;
            this.age =age;
        }

        public String  getName(){
            return this.name;
        }

        public int getage(){
            return this.age;
        }
    }


    public static void main(String[] args) {
        Student a =new Student("Mary",15);
        Student b =new Student("Tom",15);

        Thread x =new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (a) {
                    System.out.println(a.getName() + "is" + a.getage() + "歲");
                    synchronized (b) {
                        System.out.println(b.getName() + "is" + b.getage() + "歲");
                    }
                }
            }
        });
        Thread y =new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (b) {
                    System.out.println(b.getName() + "is" + b.getage() + "歲");
                    synchronized (a) {
                        System.out.println(a.getName() + "is" + a.getage() + "歲");
                    }
                }
            }
        });

        x.start();
        y.start();

        }
}


