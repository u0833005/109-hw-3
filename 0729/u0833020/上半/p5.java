public class p5 {
    static int counter = 0;
    static class Add implements Runnable{
        @Override
        public void run() {
            while (counter<=100){
                try{
                    Thread.sleep(1000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                addAndPrint();
            }
        }
    }

    public static synchronized void addAndPrint(){  //鎖這個函式
        if(counter<=100){
            System.out.println(Thread.currentThread().getName()+","+counter);
            counter++;
        }
    }

    public static void main(String[] args) {
        Add x =new Add();
        Thread a = new Thread(x);
        a.start();
        Thread b = new Thread(x);
        b.start();

    }
}
