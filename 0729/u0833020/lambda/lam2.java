public class lam2 {
    interface B{
        void printString(String s);
    }
    interface C{
        double doComputeWork(float x,float y);
    }

    public lam2(){
        B b = this::printOnce;
        b.printString("哈囉");
    }

    public static void main(String[] args) {
        B b = lam2::printTwice;
        b.printString("嗨");
        new lam2();

        C c = Math::pow;
        b.printString(String.valueOf(c.doComputeWork(2.5f,2)));

    }
    public static void printTwice(String s){
        System.out.println(s);
        System.out.println(s);
    }
    public  void printOnce (String s){
        System.out.println(s);

    }
}
