public class h10729 {
        @FunctionalInterface
        interface MyFun{
            String getValue (String str);
        }

        public static void main(String[] args) {
            String str1 = getValue("hello world",str-> str.toUpperCase());
            System.out.println("轉換成大寫後"+str1);
            String str2 = getValue("heloo world",str-> str.substring(1,4));
            System.out.println("擷取後的字串是:"+str2);
        }

        public static String getValue (String str, MyFun fun){
            return fun.getValue(str);

        }
    }

