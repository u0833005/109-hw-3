import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class lam4 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>(Arrays.asList("B", "X", "A", "M", "F"));
        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String str1, String str2) {
                return str1.compareTo(str2);
            }
        });
        System.out.println(list);


        ArrayList<String> list1 = new ArrayList<>(Arrays.asList("B", "X", "A", "M", "F"));
        list1.sort((str1,str2) -> str1.compareTo(str2));
        System.out.println(list1);


    }
}
