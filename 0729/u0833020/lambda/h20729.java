public class h20729 {
    static class T1 extends Thread {
        private int c;
        private int a;
        private int r;

        public T1(int a, int c) {
            this.c = c;
            this.a = a;
            this.r = 0;
        }

        @Override
        public void run() {
            for (int i = 0; i < c; i++) {
                r += a;
            }
            System.out.println(getName() + ":結束");
        }

        public int getResult() {
            return this.r;
        }
    }

    static class T2 extends Thread {
        private int c;
        private int a;
        private int r;

        public T2(int x, int y) {
            this.c = x;
            this.a = y;
            this.r = 0;
        }

        @Override
        public void run() {
            try {
                t1.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            while (c > 0) {
                r += a;
                c--;
            }
            System.out.println(getName() + ":結束");
        }

        public int getResult() {
            return this.r;
        }
    }

    interface Add {
        int add(int a, int b);
    }

    static p1.T1 t1 = new p1.T1(3, 10);
    static p1.T2 t2 = new p1.T2(5, 6);

    public static void main(String[] args) throws InterruptedException {
        t1.start();
        t2.start();

        t1.join();
        t2.join();

        p1.Add t3 = (int a, int b) -> {
            return a + b;
        };
        System.out.println(t1.getResult() + "," + t2.getResult() + "," + t3.add(t1.getResult(), t2.getResult()));
    }
}
