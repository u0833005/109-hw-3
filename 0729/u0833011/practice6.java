public class practice6
{
    interface Operator
    {
        void printAdd(MyInt a,MyInt b);
    }
    static class MyInt
    {
        private int value;
        MyInt()
        {
            this.value=0;
        }
        MyInt(int value)
        {
            this.value=value;
        }
        public String toString()
        {
            return Integer.toString(this.value);
        }
    }
    static class MyThread extends Thread
    {
        private MyInt myNum;
        private int target;
        public MyThread(MyInt myNum,int target)
        {
            this.myNum=myNum;
            this.target=target;
        }
        public void run()
        {
            while(myNum.value<target)
                myNum.value++;
            System.out.println(Thread.currentThread().getName()+" end!");
        }
    }
    public static void main(String[] args)
    {
        MyInt x=new MyInt();
        MyInt y=new MyInt();
        MyThread a=new MyThread(x,30);
        MyThread b=new MyThread(y,50);
        a.start();
        b.start();
        try
        {
            a.join();
            b.join();
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
        System.out.println(x+"+"+y+"="+(x.value+y.value));
        Operator oper=(MyInt i,MyInt j)->System.out.println(i+"+"+j+"="+(i.value+j.value));
        oper.printAdd(x,y);
    }
}
