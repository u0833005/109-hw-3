public class practice5
{
    interface StrOperator
    {
        String getValue(String str);
    }
    public static void main(String[] args)
    {
        StrOperator myoperator=(String str)->{
            System.out.println(str.toUpperCase());
            System.out.println(str.substring(1,4));
            return str.toUpperCase();
        };
        System.out.println("\n"+myoperator.getValue("Hello World!"));

    }
}
