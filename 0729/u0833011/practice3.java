import java.util.Scanner;

public class practice3
{
    static class MyInt
    {
        private int value;
        MyInt()
        {
            this.value=0;
        }
        MyInt(int value)
        {
            this.value=value;
        }
        public String toString()
        {
            return Integer.toString(this.value);
        }
    }
    static class MyTimer extends Thread
    {
        public MyInt time;
        public boolean isDead;
        public MyTimer(MyInt time)
        {
            this.time=time;
            isDead=false;
        }
        public void run()
        {
            while(!isDead)
            {
                try
                {
                    sleep(1000);
                    System.out.println(++this.time.value);
                }
                catch(Exception e)
                {
                    System.out.println(e);
                }
            }
        }
    }
    public static void main(String[] args)
    {
        Scanner myScanner=new Scanner(System.in);
        MyInt myNum=new MyInt();
        boolean isEnd=false;
        MyTimer timer=new MyTimer(myNum);
        timer.start();
        while(!isEnd)
        {
            String user=myScanner.nextLine();
            switch (user)
            {
                case "p":
                    timer.isDead=true;
                    break;
                case "r":
                    timer=new MyTimer(myNum);
                    timer.start();
                    break;
                case "s":
                    timer.isDead=true;
                    isEnd=true;
                    break;
            }
        }
    }
}
