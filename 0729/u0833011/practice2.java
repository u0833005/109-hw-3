public class practice2
{
    static class MyInt
    {
        private int value;
        MyInt()
        {
            this.value=0;
        }
        MyInt(int value)
        {
            this.value=value;
        }
        public String toString()
        {
            return Integer.toString(this.value);
        }
    }
    static class Thread1 extends Thread
    {
        private MyInt myNum;
        public Thread1(MyInt myNum)
        {
            this.myNum=myNum;
        }
        public void run()
        {
            try
            {
                while(myNum.value < 100)
                {
                    synchronized (myNum)
                    {
                        myNum.value++;
                        sleep(300);
                        System.out.println(getName() + ":" + myNum);
                    }
                }
            }
            catch (Exception e)
            {
                System.out.println(e);
            }
        }
    }
    public static void main(String[] args)
    {
        MyInt myNum=new MyInt();
        Thread1 thread1=new Thread1(myNum);
        Thread1 thread2=new Thread1(myNum);
        thread1.start();
        thread2.start();
    }
}
