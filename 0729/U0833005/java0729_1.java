import java.util.ArrayList;
import java.util.Locale;

public class java0729_1 {
    interface bstr{
        String getValue(String str);
    }
    public static void main(String[] args) {
        bstr b = new bstr() {
            @Override
            public String getValue(String str) {
                System.out.println(str.substring(1,4));
                return str.toUpperCase();
            }
        };
        bstr a = (String sr)->{
            System.out.println(sr.substring(1,4));
            return sr.toUpperCase();
        };
        System.out.println(a.getValue("hello world"));
        System.out.println(b.getValue("hello world"));
    }
}
