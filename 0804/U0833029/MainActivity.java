package com.example.newproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Stack;

public class MainActivity extends AppCompatActivity {

    @Override   
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText edittest=findViewById(R.id.edittest);
        Button btn11=findViewById(R.id.btn11); //C
        Button btn12=findViewById(R.id.btn12); //+/-
        Button btn13=findViewById(R.id.btn13); //(
        Button btn14=findViewById(R.id.btn14); //)

        Button btn21=findViewById(R.id.btn21); //1
        Button btn22=findViewById(R.id.btn22); //2
        Button btn23=findViewById(R.id.btn23); //3
        Button btn24=findViewById(R.id.btn24); //除

        Button btn31=findViewById(R.id.btn31); //4
        Button btn32=findViewById(R.id.btn32); //5
        Button btn33=findViewById(R.id.btn33); //6
        Button btn34=findViewById(R.id.btn34); //x

        Button btn41=findViewById(R.id.btn41); //7
        Button btn42=findViewById(R.id.btn42); //8
        Button btn43=findViewById(R.id.btn43); //9
        Button btn44=findViewById(R.id.btn44); //-

        Button btn51=findViewById(R.id.btn51); //←
        Button btn52=findViewById(R.id.btn52); //0
        Button btn53=findViewById(R.id.btn53); //=
        Button btn54=findViewById(R.id.btn54); //+

        TextView finalresult=findViewById(R.id.finalresult);

        btn21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("1");
            }
        });

        btn22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("2");
            }
        });

        btn23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("3");
            }
        });

        btn31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("4");
            }
        });

        btn32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("5");
            }
        });

        btn33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("6");
            }
        });

        btn41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("7");
            }
        });

        btn42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("8");
            }
        });

        btn43.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("9");
            }
        });

        btn52.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("0");
            }
        });

        btn54.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("+");
            }
        });

        btn44.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("-");
            }
        });

        btn34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("*");
            }
        });

        btn24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edittest.append("/");
            }
        });

        btn51.setOnClickListener(v -> {
           if(edittest.getText().toString().length()>0){
               edittest.setText(edittest.getText().subSequence(0,edittest.getText().length()-1));
           }else {
               edittest.setText("");
           }
        });

        btn11.setOnClickListener(v->edittest.setText(""));

        btn53.setOnClickListener(v->{
//            String[] x=edittest.getText().toString().split("[+|\\-*/]+");
//            String y=edittest.getText().toString().replaceAll("[0-9]+","");
//            String[] y=edittest.getText().toString().split("[0-9]+");
//            String result="";
//            int a=Integer.parseInt(x[0]);
//            int b=Integer.parseInt(x[1]);
//            int c=Integer.parseInt(x[2]);
//                switch (y[1]){
//                    case "+":
//                        switch (y[2]){
//                            case "+":
//                                result=String.valueOf(a+b+c);
//                                break;
//                            case "-":
//                                result=String.valueOf(a+b-c);
//                                break;
//                            case "*":
//                                result=String.valueOf(a+(b*c));
//                                break;
//                            case "/":
//                                result=String.valueOf(a+b/c);
//                                break;
//                        }
//                        break;
//                    case "-":
//                        switch (y[2]){
//                            case "+":
//                                result=String.valueOf(a-b+c);
//                                break;
//                            case "-":
//                                result=String.valueOf(a-b-c);
//                                break;
//                            case "*":
//                                result=String.valueOf(a-(b*c));
//                                break;
//                            case "/":
//                                result=String.valueOf(a-b/c);
//                                break;
//                        }
//                        break;
//                    case "*":
//                        switch (y[2]){
//                            case "+":
//                                result=String.valueOf(a*b+c);
//                                break;
//                            case "-":
//                                result=String.valueOf(a*b-c);
//                                break;
//                            case "*":
//                                result=String.valueOf(a*b*c);
//                                break;
//                            case "/":
//                                result=String.valueOf(a*b/c);
//                                break;
//                        }
//                        break;
//                    case "/":
//                        switch (y[2]){
//                            case "+":
//                                result=String.valueOf(a/b+c);
//                                break;
//                            case "-":
//                                result=String.valueOf(a/b-c);
//                                break;
//                            case "*":
//                                result=String.valueOf(a/b*c);
//                                break;
//                            case "/":
//                                result=String.valueOf(a/b/c);
//                                break;
//                        }
//                        break;
//                }
//            finalresult.setText(result);

            Stack<Integer> operand=new Stack<>();
            Stack<Character> operators=new Stack<>();
            StringBuilder tmp=new StringBuilder();
            String equation=edittest.getText().toString();
            for(int i=0;i<equation.length()-1;i++){
                char c=equation.charAt(i);
                if(isNumber(c)){
                    tmp.append(c);
                }else {
                    String tmpString=tmp.toString();
                    if(!tmpString.equals("")){
                        operand.push(Integer.parseInt(tmpString));
                        tmp=new StringBuilder();
                    }
                    while (!compare(operators,c)){
                        int b=operand.pop();
                        int a=operand.pop();
                        switch (operators.pop()){
                            case '+':operand.push(a+b);break;
                            case '-':operand.push(a-b);break;
                            case '*':operand.push(a*b);break;
                            case '/':operand.push(a/b);break;
                            default:break;
                        }
                    }
                    if(c!='='){
                        operators.push(c);
                    }
                }
            }
            if(!operand.empty()){
                finalresult.setText(String.valueOf(operand.pop()));
            }else {
                finalresult.setText("Error");
            }

        });

        btn13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edittest.append("(");
            }
        });

        btn14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edittest.append(")");
            }
        });


    }
    public boolean isNumber(char c){
        return (c>='0'&& c<='9');
    }
    public boolean compare(Stack<Character> operators,char s){
        if(operators.empty()){
            return true;
        }
        char top=operators.peek();
        switch (s){
            case '/':
            case '*':
                return top=='+'||top=='-';
            case '+':
            case '-':
            case '=':
                return false;
            default:
                break;
        }
        return true;
    }
}