package com.example.ajava_0804_1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private Button b0,b1,b2,b3,b4,b5,b6,b7,b8,b9;
    private Button ba,bb,bc,bd,bans,blt,brt,bcr,bde,bab;
    private EditText eas;
    private String ans="";
    private String ct1="";
    private String ct2="";
    private String ct3="";
    private String ct4="";
    private String ct5="";
    private String ctr1="",ctr2="",ctr3="",ctr4="";
    private int count=0,cn=0,aans=0;
    LinkedList<String> queue = new LinkedList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b0 = findViewById(R.id.b18);// 0
        b1 = findViewById(R.id.b5);// 1
        b2 = findViewById(R.id.b6);// 2
        b3 = findViewById(R.id.b7);// 3
        b4 = findViewById(R.id.b9);// 4
        b5 = findViewById(R.id.b10);// 5
        b6 = findViewById(R.id.b11);// 6
        b7 = findViewById(R.id.b13);// 7
        b8 = findViewById(R.id.b14);// 8
        b9 = findViewById(R.id.b15);// 9

        ba = findViewById(R.id.b20);// +
        bb = findViewById(R.id.b16);// -
        bc = findViewById(R.id.b12);// *
        bd = findViewById(R.id.b8);// /
        bans = findViewById(R.id.b19);// =

        blt = findViewById(R.id.b3);// (
        brt = findViewById(R.id.b4);// )

        bcr = findViewById(R.id.b1);// 清空
        bde = findViewById(R.id.b17);// 刪除

        eas = findViewById(R.id.eas);// 顯示格


        b0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("0");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("1");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("2");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("3");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("4");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("5");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("6");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("7");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("8");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("9");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });

        bcr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                while (queue.size()!=0) {
                    queue.remove();
                }
                eas.setText(ans);
                ans="";
            }
        });
        bde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.removeLast();
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });


        ba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("+");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        bb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("-");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        bc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("*");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        bd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("/");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });



        blt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add("(");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });
        brt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queue.add(")");
                for (int i=0;i<queue.size();i++) {
                    ans+=queue.get(i);
                }
                eas.setText(ans);
                ans="";
            }
        });




        bans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i=0;i<queue.size();i++) {
                    if(queue.get(i)=="+"||queue.get(i)=="-"||queue.get(i)=="*"||queue.get(i)=="/"){
                        for(int j=count;j<i;j++){
                            if(cn==0) {
                                ct1 += queue.get(j);
                            }
                            if(cn==1) {
                                ct2 += queue.get(j);
                            }
                            if(cn==2) {
                                ct3 += queue.get(j);
                            }
                            if(cn==3) {
                                ct4 += queue.get(j);
                            }
                            if(cn==4) {
                                ct5 += queue.get(j);
                            }
                        }
                        if(cn==0) {
                            ctr1=queue.get(i);
                        }
                        if(cn==1) {
                            ctr2=queue.get(i);
                        }
                        if(cn==2) {
                            ctr3=queue.get(i);
                        }
                        if(cn==3) {
                            ctr4=queue.get(i);
                        }
                        cn+=1;
                        count=i+1;
                    }
                    if(i==queue.size()-1){
                        for(int j=count;j<queue.size();j++){
                            if(cn==0) {
                                ct1 += queue.get(j);
                            }
                            if(cn==1) {
                                ct2 += queue.get(j);
                            }
                            if(cn==2) {
                                ct3 += queue.get(j);
                            }
                            if(cn==3) {
                                ct4 += queue.get(j);
                            }
                            if(cn==4) {
                                ct5 += queue.get(j);
                            }
                        }
                    }
                }
                switch (cn) {
                    case 1:
                        switch (ctr1) {
                            case "+":
                                aans = Integer.parseInt(ct1) + Integer.parseInt(ct2);
                                break;
                            case "-":
                                aans = Integer.parseInt(ct1) - Integer.parseInt(ct2);
                                break;
                            case "*":
                                aans = Integer.parseInt(ct1) * Integer.parseInt(ct2);
                                break;
                            case "/":
                                aans = Integer.parseInt(ct1) / Integer.parseInt(ct2);
                                break;
                        }
                        break;
                    case 2:
                        if(ctr1=="*"||ctr1=="/"){
                            if(ctr2=="+"||ctr2=="-"){
                                switch (ctr1) {
                                    case "*":
                                        aans = Integer.parseInt(ct1) * Integer.parseInt(ct2);
                                        break;
                                    case "/":
                                        aans = Integer.parseInt(ct1) / Integer.parseInt(ct2);
                                        break;
                                }
                                switch (ctr2) {
                                    case "+":
                                        aans = aans + Integer.parseInt(ct3);
                                        break;
                                    case "-":
                                        aans = aans - Integer.parseInt(ct3);
                                        break;
                                }
                            }
                            else{
                                switch (ctr1) {
                                    case "*":
                                        aans = Integer.parseInt(ct1) * Integer.parseInt(ct2);
                                        break;
                                    case "/":
                                        aans = Integer.parseInt(ct1) / Integer.parseInt(ct2);
                                        break;
                                }
                                switch (ctr2) {
                                    case "*":
                                        aans = aans * Integer.parseInt(ct3);
                                        break;
                                    case "/":
                                        aans = aans / Integer.parseInt(ct3);
                                        break;
                                }
                            }
                        }
                        if(ctr1=="+"||ctr1=="-"){
                            if(ctr2=="+"||ctr2=="-"){
                                switch (ctr1) {
                                    case "+":
                                        aans = Integer.parseInt(ct1) + Integer.parseInt(ct2);
                                        break;
                                    case "-":
                                        aans = Integer.parseInt(ct1) - Integer.parseInt(ct2);
                                        break;
                                }
                                switch (ctr2) {
                                    case "+":
                                        aans = aans + Integer.parseInt(ct3);
                                        break;
                                    case "-":
                                        aans = aans - Integer.parseInt(ct3);
                                        break;
                                }
                            }
                            else{
                                switch (ctr2) {
                                    case "*":
                                        aans = Integer.parseInt(ct2) * Integer.parseInt(ct3);
                                        break;
                                    case "/":
                                        aans = Integer.parseInt(ct2) / Integer.parseInt(ct3);
                                        break;
                                }
                                switch (ctr1) {
                                    case "+":
                                        aans = Integer.parseInt(ct1) + aans;
                                        break;
                                    case "-":
                                        aans = Integer.parseInt(ct1) - aans;
                                        break;
                                }
                            }
                        }
                        break;
                    case 3:
                        aans = 0;
                        break;
                    case 4:
                        aans = 0;
                        break;
                }
                ans = String.valueOf(aans);
                eas.setText(ans);

                while (queue.size()!=0) {
                    queue.remove();
                }
                cn=0;
                count=0;
                ct1="";
                ct2="";
                ct3="";
                ct4="";
                ct5="";

                ctr1="";
                ctr2="";
                ctr3="";
                ctr4="";
                aans=0;
                ans="";
            }
        });
    }
}