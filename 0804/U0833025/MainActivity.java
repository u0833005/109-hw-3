package com.example.newproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText Edit = findViewById(R.id.Edit);

        Button clear = findViewById(R.id.clear);
        Button sign = findViewById(R.id.sign);
        Button c1 = findViewById(R.id.c1);
        Button c2 = findViewById(R.id.c2);

        Button one = findViewById(R.id.one);
        Button two = findViewById(R.id.two);
        Button three = findViewById(R.id.three);
        Button div = findViewById(R.id.div);

        Button four = findViewById(R.id.four);
        Button five = findViewById(R.id.five);
        Button six = findViewById(R.id.six);
        Button mul = findViewById(R.id.mul);

        Button seven = findViewById(R.id.seven);
        Button eight = findViewById(R.id.eight);
        Button nine = findViewById(R.id.nine);
        Button sub = findViewById(R.id.sub);

        Button back = findViewById(R.id.back);
        Button zero = findViewById(R.id.zero);
        Button equal = findViewById(R.id.equal);
        Button add = findViewById(R.id.add);

        TextView textTitle = findViewById(R.id.textTitle);

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("1");
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("2");
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("3");
            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("4");
            }
        });

        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("5");
            }
        });

        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("6");
            }
        });

        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("7");
            }
        });

        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("8");
            }
        });

        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("9");
            }
        });

        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("0");
            }
        });

        sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("-");
            }
        });

        c1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("(");
            }
        });

        c2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append(")");
            }
        });

        div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("/");
            }
        });

        mul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("*");
            }
        });

        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("-");
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Edit.append("+");
            }
        });

        equal.setOnClickListener(v -> {
            String[] x = Edit.getText().toString().split("[+\\-*/]");
            String y = Edit.getText().toString().replaceAll("[0-9]","");
            String result = "";
            int X = Integer.parseInt(x[0]);
            int Y = Integer.parseInt(x[1]);

            switch (y){
                case "+":
                    result = String.valueOf(X+Y) ;
                    break;
                case "-":
                    result = String.valueOf(X-Y);
                    break;
                case "*":
                    result = String.valueOf(X*Y);
                    break;
                case "/":
                    result = String.valueOf(X/Y);
                    break;
            }
            textTitle.setText("result:"+result);
        });

        clear.setOnClickListener(v -> Edit.setText(""));

        back.setOnClickListener(v -> {
            if(Edit.getText().toString().length() > 0){
                Edit.setText(Edit.getText().subSequence(0, Edit.getText().length() - 1));
            }else{
                Edit.setText("");
            }
        });
    }
}