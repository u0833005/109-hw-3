package com.example.a0804_1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText number =findViewById(R.id.number);

        Button clear = findViewById(R.id.clear);
        Button buttonplus = findViewById(R.id.buttonplus);
        Button but9 = findViewById(R.id.but9);
        Button but0 = findViewById(R.id.but0);

        Button button1 = findViewById(R.id.button1);
        Button button2 = findViewById(R.id.button2);
        Button button3 = findViewById(R.id.button3);
        Button division = findViewById(R.id.division);

        Button button4 = findViewById(R.id.button4);
        Button button5 = findViewById(R.id.button5);
        Button button6 = findViewById(R.id.button6);
        Button multiplication = findViewById(R.id.multiplication);

        Button button7 = findViewById(R.id.button7);
        Button button8 = findViewById(R.id.button8);
        Button button9 = findViewById(R.id.button9);
        Button plus = findViewById(R.id.plus);

        Button butback = findViewById(R.id.butback);
        Button button0 = findViewById(R.id.button0);
        Button equal = findViewById(R.id.equal);
        Button minus = findViewById(R.id.minus);

        TextView result =findViewById(R.id.result);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("1");
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("2");
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("3");
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("4");
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("5");
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("6");
            }
        });
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("7");
            }
        });
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("8");
            }
        });
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("9");
            }
        });
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("0");
            }
        });


        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("+");
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("-");
            }
        });
        division.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("/");
            }
        });
        multiplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view ) {
                number.append("*");
            }
        });

        butback.setOnClickListener(view -> {
            if(number.getText().toString().length()>0){
                number.setText(number.getText().subSequence(0,number.getText().length()-1));
            }else{
                number.setText("");
            }
        });
        clear.setOnClickListener(view -> number.setText(""));

        equal.setOnClickListener(view -> {
            String[] x =number.getText().toString().split("[+\\-*/]");
            String y = number.getText().toString().replaceAll("[0-9]","");
            String res ="";
            int a= Integer.parseInt(x[0]);
            int b= Integer.parseInt(x[1]);
            switch (y){
                case "+":
                    res = String.valueOf(a+b);
                    break;
                case "-":
                    res = String.valueOf(a-b);
                    break;
                case "*":
                    res = String.valueOf(a*b);
                    break;
                case "/":
                    res = String.valueOf(a/b);
                    break;
            }
            result.setText(res);
        });
    }
}