package com.example.newproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.QuickContactBadge;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText input = findViewById(R.id.input);

        Button button42 = findViewById(R.id.button42);
        Button button43 = findViewById(R.id.button43);
        Button button44 = findViewById(R.id.button44);
        Button button45 = findViewById(R.id.button45);//第一排
        Button backspace = findViewById(R.id.backspace); //倒退刪除

        Button button39 = findViewById(R.id.button39); //加
        Button button41 = findViewById(R.id.button41); //減
        Button button33 = findViewById(R.id.button33); //乘
        Button button4 = findViewById(R.id.button4);   //除
        Button button38 = findViewById(R.id.button38); // =

        Button btn1 = findViewById(R.id.btn1);
        Button btn2 = findViewById(R.id.btn2);
        Button btn3 = findViewById(R.id.btn3);
        Button btn4 = findViewById(R.id.btn4);
        Button btn5 = findViewById(R.id.btn5);
        Button btn6 = findViewById(R.id.btn6);
        Button btn7 = findViewById(R.id.btn7);
        Button btn8 = findViewById(R.id.btn8);
        Button btn9 = findViewById(R.id.btn9);
        Button btn0 = findViewById(R.id.btn0);


        TextView tvResult = findViewById(R.id.tvResult);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("1");
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("2");
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("3");
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("4");
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("5");
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("6");
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("7");
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("8");
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("9");
            }
        });
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("0");
            }
        });


        button39.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("+");
            }
        });
        button41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("-");
            }
        });
        button33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("*");
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.append("/");
            }
        });

        backspace.setOnClickListener(view-> {  //倒退
            if (input.getText().toString().length()>0){
                input.setText(input.getText().subSequence(0,input.getText().length()-1));
            }else {
                input.setText("");
            }
        });

        button42.setOnClickListener(view->input.setText("")); //全部清除

        button38.setOnClickListener(view->{       //等號
            String[] x = input.getText().toString().split("[+|\\-*/]");
            String y = input.getText().toString().replaceAll("[0-9]+",""); //取代字串中全部0-9的數字為空字串,regex=正規表示式
            String result = "";

            int a = Integer.parseInt(x[0]);
            int b = Integer.parseInt(x[1]);

            switch (y){
                case  "+" : result=String.valueOf(a+b);break;
                case  "-" : result=String.valueOf(a-b);break;
                case  "*" : result=String.valueOf(a*b);break;
                case  "/" : result=String.valueOf(a/b);break;
            }
            tvResult.setText(result);



//            switch(x)
//            {
//                case "+":
//                    tvResult= (input.getText().toString().split("[+|\\-*/]")).toString();
//                    break;
//                case "-":
//                    tvResult= (input.getText().toString() - ).toString();
//                    break;
//                case "*":
//                    tvResult= (input.getText().toString() * ).toString();
//                    break;
//                case "/":
//                    tvResult= (input.getText().toString() / ).toString();
//                    break;
//                default:
//                    break;
//            }

        });






    }
    }
