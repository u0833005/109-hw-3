package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView output=(TextView)findViewById(R.id.output);
        EditText input=(EditText)findViewById(R.id.input);

        Button btnC=(Button)findViewById(R.id.buttonC);
        Button btn0=(Button)findViewById(R.id.button0);
        Button btn1=(Button)findViewById(R.id.button1);
        Button btn2=(Button)findViewById(R.id.button2);
        Button btn3=(Button)findViewById(R.id.button3);
        Button btn4=(Button)findViewById(R.id.button4);
        Button btn5=(Button)findViewById(R.id.button5);
        Button btn6=(Button)findViewById(R.id.button6);
        Button btn7=(Button)findViewById(R.id.button7);
        Button btn8=(Button)findViewById(R.id.button8);
        Button btn9=(Button)findViewById(R.id.button9);

        Button btnSubstract=(Button)findViewById(R.id.buttonAbstract); //-
        Button btn_Substract=(Button)findViewById(R.id.buttonSubstract); //-
        Button btnLeft=(Button)findViewById(R.id.buttonLeft); //(
        Button btnRight=(Button)findViewById(R.id.buttonRight); //)
        Button btnDivide=(Button)findViewById(R.id.buttonDivide); // divide
        Button btnProduct=(Button)findViewById(R.id.buttonProduct); //*
        Button btnAdd=(Button)findViewById(R.id.buttonAdd); //+
        Button btnDisappear=(Button)findViewById(R.id.buttonDisappear); //disappear
        Button btnEnd=(Button)findViewById(R.id.buttonEnd); //calculate

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine=manager.getEngineByName("js");

        btnC.setOnClickListener(v -> {
            input.setText("");
            output.setText("結果顯示");
        });
        btn0.setOnClickListener(v -> input.append("0"));
        btn1.setOnClickListener(v -> input.append("1"));
        btn2.setOnClickListener(v -> input.append("2"));
        btn3.setOnClickListener(v -> input.append("3"));
        btn4.setOnClickListener(v -> input.append("4"));
        btn5.setOnClickListener(v -> input.append("5"));
        btn6.setOnClickListener(v -> input.append("6"));
        btn7.setOnClickListener(v -> input.append("7"));
        btn8.setOnClickListener(v -> input.append("8"));
        btn9.setOnClickListener(v -> input.append("9"));

        btnSubstract.setOnClickListener(v -> input.append("-"));
        btn_Substract.setOnClickListener(v -> input.append("-"));
        btnLeft.setOnClickListener(v -> input.append("("));
        btnRight.setOnClickListener(v -> input.append(")"));
        btnDivide.setOnClickListener(v -> input.append("/"));
        btnProduct.setOnClickListener(v -> input.append("*"));
        btnAdd.setOnClickListener(v -> input.append("+"));

        btnDisappear.setOnClickListener(view -> input.setText(input.getText().toString().replaceFirst(".$","")));
        btnEnd.setOnClickListener(view -> {
            try {
                output.setText(String.valueOf((double)engine.eval(input.getText().toString())));
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
                output.setText("無法計算");
            }
        });
    }
}