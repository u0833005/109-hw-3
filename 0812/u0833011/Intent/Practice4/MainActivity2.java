package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import java.util.HashMap;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        setTitle("Result");
        TextView tvBMI=findViewById(R.id.tvBMI);
        TextView tvResult=findViewById(R.id.tvResult);
        Button btnHealth=findViewById(R.id.btnHealth);
        HashMap<String,String> data=(HashMap<String, String>) (getIntent().getSerializableExtra("data"));
        tvBMI.setText(data.get("bmi"));
        tvResult.setText(data.get("gender")+","+data.get("result"));
        btnHealth.setOnClickListener(view -> {
            Uri uri= Uri.parse("https://health99.hpa.gov.tw/onlineQuiz/bmi");
            Intent intent=new Intent(Intent.ACTION_VIEW,uri);
            startActivity(intent);
        });

    }
}