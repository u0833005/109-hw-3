package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText etCm=findViewById(R.id.etCm);
        EditText etKg=findViewById(R.id.etKg);
        RadioGroup rg=findViewById(R.id.rg);
        Button btnSend=findViewById(R.id.btnSend);
        btnSend.setOnClickListener(view -> {
            Intent intent=new Intent("android.intent.action.MAIN2");
            String cmStr=etCm.getText().toString();
            String kgStr=etKg.getText().toString();
            double cm,kg,bmi;
            try {
                cm=Double.parseDouble(cmStr);
                kg=Double.parseDouble(kgStr);
                if(cm<=0||kg<=0)
                    throw new Exception();
                bmi=kg/((cm/100)*(cm/100));

                HashMap<String,String> data=new HashMap<>();
                String gender=(rg.getCheckedRadioButtonId()==R.id.rbMale)?"男性":"女性";
                data.put("gender",gender);
                data.put("bmi",String.valueOf(bmi));
                String result;
                if(bmi<18.5)
                    result="體重過輕";
                else if(bmi>=24)
                    result="體重過重";
                else
                    result="體重正常";
                data.put("result",result);
                intent.putExtra("data",data);
                startActivity(intent);
            }
            catch (Exception e)
            {
                Toast.makeText(MainActivity.this,"請輸入合法資料",Toast.LENGTH_SHORT).show();
            }
        });
    }
}