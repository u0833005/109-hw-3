package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnCall=findViewById(R.id.btnCall);
        Button btnWeb=findViewById(R.id.btnWeb);
        btnCall.setOnClickListener(view -> {
            Uri phone= Uri.parse("tel:0912010286");
            Intent intent=new Intent(Intent.ACTION_DIAL,phone);
            startActivity(intent);
        });
        btnWeb.setOnClickListener(view -> {
            Uri uri=Uri.parse("https://m21248074.github.io");
            Intent intent=new Intent(Intent.ACTION_VIEW,uri);
            startActivity(intent);
        });
    }
}