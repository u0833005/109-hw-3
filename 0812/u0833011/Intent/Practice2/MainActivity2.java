package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        setTitle("Page2");
        TextView tvHeight=findViewById(R.id.tvHeight);
        TextView tvWidth=findViewById(R.id.tvWidth);
        Bundle bundle=getIntent().getExtras();
        MainActivity.Data mydata=bundle.getParcelable("Data");
        tvHeight.setText(mydata.getHeight());
        tvWidth.setText(mydata.getWidth());
    }
}