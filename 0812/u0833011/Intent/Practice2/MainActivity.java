package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    public static class Data implements Parcelable
    {
        private final String height;
        private final String width;
        public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };
        public Data(String height,String width)
        {
            this.height=height;
            this.width=width;
        }

        public String getHeight() {
            return height;
        }

        public String getWidth() {
            return width;
        }

        protected Data(Parcel in) {
            height = in.readString();
            width = in.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.height);
            parcel.writeString(this.width);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Page1");
        EditText etHeight=findViewById(R.id.etHeight);
        EditText etWidth=findViewById(R.id.etWidth);
        Button btnToPage2=findViewById(R.id.btnToPage2);
        btnToPage2.setOnClickListener(view -> {
            Intent intent=new Intent(MainActivity.this,MainActivity2.class);
            Data mydata=new Data(etHeight.getText().toString(),etWidth.getText().toString());
            Bundle bundle=new Bundle();
            bundle.putParcelable("Data",mydata);
            intent.putExtras(bundle);
            startActivity(intent);
        });
    }
}