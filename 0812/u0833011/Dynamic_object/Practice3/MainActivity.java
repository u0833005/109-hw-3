package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout mainLayout= findViewById(R.id.mainLayout);
        EditText etInput= findViewById(R.id.etInput);
        Button btnAdd= findViewById(R.id.btnAdd);
        ListView listView=new ListView(this);
        LinearLayout.LayoutParams mySetup=new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        mainLayout.addView(listView,mySetup);
        ArrayList<String> listData=new ArrayList<>();
        ArrayAdapter<String> adapter=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,listData);
        listView.setAdapter(adapter);
        btnAdd.setOnClickListener(view -> {
            String userInput=etInput.getText().toString();
            listData.add(userInput);
            adapter.notifyDataSetChanged();
            etInput.setText("");
        });
        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            listData.remove(i);
            adapter.notifyDataSetChanged();
        });

    }
}