package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LinearLayout mainLayout=(LinearLayout) findViewById(R.id.mainLayout);
        Button btnClick=new Button(this);
        TextView tvOutput=new TextView(this);
        int tvOutputId=5000;
        tvOutput.setId(tvOutputId);
        btnClick.setText("Click Me");
        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy年MM月dd日HH:mm:ss");
                Date currentDate=new Date(System.currentTimeMillis());
                tvOutput.setText(dateFormat.format(currentDate));
                if(mainLayout.findViewById(tvOutputId)!=null)
                    mainLayout.removeView(tvOutput);
                mainLayout.addView(tvOutput);
            }
        });
        LinearLayout.LayoutParams mySetup=new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        mainLayout.addView(btnClick,mySetup);
    }
}