package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private boolean isFirst=true;
    private Button btnFirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LinearLayout row1=findViewById(R.id.row1);
        LinearLayout row2=findViewById(R.id.row2);
        int size=2; //only even number
        ArrayList<Integer> colors=new ArrayList<>();
        Random random=new Random();
        for(int i=0;i<size*size/2;i++)
        {
            int r=random.nextInt(256);
            int g=random.nextInt(256);
            int b=random.nextInt(256);
            colors.add(Color.rgb(r,g,b));
        }
        ArrayList<String> hints=new ArrayList<>();
        for(int i=0;i<size*size/2;i++)
        {
            hints.add(String.valueOf(i));
            hints.add(String.valueOf(i));
        }
        Collections.shuffle(hints);
        Button[][] btns=new Button[size][size];
        int count=1;
        int defaultColor=Color.rgb(255,255,255);
        for(int i=0;i<size;i++)
        {
            for(int j=0;j<size;j++)
            {
                btns[i][j]=new Button(this);
                btns[i][j].setBackgroundColor(defaultColor);
                btns[i][j].setText(String.valueOf(count++));
                btns[i][j].setHint(hints.get(count-2));
                if(i==0)
                    row1.addView(btns[i][j]);
                else
                    row2.addView(btns[i][j]);
                int finalI = i;
                int finalJ = j;
                btns[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Button btn=btns[finalI][finalJ];
                        int btnColor=((ColorDrawable)btn.getBackground()).getColor();
                        String btnHint=btn.getHint().toString();
                        if(isFirst)
                        {
                            if(btnColor==defaultColor)
                            {
                                btn.setBackgroundColor(colors.get(Integer.parseInt(btnHint)));
                                btnFirst=btn;
                                isFirst = false;
                            }
                        }
                        else
                        {
                            if(btnColor==defaultColor)
                            {
                                btn.setBackgroundColor(colors.get(Integer.parseInt(btnHint)));
                                if(!btnFirst.getHint().toString().equals(btnHint))
                                {
                                    Handler handler=new Handler();
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            btn.setBackgroundColor(defaultColor);
                                            btnFirst.setBackgroundColor(defaultColor);
                                        }
                                    }, 1000);
                                }
                                isFirst=true;
                            }
                        }
                    }
                });
            }
        }
    }
}