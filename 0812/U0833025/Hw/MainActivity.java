package com.example.hw2_0812;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.icu.text.IDNA;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView textView;
    RadioGroup RG;
    RadioButton boy,girl;
    EditText height,weight;
    Button submit;

    public void setView(){
        textView = findViewById(R.id.textView);
        RG =  findViewById(R.id.RG);
        boy =  findViewById(R.id.boy);
        girl =  findViewById(R.id.girl);
        height = findViewById(R.id.height);
        weight = findViewById(R.id.weight);
        submit = findViewById(R.id.submit);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setView();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MainActivity2.class);

                Bundle bundle = new Bundle();
                bundle.putString("Height",height.getText().toString());
                bundle.putString("Weight",weight.getText().toString());
                if (RG.getCheckedRadioButtonId() == R.id.boy) {
                    bundle.putString("Gender",boy.getText().toString());
                } else {
                    bundle.putString("Gender",girl.getText().toString());
                }


                Double cm,kg,bmi;
                cm = Double.parseDouble(height.getText().toString());
                kg = Double.parseDouble(weight.getText().toString());
                bmi = kg/((cm/100)*(cm/100));
                bundle.putString("BMI",String.valueOf(bmi));
                String result;
                if(bmi >= 24){
                    result = "體重過重";
                    bundle.putString("Health",result);
                }else if(bmi < 24 ||bmi >= 18.5){
                    result = "體重適中";
                    bundle.putString("Health",result);
                }else {
                    result = "體重過輕";
                    bundle.putString("Health",result);
                }
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}