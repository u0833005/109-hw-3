package com.example.hw2_0812;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        TextView INFO = findViewById(R.id.info);
        ImageView image = findViewById(R.id.image);


        Bundle bundle = getIntent().getExtras();    //獲取傳遞過來的bundle
        String HEIGHT = bundle.getString("Height");
        String WEIGHT = bundle.getString("Weight");
        String GENDER = bundle.getString("Gender");
        String HEALTH = bundle.getString("Health");
        String BMI = bundle.getString("BMI");
        INFO.setText("性別為:"+GENDER+"\n身高為:"+HEIGHT+"公分\n體重為:"+WEIGHT+"公斤\nBMI為:"+BMI+"\n健康程度:"+HEALTH);

        if(GENDER.equals("男性")){
            image.setImageResource(R.drawable.male);
        }else {
            image.setImageResource(R.drawable.female);
        }

        Button web = findViewById(R.id.web);
        web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://health99.hpa.gov.tw/onlineQuiz/bmi");
                Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                startActivity(intent);
            }
        });
        ImageButton click = findViewById(R.id.click);
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://health99.hpa.gov.tw/onlineQuiz/bmi");
                Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                startActivity(intent);
            }
        });
    }


}