package com.example.chiayuan0812;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button button,button2;
    EditText height,weight;

    public void setView(){
        button = findViewById(R.id.button);
        button2 = findViewById(R.id.button2);
        height = findViewById(R.id.edit1);
        weight = findViewById(R.id.edit2);
    }

//    public static class Test implements Parcelable{
//        private String height1;
//        private String weight1;
//
//        public Test(String height1,String weight1){
//            this.height1 = height1;
//            this.weight1 = weight1;
//        }
//
//        public String getHeight1(){
//            return height1;
//        }
//        public String getWeight1(){
//            return weight1;
//        }
//
//        public void setHeight1(String height1){
//            this.height1 = height1;
//        }
//        public void setWeight1(String weight1){
//            this.weight1 = weight1;
//        }
//        @Override
//        public int describeContents() {
//            return 0;
//        }
//
//        @Override
//        public void writeToParcel(Parcel parcel, int i) {   //寫入的參數
//            parcel.writeString(height1);
//            parcel.writeString(weight1);
//        }
//        protected Test(Parcel in) { //讀出寫入的參數=>順序需跟寫入一樣<顛倒>
//            height1 = in.readString();
//            weight1 = in.readString();
//        }
//        public static final Creator<Test> CREATOR = new Creator<Test>() {   //傳遞Parcel後會利用CREATOR建構物件
//            @Override
//            public Test createFromParcel(Parcel in) {
//                return new Test(in);
//            }
//
//            @Override
//            public Test[] newArray(int size) {
//                return new Test[size];
//            }
//        };
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setView();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Test t = new Test(height.getText().toString(),weight.getText().toString());
//
//                Intent intent = new Intent(MainActivity.this,MainActivity2.class);
//
//                Bundle bundle = new Bundle();
//                bundle.putParcelable("T",t);
//                intent.putExtras(bundle);
//
                Uri uri = Uri.parse("tel:123456789");
                Intent intent = new Intent(Intent.ACTION_DIAL,uri);
                startActivity(intent);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("https://www.google.com.tw/?gws_rd=ssl");
                Intent intent = new Intent(Intent.ACTION_VIEW,uri);
                startActivity(intent);
            }
        });
    }
}