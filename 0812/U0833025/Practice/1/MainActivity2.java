package com.example.chiayuan0812;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity2 extends AppCompatActivity {
    int size = 2;   //2*2陣列
    int count = 0;
    Button[][] btlist;  //button陣列
    LinearLayout row1,row2;
    int colorArray[]={Color.parseColor("#e6ffe6"),Color.parseColor("#ffffcc")}; //顏色陣列
    int hintNum[]={0,0,1,1};
    Button btopen = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        row1 = findViewById(R.id.row1);
        row2 = findViewById(R.id.row2);
        createButton(); //產生button
        shuffle();  //洗牌
    }

    private void createButton(){    //產生button
        btlist = new Button[size][size];    //產生2*2的button陣列
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size ; j++){
                btlist[i][j] = new Button(MainActivity2.this);
                btlist[i][j].setText(String.valueOf(count));    //button上的數字
                count++;
                if(i == 0) row1.addView(btlist[i][j]);
                else row2.addView(btlist[i][j]);


                int final1 = i;
                int final2 = j;
                btlist[i][j].setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        btlist[final1][final2].setHint(String.valueOf(hintNum[final1*2+final2]));
                        btlist[final1][final2].setBackgroundColor(colorArray[hintNum[final1*2+final2]]);

                        if(btopen == null){
                            btopen = btlist[final1][final2];
                        }else{
                            if(btopen.getHint() == btlist[final1][final2].getHint()){
                                btopen.setBackgroundColor(Color.YELLOW);
                                btlist[final1][final2].setBackgroundColor(Color.YELLOW);
                                btopen = null;
                            }else{
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        btopen.setBackgroundColor(Color.parseColor("#d9d9d9"));
                                        btlist[final1][final2].setBackgroundColor(Color.parseColor("#d9d9d9"));
                                        btopen = null;
                                    }
                                },1000);
                            }
                        }
                    }
                });
            }
        }
    }

    private  void shuffle(){
        for(int i = 0; i < hintNum.length; i++){
            int current = hintNum[i];
            int ranNum = (int)(Math.random()*hintNum.length);
            hintNum[i] = hintNum[ranNum];
            hintNum[ranNum] = current;
        }
    }
}