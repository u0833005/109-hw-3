package com.example.chiayuan0812;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity3 extends AppCompatActivity {
    EditText editText;
    ImageButton imageButton;
    ListView listView;
    ArrayList<String> fruit = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        editText = findViewById(R.id.editText);
        imageButton = findViewById(R.id.imageButton);
        listView = findViewById(R.id.listView);
        fruit.add("apple");
        fruit.add("pineapple");
        ArrayAdapter arrayAdapter = new ArrayAdapter(MainActivity3.this, android.R.layout.simple_list_item_1,fruit);
        listView.setAdapter(arrayAdapter);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editText.getText().toString();
                if(TextUtils.isEmpty(name)){
                    Toast.makeText(MainActivity3.this,"無輸入值",Toast.LENGTH_SHORT).show();
                }else{
                    if(fruit.contains(name)){
                        Toast.makeText(MainActivity3.this,"重複",Toast.LENGTH_SHORT).show();
                    }else{
                        fruit.add(name);
                        listView.setAdapter(arrayAdapter);
                        editText.setText("");
                    }
                }
            }
        });
    }
}