package com.example.chiayuan0812;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    LinearLayout linearlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearlayout = findViewById(R.id.layout);
        Button bt = new Button(MainActivity.this);
        TextView text = new TextView(MainActivity.this);


        bt.setBackgroundColor(Color.BLUE);
        bt.setText("當前時間");
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日HH:mm:ss");
                Date curDate = new Date(System.currentTimeMillis()) ; // 獲取當前時間
                String str = formatter.format(curDate);
                text.setText(str);

            }
        });
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(250,100);
        layoutParams.setMargins(100,100,100,100);
        //linearlayout.addView(bt, 0);
        linearlayout.addView(bt, layoutParams);
        linearlayout.addView(text,0);

    }
}