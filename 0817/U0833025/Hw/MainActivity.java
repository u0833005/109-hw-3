package com.example.hw0817;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button show;
    TextView data,name;
    StringBuilder response = new StringBuilder();
    List<String> number = new ArrayList<>();
    List<String> type = new ArrayList<>();
    List<String> Name = new ArrayList<>();
    List<String> Phone = new ArrayList<>();

    public void Request(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL("http://192.168.68.109/data.json");
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.connect();

                    InputStream is = connection.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));

                    String line;
                    while((line = br.readLine()) != null){
                        response.append(line);
                    }
                    Log.e("response", response.toString());
                    String re = response.toString();

                    JSONObject jsonObject = new JSONObject(re);
                    String info = jsonObject.getString("phone");
                    Log.e("info",info);

                    JSONArray array = new JSONArray(info);
                    for(int i = 0; i < array.length() ;i++ ){
                        JSONObject data = array.getJSONObject(i);
                        String no = data.getString("number");
                        number.add(no);
                        JSONArray Num = new JSONArray(no);
                        Log.e("number",no);
                        for(int j = 0; j < Num.length(); j++){
                            JSONObject contents = Num.getJSONObject(j);
                            String name = contents.getString("姓名");
                            Name.add(name);
                            Message msg = handleInfo.obtainMessage(1,name);
                            handleInfo.sendMessage(msg);
                            String phone = contents.getString("電話");
                            Phone.add(phone);
                            msg = handleInfo.obtainMessage(2,phone);
                            handleInfo.sendMessage(msg);
                        }
                        String t = data.getString("type");
                        type.add(t);
                    }
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    Handler handleInfo = new Handler(Looper.myLooper()){
      public void handleMessage(Message msg){

//          switch(msg.what){
//              case 1:
//                  name.setText("姓名: "+(CharSequence)msg.obj);
//                  break;
//              case 2:
//                  data.setText("電話:"+(CharSequence)msg.obj);
//                  break;
//          }
          if(msg.what == 2){
              data.setText("電話:"+(CharSequence)msg.obj);
          }
      }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        show = findViewById(R.id.show);
        data = findViewById(R.id.data);
//        name = findViewById(R.id.name);

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Request();
            }
        });
    }
}