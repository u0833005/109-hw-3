package com.example.a0817_2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    Button bt_show;
    TextView tv_number,tv_type;
    String number;
    String type;
    URL url;
    HttpURLConnection connection;

    public void setView(){
        bt_show = findViewById(R.id.bt_show);
        tv_number = findViewById(R.id.tv_number);
        tv_type = findViewById(R.id.tv_type);
    }

    public void read(){

        Thread x = new Thread(new Runnable() {
            @Override
            public void run() {

                try{
                    url = new URL("http://192.168.68.109/echo.php");
                    connection = (HttpURLConnection) url.openConnection();
                    InputStream is = connection.getInputStream();
                    connection.connect();
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    String line;
                    StringBuilder response = new StringBuilder();
                    while((line = br.readLine()) != null){
                        response.append(line+"\n");
                    }
                    br.close();

                    String re = response.toString();
                    Log.e("re",re);

                    JSONObject jsonObject = new JSONObject(re);
                    String phone = jsonObject.getString("phone");
                    Log.e("phone",phone);

                    JSONArray array = new JSONArray(phone);
                    Log.e("array",String.valueOf(array));
                    for(int i = 0; i < array.length(); i++){
                        JSONObject data = array.getJSONObject(i);
                        Log.e("data",String.valueOf(data));

                        number = data.getString("number");
                        Log.e("number",number);
                        type = data.getString("type");
                        Log.e("type",type);

//                        Message msg = handlerNum.obtainMessage(1,number);
//                        handlerNum.sendMessage(msg);
//
//                        msg = handlerNum.obtainMessage(2,type);
//                        handlerNum.sendMessage(msg);

                    }


                }catch (IOException | JSONException e){
                    e.printStackTrace();
                }
            }
        });
        x.start();
    }

//    Handler handlerNum = new Handler(Looper.myLooper()){
//        public  void handleMessage(Message msg){
//            switch(msg.what){
//                case 1:
//                    tv_number.setText((CharSequence) msg.obj);
//                    break;
//                case 2:
//                    tv_type.setText((CharSequence) msg.obj);
//                    break;
//            }
//        }
//    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();

//        read();
        bt_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                read();
                //取得json資料
//                tv_number.setText(number);
//                tv_type.setText(type);
            }
        });
    }
}