package com.example.a0817_3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView lv_friend;
    Button btn_query;
    StringBuilder response = new StringBuilder();
    List<String> name = new ArrayList<>();
    List<String> star = new ArrayList<>();
    List<String> phone = new ArrayList<>();
    List<String> birth = new ArrayList<>();

    public void  WebService(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    URL url = new URL("http://192.168.68.109/name.json");
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.connect();

                    InputStream is = connection.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));

                    String line;
                    while ((line = br.readLine()) != null){
                        response.append(line);
                    }
                    Log.e("response",response.toString());
                    String re = response.toString();

                    JSONObject jsonObject = new JSONObject(re);
                    String people = jsonObject.getString("people");
                    Log.e("people",people);

                    JSONArray array = new JSONArray(people);
                    for(int i = 0; i < array.length() ;i++ ){
                        JSONObject data = array.getJSONObject(i);
                        String Name = data.getString("名子");
                        name.add(Name);
                        Log.e("Name",Name);
                        String Star = data.getString("星座");
                        star.add(Star);
                        String Phone = data.getString("電話");
                        phone.add(Phone);
                        String Birth = data.getString("生日");
                        birth.add(Birth);
                    }

                }catch (MalformedURLException e){
                    e.printStackTrace();
                }catch (IOException e){
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv_friend = findViewById(R.id.lv_friend);
        btn_query = findViewById(R.id.btn_query);

        WebService();

        btn_query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseAdapter baseAdapter = new BaseAdapter() {
                    @Override
                    public int getCount() {
                        return name.size();
                    }

                    @Override
                    public Object getItem(int position) {
                        return position;
                    }

                    @Override
                    public long getItemId(int position) {
                        return position;
                    }

                    @Override
                    public View getView(int position, View view, ViewGroup viewGroup) {
                        View layout_friend = View.inflate(MainActivity.this,R.layout.layout_friend,null);
                        TextView tv_name = (TextView) layout_friend.findViewById(R.id.tv_name);
                        TextView tv_star = (TextView) layout_friend.findViewById(R.id.tv_star);
                        TextView tv_phone = (TextView) layout_friend.findViewById(R.id.tv_phone);
                        TextView tv_birth = (TextView) layout_friend.findViewById(R.id.tv_birth);

                        tv_name.setText(name.get(position));
                        tv_star.setText(star.get(position));
                        tv_phone.setText(phone.get(position));
                        tv_birth.setText(birth.get(position));
                        return layout_friend;
                    }
                };
                lv_friend.setAdapter(baseAdapter);
            }
        });
    }
}