package com.example.a0817;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {
    Button send;
    TextView show;
    EditText text;
    DataInputStream inputStream;
    DataOutputStream outputStream;
    static Socket s;

    public void setView(){
        send = findViewById(R.id.send);
        show = findViewById(R.id.show);
        text = findViewById(R.id.text);
    }

    public void connect(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    s = new Socket("192.168.68.109",2222);
                    Log.e("connect: ","success");
                    outputStream = new DataOutputStream(s.getOutputStream());
                    inputStream = new DataInputStream(s.getInputStream());

                    while(true){
                        //收server訊息
                        String server_message = inputStream.readUTF();
//                        Message msg = handlerRec.obtainMessage(1,server_message);
//                        handlerRec.sendMessage(msg);
                        Message msg = handler.obtainMessage(1,server_message);
                        handler.sendMessage(msg);
                    }

                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }).start();
    }

//    Handler handlerRec = new Handler(Looper.myLooper()){
//        public void handleMessage(Message msg){
//            show.append("Server message: " + msg.obj + "\n");
//        }
//    };
//    Handler handlerSen = new Handler(Looper.myLooper()){
//        public void handleMessage(Message msg){
//            show.append("輸入訊息: " + msg.obj + "\n");
//            text.setText("");
//        }
//    };
    Handler handler = new Handler(Looper.myLooper()){
        public void handlerMessage(Message msg){
            switch (msg.what){
                case 1:
                    show.append("Server message: " + msg.obj + "\n");
                    break;
                case 2:
                    show.append("輸入訊息: " + msg.obj + "\n");
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setView();

        connect();
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = text.getText().toString();
                if(s.isConnected()){
                    //連線
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                outputStream.writeUTF(message);
                                Log.e("message",message);

//                                Message msg = handlerSen.obtainMessage(2,message);
//                                handlerSen.sendMessage(msg);
                                Message msg = handler.obtainMessage(2,message);
                                handler.sendMessage(msg);
                            }catch(IOException e){
                                e.printStackTrace();
                            }
                        }
                    }).start();
                }else{
                    Log.e("connect","disconnected");
                }
            }
        });

    }
}