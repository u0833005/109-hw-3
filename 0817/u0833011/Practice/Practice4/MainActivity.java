package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    private JSONArray jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lvData=findViewById(R.id.lvData);
        Button btnReq=findViewById(R.id.btnReq);

        Handler myhandler=new Handler(Looper.myLooper())
        {
            @Override
            public void handleMessage(@NonNull Message msg) {
                BaseAdapter adapter=new BaseAdapter() {
                    @Override
                    public int getCount() {
                        return jsonArray.length();
                    }

                    @Override
                    public Object getItem(int i) {
                        return i;
                    }

                    @Override
                    public long getItemId(int i) {
                        return i;
                    }

                    @Override
                    public View getView(int i, View view, ViewGroup viewGroup) {
                        View myView=View.inflate(MainActivity.this,R.layout.card,null);
                        TextView tvName=myView.findViewById(R.id.tvName);
                        TextView tvPhone=myView.findViewById(R.id.tvPhone);
                        TextView tvBirthday=myView.findViewById(R.id.tvBirthday);
                        TextView tvNight=myView.findViewById(R.id.tvNight);
                        try {
                            JSONObject object=jsonArray.getJSONObject(i);
                            tvName.setText("姓名: "+object.getString("名子"));
                            tvPhone.setText("電話: "+object.getString("電話"));
                            tvBirthday.setText("生日: "+object.getString("生日"));
                            tvNight.setText("星座: "+object.getString("星座"));
                        }
                        catch (Exception e)
                        {
                            System.out.println(e.getMessage());
                        }
                        return myView;
                    }
                };
                lvData.setAdapter(adapter);
                super.handleMessage(msg);
            }
        };

        btnReq.setOnClickListener(view -> new Thread(() -> {
            try {
                URL url = new URL("https://gitlab.com/mandylove0215/110-nuu-im-training/-/raw/master/0817_socket%E3%80%81http/name.json");
                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                connection.connect();

                InputStream is = connection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder buffer = new StringBuilder();
                while (true) {
                    String temp = br.readLine();
                    if (temp == null)
                        break;
                    buffer.append(temp);
                }
                br.close();
                String jsonStr = buffer.toString();
                JSONObject jsonObject = new JSONObject(jsonStr);
                jsonArray = jsonObject.getJSONArray("people");
                myhandler.sendMessage(myhandler.obtainMessage(1,""));
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }).start());
    }
}