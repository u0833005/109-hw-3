package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnReq=findViewById(R.id.btnReq);
        TextView tvNumber=findViewById(R.id.tvNumber);
        TextView tvType=findViewById(R.id.tvType);

        Handler myHandler=new Handler(Looper.myLooper())
        {
            @Override
            public void handleMessage(@NonNull Message msg) {
                String output=(String) msg.obj;
                switch(msg.what)
                {
                    case 0:
                        tvNumber.setText("Number: "+output);
                        break;
                    case 1:
                        tvType.setText("Type: "+output);
                        break;
                    case 2:
                        Toast.makeText(MainActivity.this,output,Toast.LENGTH_SHORT).show();
                }
                super.handleMessage(msg);
            }
        };

        btnReq.setOnClickListener(view ->
                new Thread(() -> {
                    try
                    {
                        URL url=new URL("https://gitlab.com/u0833005/109-hw-3/-/raw/master/0817/u0833011/Practice/Practice3/data.json");
                        HttpsURLConnection connection=(HttpsURLConnection) url.openConnection();
                        connection.connect();
                        InputStream is=connection.getInputStream();
                        BufferedReader br=new BufferedReader(new InputStreamReader(is));
                        StringBuilder buffer=new StringBuilder();
                        while(true)
                        {
                            String temp=br.readLine();
                            if(temp==null)
                                break;
                            buffer.append(temp);
                        }
                        br.close();
                        String jsonStr=buffer.toString();
                        JSONObject jsonObject=new JSONObject(jsonStr);
                        JSONArray jsonArray=jsonObject.getJSONArray("phone");
                        JSONObject jsonObject1=jsonArray.getJSONObject(0);
                        Message msg=myHandler.obtainMessage(0,jsonObject1.getString("number"));
                        myHandler.sendMessage(msg);
                        msg=myHandler.obtainMessage(1,jsonObject1.getString("type"));
                        myHandler.sendMessage(msg);
                    }
                    catch(Exception e)
                    {
                        myHandler.sendMessage(myHandler.obtainMessage(2,e.getMessage()));
                        System.out.println(e.getMessage());
                    }
                }).start());
    }
}