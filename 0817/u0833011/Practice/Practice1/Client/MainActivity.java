package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    public class MyThread extends Thread
    {
        public Handler mHandler;
        @Override
        public void run()
        {
            try
            {
                Looper.prepare();
                Socket s=new Socket("192.168.98.206",1234);
                DataOutputStream dos=new DataOutputStream(s.getOutputStream());
                DataInputStream dis=new DataInputStream(s.getInputStream());
                //tvOutput.setText("Server: "+dis.readUTF()+"\n"); //may be error
                Message msg=myHandler.obtainMessage(0,dis.readUTF());
                myHandler.sendMessage(msg);
                mHandler = new Handler(Looper.myLooper()) {
                    @Override
                    public void handleMessage(@NonNull Message msg) {
                        String userInput=(String)msg.obj;
                        try
                        {
                            dos.writeUTF(userInput);
                            String serverOutput=dis.readUTF();
                            Message toMsg=myHandler.obtainMessage(0,serverOutput);
                            myHandler.sendMessage(toMsg);
                        }
                        catch (Exception e)
                        {
                            Toast.makeText(MainActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                            System.out.println(e.getMessage());
                        }
                        super.handleMessage(msg);
                    }
                };
                Looper.loop();
            }
            catch(Exception e)
            {
                Toast.makeText(MainActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                System.out.println(e.getMessage());
            }
        }
    }

    private TextView tvOutput;
    private EditText etInput;
    private Handler myHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //binding object in view
        tvOutput=findViewById(R.id.tvOutput);
        etInput=findViewById(R.id.etInput);
        Button btnSend = findViewById(R.id.btnSend);

        myHandler=new Handler(Looper.myLooper()) // Looper.getMainLooper()
        {
            @Override
            public void handleMessage(@NonNull Message msg) {
                String serverOutput=(String)msg.obj;
                tvOutput.append("Server: "+serverOutput+"\n");
                super.handleMessage(msg);
            }
        };
        MyThread myThread=new MyThread();
        myThread.start();

        btnSend.setOnClickListener(view -> {
            String userInput=etInput.getText().toString();
            etInput.setText("");
            try
            {
                tvOutput.append("Client: "+userInput+"\n");
                Message msg=myThread.mHandler.obtainMessage(0,userInput);
                myThread.mHandler.sendMessage(msg);
            }
            catch(Exception e)
            {
                Toast.makeText(MainActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                System.out.println(e.getMessage());
            }
        });
    }
}