import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class practice1_server
{
    public static void main(String[] args)
    {
        try
        {
            ServerSocket server=new ServerSocket(1234);
            System.out.println("Start listening on port "+server.getLocalPort());
            System.out.println("Waiting for connect......");
            Socket client=server.accept();
            System.out.println("A client connected");
            DataInputStream dis=new DataInputStream(client.getInputStream());
            DataOutputStream dos=new DataOutputStream(client.getOutputStream());
            Scanner myScanner=new Scanner(System.in);
            dos.writeUTF("連線成功");
            System.out.println("Server: 連線成功");
            while(true)
            {
                if(client.isConnected())
                {
                    System.out.println("Client: "+dis.readUTF());
                    String serverInput=myScanner.nextLine();
                    dos.writeUTF(serverInput);
                    System.out.println("Server: "+serverInput);
                }
                else
                    break;
            }
            myScanner.close();
            server.close();
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}