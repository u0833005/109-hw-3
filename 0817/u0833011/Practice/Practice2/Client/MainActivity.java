package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    private Socket s;
    private DataInputStream dis;
    private DataOutputStream dos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tvOutput=findViewById(R.id.tvOutput);
        EditText etInput=findViewById(R.id.etInput);
        Button btnSend=findViewById(R.id.btnSend);

        Handler myHandler=new Handler(Looper.myLooper())
        {
            @Override
            public void handleMessage(@NonNull Message msg) {
                String message=(msg.obj==null)?"null":(String) msg.obj;
                switch(msg.what)
                {
                    case 0:
                        tvOutput.append("Server: "+message+"\n");
                        break;
                    case 1:
                        tvOutput.append("Client: "+message+"\n");
                        break;
                    case 2:
                        Toast.makeText(MainActivity.this,message,Toast.LENGTH_SHORT).show();
                        break;
                }
                super.handleMessage(msg);
            }
        };

        new Thread(() -> {
            try
            {
                s = new Socket("192.168.98.206",1234);
                dis=new DataInputStream(s.getInputStream());
                dos=new DataOutputStream(s.getOutputStream());
                Thread myThread = new Thread(() ->
                {
                    while(true)
                    {
                        try
                        {
                            String serverOutput=dis.readUTF();
                            myHandler.sendMessage(myHandler.obtainMessage(0,serverOutput));
                        }
                        catch (Exception e)
                        {
                            System.out.println(e.getMessage());
                            break;
                        }
                    }
                });
                myThread.start();
            }
            catch(Exception e)
            {
                myHandler.sendMessage(myHandler.obtainMessage(2,e.getMessage()));
                System.out.println(e.getMessage());
            }
        }).start();

        btnSend.setOnClickListener(view ->
        {
            String userInput=etInput.getText().toString();
            etInput.setText("");
            new Thread(() -> {
                try
                {
                    dos.writeUTF(userInput);
                    myHandler.sendMessage(myHandler.obtainMessage(1,userInput));
                }
                catch(Exception e)
                {
                    myHandler.sendMessage(myHandler.obtainMessage(2,e.getMessage()));
                    System.out.println(e.getMessage());
                }
            }).start();
        });
    }
}