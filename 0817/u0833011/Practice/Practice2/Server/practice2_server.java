import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class practice2_server {
    public static void main(String[] args)
    {
        try
        {
            ServerSocket server=new ServerSocket(1234);
            System.out.println("Start listening on port "+server.getLocalPort());
            System.out.println("Waiting for connect......");
            Socket client=server.accept();
            System.out.println("A client connected");
            DataInputStream dis=new DataInputStream(client.getInputStream());
            DataOutputStream dos=new DataOutputStream(client.getOutputStream());
            Scanner myScanner=new Scanner(System.in);
            new Thread(new Runnable(){
                public void run()
                {
                    while(true)
                    {
                        try
                        {
                            String userInput=dis.readUTF();
                            System.out.println("Client: "+userInput);
                        }
                        catch(Exception e)
                        {
                            System.out.println(e.getMessage());
                            break;
                        }
                    }
                }
            }).start();
            while(true) //main loop
            {
                if(client.isConnected())
                {
                    String serverOutput=myScanner.nextLine();
                    dos.writeUTF(serverOutput);
                    System.out.println("Server: "+serverOutput);
                }
                else
                    break;
            }
            myScanner.close();
            server.close();
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
