package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnReq=findViewById(R.id.btnReq);
        TextView tvNumber=findViewById(R.id.tvNumber);
        Handler myhandler=new Handler(Looper.myLooper())
        {
            @Override
            public void handleMessage(@NonNull Message msg) {
                tvNumber.setText("電話號碼: "+(String) msg.obj);
                super.handleMessage(msg);
            }
        };
        btnReq.setOnClickListener(view -> new Thread(() -> {
            try
            {
                URL url=new URL("https://gitlab.com/u0833005/109-hw-3/-/raw/master/0817/u0833011/Homework/data.json");
                HttpsURLConnection connection=(HttpsURLConnection) url.openConnection();
                connection.connect();
                InputStream is=connection.getInputStream();
                BufferedReader br=new BufferedReader(new InputStreamReader(is));
                StringBuilder buffer=new StringBuilder();
                while(true)
                {
                    String temp=br.readLine();
                    if(temp==null)
                        break;
                    buffer.append(temp);
                }
                br.close();
                String jsonStr=buffer.toString();
                JSONObject rootObject=new JSONObject(jsonStr);
                JSONArray phoneArray=rootObject.getJSONArray("phone");
                JSONObject targetPhoneObject=phoneArray.getJSONObject(0);
                JSONArray numberArray=targetPhoneObject.getJSONArray("number");
                JSONObject targetPersonObject=numberArray.getJSONObject(0);
                String targetPhoneStr=targetPersonObject.getString("電話");
                myhandler.sendMessage(myhandler.obtainMessage(0,targetPhoneStr));
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }).start());
    }
}