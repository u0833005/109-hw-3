function setup() {
    createCanvas(500, 500);

}

function draw(){
    background(220);
    translate(width/2,height/2)

    push()
    fill(255, 204, 0)
    rotate(hour() / 12 * TWO_PI + PI)
    rect(-2,0,10,100)
    pop()

    push()
    fill(color(0, 0, 255))
    rotate(minute() / 60 * TWO_PI + PI)
    rect(-5,0,4,150)
    pop()
    
    push()
    fill(255,0,0)
    //rotate(second() / 60 * TWO_PI + PI)
    secondAngle=map(second(),0,60,0,TWO_PI);
    rotate(secondAngle+PI);
    rect(0,-10,0,200)
    pop()

    for(let i=0;i<12;i++){
        push()
        //rotate(i / 12 * TWO_PI )
        rotate(map(i,0,12,0,TWO_PI));
        translate(0,-200)
        line(0,10,0,0)
        pop()


    }

}