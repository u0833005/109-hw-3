function setup()
{
    createCanvas(1200,800);
    background(255, 204, 0);
    button = createButton('click me')
    button.position(0, 500)
    button.mousePressed(changeBG)
	
}
function draw()
{
	fill(255,255,255)
	circle(100,600,100)
    noStroke();
    fill(30,144,255)
    ellipse(100,70,180,110)
    fill(30,144,255)
    rect(10,20,30,350,10)
    fill(255, 204, 0)
    ellipse(100,70,60,30)

    fill(30,144,255)
    rect(200,20,30,350,10)
    rect(210,20,100,30,10)
    rect(210,170,100,30,10)
    rect(210,340,100,30,10)

    ellipse(480,200,220,370)
    fill(255, 204, 0)
    ellipse(480,200,160,300)
    rect(520,120,100,180,10)

    fill(30,144,255)
    ellipse(720,200,220,370)
    fill(255, 204, 0)
    ellipse(720,200,160,300)

    
}
function changeBG()
{
    let val1 = random(255);
	let val2 = random(255);
	let val3 = random(255);
    background(val1,val2,val3);
}