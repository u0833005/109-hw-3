let inp,but,pause,a=true,b=false,inp1,sound;
function preload()
{
    sound=loadSound("ayame.mp3");
}
function setup()
{
    let tit = createP("計時器")
    tit.position(0, 0);
    inp = createInput("0")
    inp.position(0, 40);
    inp.size(300)
    but=createButton("開始")
    but.position(310,40)
    but.mousePressed(butPressed)
    pause=createButton("暫停")
    pause.position(360,40)
    pause.mousePressed(pausePressed)
    frameRate(1)
    
    let c=createCanvas(300,300)
    c.position(0,80)
    
}
function butPressed()
{   
    inp1=inp.value()
    textSize(60);
    text(inp1,10,60); 
    b=true;
    a=true;
}
function pausePressed()
{
    b=!b;
}
function draw()
{
    background(0,0,255)
    if(b)
    {
        if(inp1 == 0)
        {
            if(a){
                sound.play();
                a=false;
            }
        }else
        {
            inp1=inp1-1;        
        }
    }else
    {
        sound.stop();
    }
    textSize(60);
    text(inp1,10,60); 
}

