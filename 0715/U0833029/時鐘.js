function setup()
{
   createCanvas(1000,1000); 
}
function draw()
{
    background(255,204,0);
    translate(width/2,height/2);
    push()
    fill(51)
    rotate(hour()/12*TWO_PI+PI)
    rect(-5,-5,10,100)
    pop()
    push()
    fill(51)
    rotate(minute()/60*TWO_PI+PI)
    rect(-2,-2,4,150)
    pop()
    push()
    fill(51)
    rotate(second()/60*TWO_PI+PI)
    rect(0,-10,0,200)
    pop()
    for(let i=0;i<12;i++)
    {
        push()
        rotate(i/12*TWO_PI)
        translate(0,-200)
        line(0,-10,0,0)
        pop()
    }
}