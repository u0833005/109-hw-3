package com.example.myapplication;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private enum ViewType {
        LEFT, RIGHT
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public TextView tvMsg;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    private DataInputStream dis;
    private DataOutputStream dos;
    private List<Pair<String, String>> record;
    private FusedLocationProviderClient client;
    private String temp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextToSpeech myTTS = new TextToSpeech(this, null);
        client = LocationServices.getFusedLocationProviderClient(this);
        initLocation();

        RecyclerView rvOutput = findViewById(R.id.rvOutput);
        EditText etInput = findViewById(R.id.etInput);
        Button btnSend = findViewById(R.id.btnSend);
        Button btnSTT = findViewById(R.id.btnSTT);
        record = new ArrayList<>();

        RecyclerView.Adapter<ViewHolder> adapter = new RecyclerView.Adapter<ViewHolder>() {
            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LinearLayout layout = new LinearLayout(MainActivity.this);
                layout.setOrientation(LinearLayout.VERTICAL);
                layout.setPadding(20, 0, 20, 0);
                TextView tvName = new TextView(MainActivity.this);
                TextView tvMsg = new TextView(MainActivity.this);
                tvMsg.setTextSize(18);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                layout.setLayoutParams(params);
                tvName.setLayoutParams(params);
                tvMsg.setLayoutParams(params);
                if (viewType == ViewType.RIGHT.ordinal()) {
                    tvName.setGravity(Gravity.END);
                    tvMsg.setGravity(Gravity.END);
                }
                layout.addView(tvName);
                layout.addView(tvMsg);
                ViewHolder holder = new ViewHolder(layout);
                holder.tvName = tvName;
                holder.tvMsg = tvMsg;
                return holder;
            }

            @Override
            public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
                holder.tvName.setText(record.get(position).first);
                holder.tvMsg.setText(record.get(position).second);
            }

            @Override
            public int getItemCount() {
                return record.size();
            }

            @Override
            public int getItemViewType(int position) {
                return (record.get(position).first.equals("User")) ? ViewType.RIGHT.ordinal() : ViewType.LEFT.ordinal();
            }
        };
        rvOutput.setAdapter(adapter);
        rvOutput.setLayoutManager(new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false));

        Handler handler = new Handler(Looper.myLooper()) {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                if (msg.what == 0)
                    adapter.notifyDataSetChanged();
            }
        };

        ActivityResultLauncher<Intent> launcher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == AppCompatActivity.RESULT_OK) {
                        Intent data = result.getData();
                        if (data != null) {
                            ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                            String userInput = text.get(0);
                            record.add(new Pair<>("User", userInput));
                            handler.sendMessage(handler.obtainMessage(0, null));
                            new Thread(() -> {
                                try {
                                    dos.write(userInput.getBytes(StandardCharsets.UTF_8));
                                } catch (Exception e) {
                                    System.out.println(e.getMessage());
                                }
                            }).start();
                        }
                    }
                }
        );

        new Thread(() -> {
            try {
                Socket socket = new Socket("192.168.1.12", 1234);
                dis = new DataInputStream(socket.getInputStream());
                dos = new DataOutputStream(socket.getOutputStream());
                new Thread(() -> {
                    while (true) {
                        try {
                            byte[] buffer = new byte[1024];
                            int dataLength = dis.read(buffer);
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            baos.write(buffer, 0, dataLength);
                            String serverOutput = new String(baos.toByteArray(), StandardCharsets.UTF_8);
                            Log.d("Server Output",serverOutput);
                            try {
                                JSONObject response = new JSONObject(serverOutput);
                                String type = response.getString("type");
                                switch (type) {
                                    case "languageResult":
                                        String prefix = response.getString("prefix");
                                        String result = response.getString("result");
                                        String target = response.getString("target");
                                        record.add(new Pair<>("Robot", prefix + " " + result));
                                        handler.sendMessage(handler.obtainMessage(0, null));
                                        myTTS.speak(prefix, TextToSpeech.QUEUE_ADD, null, null);
                                        switch (target) {
                                            case "ja":
                                                myTTS.setLanguage(Locale.JAPAN);
                                                break;
                                            case "ko":
                                                myTTS.setLanguage(Locale.KOREA);
                                                break;
                                            case "en":
                                                myTTS.setLanguage(Locale.ENGLISH);
                                                break;
                                        }
                                        myTTS.speak(result, TextToSpeech.QUEUE_ADD, null, null);
                                        myTTS.setLanguage(Locale.getDefault());
                                        break;
                                    case "CoordinateRequest":
                                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                            // TODO: Consider calling
                                            //    ActivityCompat#requestPermissions
                                            // here to request the missing permissions, and then overriding
                                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                            //                                          int[] grantResults)
                                            // to handle the case where the user grants the permission. See the documentation
                                            // for ActivityCompat#requestPermissions for more details.
                                            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                                            ActivityCompat.requestPermissions(this, permissions, 1);
                                            break;
                                        }
                                        responseCoordinate();
                                        break;
                                }
                            } catch (Exception e) {
                                Log.e("error0",e.getMessage());
                                record.add(new Pair<>("Robot", serverOutput));
                                handler.sendMessage(handler.obtainMessage(0, null));
                                myTTS.speak(serverOutput, TextToSpeech.QUEUE_ADD, null, null);
                            }
                        } catch (Exception e) {
                            Log.e("error1", e.getMessage());
                            break;
                        }
                    }
                }).start();
            } catch (Exception e) {
                Log.e("error2", e.getMessage());
            }
        }).start();
        btnSend.setOnClickListener(view -> {
            String userInput = etInput.getText().toString();
            etInput.setText("");
            if(!userInput.equals(""))
            {
                record.add(new Pair<>("User", userInput));
                handler.sendMessage(handler.obtainMessage(0, null));
                new Thread(() -> {
                    try {
                        dos.write(userInput.getBytes(StandardCharsets.UTF_8));
                    } catch (Exception e) {
                        Log.e("error3", e.getMessage());
                    }
                }).start();
            }
        });
        btnSTT.setOnClickListener(view -> {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            launcher.launch(intent);
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            initLocation();
            responseCoordinate();
        }
    }

    public void initLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationRequest request = LocationRequest.create();
        request.setInterval(0);
        request.setFastestInterval(0);
        request.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        client.requestLocationUpdates(request, new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);
            }
        }, Looper.myLooper());
    }

    @SuppressLint("MissingPermission")
    public void responseCoordinate()
    {
        client.getLastLocation().addOnSuccessListener(location -> {
            try {
                JSONObject coordinate=new JSONObject();
                coordinate.put("lat",location.getLatitude());
                coordinate.put("lng",location.getLongitude());
                temp=coordinate.toString();
                System.out.println(temp);
                new Thread(() -> {
                    try {
                        dos.write(temp.getBytes(StandardCharsets.UTF_8));
                    }
                    catch (Exception e)
                    {
                        Log.e("error5",e.getMessage());
                    }
                }).start();
            }
            catch (Exception e)
            {
                Log.e("error4", e.getMessage());
            }
        });
    }
}