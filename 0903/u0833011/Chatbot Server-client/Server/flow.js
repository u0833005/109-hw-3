// @ts-check

import fs from "fs";
import util from "util";
import fetch from "node-fetch";
import { Bot } from "bard-builder";

const readFile=util.promisify(fs.readFile);

export default (/** @type { Bot } */ bot)=>
{
    bot.trailing("greeting",
    [
        (session,course)=>
        {
            /** @type { boolean } */
            const isKnown=session.storage.get("known");
            let greeting_message="你好，初次見面，我是凱維機器人";
            if(isKnown)
                greeting_message="很高興又見到你，我是凱維機器人";
            session.send(greeting_message);
            session.storage.set("greeted",true);
            session.storage.set("known",true);
            return course.replace("functions");
        }
    ]);
    bot.trailing("functions",
    [
        (session,course)=>
        {
            session.send("請問我能幫到你什麼忙?");
            return course.wait();
        }
    ]);
    bot.trailing("intent-route",
    [
        (session,course)=>
        {
            /** @type { string } */
            const intent=session.storage.get("intent");
            if(intent)
            {
                const previousIntent=session.storage.get("previousIntent");
                if(previousIntent!=null&&previousIntent!=intent)
                {
                    switch(previousIntent)
                    {
                        case "translation":
                            session.storage.set("translation_status",null);
                            break;
                        case "weather":
                            session.storage.set("weather_status",null);
                            break;
                    }
                }
                return course.replace(intent);
            }
            return course.replace("unknown");
        }
    ]);
    bot.trailing("unknown",
    [
        (session,course)=>
        {
            session.send("不好意思，我不瞭解你的意思。");
            return course.replace("functions");
        }
    ]);
    bot.trailing("translation",
    [
        (session,course)=>
        {
            /** @type { number } */
            const status=session.storage.get("translation_status");
            if(status)
                return course.jump(status);
            return course.next();
        },
        (session,course)=>
        {
            session.send("好的，請說出要翻譯的句子。");
            session.storage.set("translation_status",2);
            return course.wait();
        },
        (session,course)=>
        {
            session.storage.set("translation_source",session.storage.get("message"));
            session.send("請問要翻成什麼語言?");
            session.storage.set("translation_status",3);
            return course.wait();
        },
        async (session,course)=>
        {
            /** @type { string } */
            const message=session.storage.get("message");
            if(message.indexOf("日文")!=-1)
                session.storage.set("translation_targetLanguage","ja");
            else if(message.indexOf("韓文")!=-1)
                session.storage.set("translation_targetLanguage","ko");
            else
                session.storage.set("translation_targetLanguage","en");
            let requestURL="https://translation.googleapis.com/language/translate/v2?key=";
            /** @type { string } */
            let api_key=JSON.parse(await readFile("./api-key.json","utf-8")).google_api_key;
            /** @type { string } */
            let result=await fetch(requestURL+api_key,
            {
                method: "POST",
                headers:
                {
                    "Content-type": "application/json"
                },
                body:JSON.stringify(
                    {
                        q:session.storage.get("translation_source"),
                        target: session.storage.get("translation_targetLanguage"),
                        format: "text"
                    })
            }).then(async (response)=>
            {
                /** @type { object } */
                let temp=await response.json();
                return temp.data.translations[0].translatedText;
            });
            session.send(JSON.stringify(
                {
                    type: "languageResult",
                    target: session.storage.get("translation_targetLanguage"),
                    result: result,
                    prefix: "翻譯結果為"
                }
            ));
            session.storage.set("intent",null);
            session.storage.set("translation_status",null);
            course.replace("functions");
        }
    ]);
    bot.trailing("weather",
    [
        (session,course)=>
        {
            /** @type { number } */
            const status=session.storage.get("weather_status");
            if(status)
                return course.jump(status);
            return course.next();
        },
        (session,course)=>
        {
            session.send(JSON.stringify(
                {
                    type: "CoordinateRequest"
                }));
            session.storage.set("weather_status",2);
            course.wait();
        },
        async (session,course)=>
        {
            let latlng=JSON.parse(session.storage.get("message"));
            let requestURL=`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng.lat},${latlng.lng}&result_type=administrative_area_level_2&language=zh_TW&key=`;
            /** @type { object } */
            let api_key=JSON.parse(await readFile("./api-key.json","utf-8"));
            /** @type { string } */
            let cityName=await fetch(requestURL+api_key.google_api_key)
            .then(async (response)=>
            {
                /** @type { object } */
                let temp=await response.json();
                return temp.results[0].address_components[0].long_name;
            });
            requestURL=`https://opendata.cwb.gov.tw/api/v1/rest/datastore/F-C0032-001?Authorization=${api_key.weather_api_key}&format=JSON&locationName=${cityName}`;
            let result=await fetch(requestURL)
            .then(async (response)=>
            {
                /** @type { object } */
                let temp=await response.json();
                temp=temp.records.location[0].weatherElement;
                let result=
                {
                    Wx: temp[0].time[0].parameter.parameterName,
                    PoP: "百分之"+temp[1].time[0].parameter.parameterName,
                    MinT: "攝氏"+temp[2].time[0].parameter.parameterName+"度",
                    MaxT: "攝氏"+temp[4].time[0].parameter.parameterName+"度"
                };
                return result;
            });
            session.send(`今天天氣為${result.Wx}，降雨機率為${result.PoP}，溫度為${result.MinT}到${result.MaxT}`);
            session.storage.set("intent",null);
            session.storage.set("weather_status",null);
            course.replace("functions");
        }
    ])
    bot.trailing("bye",
    [
        (session,course)=>
        {
            session.send("再見，期待與你下次相見。");
            session.storage.set("greeted",false);
            session.storage.set("intent",null);
            return course.end();
        }
    ])
    bot.incoming("root",
    [
        (session,course)=>
        {
            /** @type { boolean } */
            const greeted=session.storage.get("greeted");
            if(!greeted)
                return course.replace("greeting");
            return course.next();
        }
    ])
    bot.incoming("understand-intent",[
        (session,course)=>
        {
            /** @type { string } */
            const message=session.getMessage().data;
            session.storage.set("message",message);
            const previousIntent=session.storage.get("intent");
            session.storage.set("previousIntent",previousIntent);

            if(message.indexOf("翻譯")!=-1)
                session.storage.set("intent","translation");
            else if(message.indexOf("天氣")!=-1)
                session.storage.set("intent","weather");
            else if(message.indexOf("再見")!=-1)
                session.storage.set("intent","bye");
            return course.replace("intent-route");
        }
    ]);
}