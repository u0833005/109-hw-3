// @ts-check

import net from "net";
import { Bot,Message } from "bard-builder";
import initFlow from "./flow.js";

let server=net.createServer(
    async (socket)=>
    {
        console.log("new client connected!");
        const bot=new Bot({"name": "Cavey_bot"});
        initFlow(bot);
        socket.on("data",(response)=>
        {
            /** @type { string } */
            let data=response.toString("utf-8");
            const message=new Message("contact","session","origin",data);
            bot.push(message);
        });
        await bot.start();
        function pullProcess()
        {
            const message=bot.pull();
            if(message instanceof Error!=true)
            {
                // @ts-ignore
                socket.write(message.data);
            }
            return setTimeout(pullProcess,500);
        }
        pullProcess();
        const agentMessage=new Message("contact","session","origin","Hello");
        bot.push(agentMessage);
    });
server.listen(1234);