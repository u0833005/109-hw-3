package com.example.myapplication;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Size;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Objects;

public class MainActivity extends AppCompatActivity
{
    private enum ViewType
    {
        LEFT, RIGHT
    }
    private final static ViewType[] types=ViewType.values();

    static class ViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView ivThumbnail;
        public TextView tvFilename;
        public Button btnDelete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    private File[] files;
    private final RecyclerView.Adapter<ViewHolder> adapter=new RecyclerView.Adapter<ViewHolder>()
    {
        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
        {
            View view=null;
            ImageView ivThumbnail=null;
            TextView tvFilename=null;
            Button btnDelete=null;
            switch(types[viewType])
            {
                case LEFT:
                    view=new RelativeLayout(MainActivity.this);
                    RelativeLayout layout=(RelativeLayout) view;
                    layout.setPadding(20,0,20,0);
                    RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                    RelativeLayout.LayoutParams params1=new RelativeLayout.LayoutParams(params);
                    params1.addRule(RelativeLayout.ALIGN_PARENT_LEFT,RelativeLayout.TRUE);
                    params1.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);
                    ivThumbnail=new ImageView(MainActivity.this);
                    ivThumbnail.setLayoutParams(params1);
                    layout.addView(ivThumbnail);

                    RelativeLayout.LayoutParams params2=new RelativeLayout.LayoutParams(params);
                    params2.addRule(RelativeLayout.ALIGN_PARENT_TOP,RelativeLayout.TRUE);
                    params2.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
                    tvFilename=new TextView(MainActivity.this);
                    tvFilename.setLayoutParams(params2);
                    layout.addView(tvFilename);

                    RelativeLayout.LayoutParams params3=new RelativeLayout.LayoutParams(params);
                    params3.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,RelativeLayout.TRUE);
                    params3.addRule(RelativeLayout.CENTER_VERTICAL,RelativeLayout.TRUE);
                    btnDelete=new Button(MainActivity.this);
                    btnDelete.setLayoutParams(params3);
                    btnDelete.setText("刪除");
                    layout.addView(btnDelete);
                    break;
                case RIGHT:
                    view=View.inflate(MainActivity.this,R.layout.right,null);
                    ivThumbnail=view.findViewById(R.id.ivThumbnail);
                    tvFilename=view.findViewById(R.id.tvFilename);
                    btnDelete=view.findViewById(R.id.btnDelete);
                    break;
            }
            ViewHolder holder=new ViewHolder(view);
            holder.ivThumbnail=ivThumbnail;
            holder.tvFilename=tvFilename;
            holder.btnDelete=btnDelete;
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            try
            {
                File file= Objects.requireNonNull(files)[position];
                Bitmap thumbnail= ThumbnailUtils.createImageThumbnail(file,new Size(200,200),null);
                holder.ivThumbnail.setImageBitmap(thumbnail);
                holder.ivThumbnail.setOnClickListener(view -> {
                    Intent intent=new Intent(Intent.ACTION_VIEW);
                    Uri uri=FileProvider.getUriForFile(
                            MainActivity.this,
                            "com.example.myapplication.fileprovider",
                            file);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setDataAndType(uri,"image/*");
                    startActivity(intent);
                });
                holder.tvFilename.setTextSize(20);
                holder.tvFilename.setText(file.getName());
                holder.tvFilename.setOnClickListener(view -> Toast.makeText(MainActivity.this,file.getAbsolutePath(),Toast.LENGTH_SHORT).show());
                holder.btnDelete.setOnClickListener(view -> {
                    if(file.delete())
                    {
                        Toast.makeText(MainActivity.this, "刪除成功", Toast.LENGTH_SHORT).show();
                        files=getFilesDir().listFiles();
                        adapter.notifyItemRemoved(position);
                    }
                });
            }
            catch (Exception e)
            {
                Log.e("Error", e.getMessage());
            }
        }

        @Override
        public int getItemCount() {
            return Objects.requireNonNull(files).length;
        }

        @Override
        public int getItemViewType(int position) {
            if(position%2==0)
                return ViewType.LEFT.ordinal();
            return ViewType.RIGHT.ordinal();
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        files=getFilesDir().listFiles();
        Button btnCamera=findViewById(R.id.btnCamera);
        RecyclerView rvOutput=findViewById(R.id.rvOutput);

        rvOutput.setAdapter(adapter);
        rvOutput.setLayoutManager(new LinearLayoutManager(MainActivity.this,LinearLayoutManager.VERTICAL,false));

        @SuppressLint("NotifyDataSetChanged") ActivityResultLauncher<Intent> launcher=registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    files=getFilesDir().listFiles();
                    adapter.notifyDataSetChanged();
                });

        btnCamera.setOnClickListener(view -> {
            AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("檔案名稱");
            EditText etFilename=new EditText(MainActivity.this);
            etFilename.setHint("請輸入檔案名稱");
            builder.setView(etFilename);
            builder.setPositiveButton("OK", (dialogInterface, i) -> {
                Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File tmpFile=new File(getFilesDir(),etFilename.getText()+".jpg");
                Uri uri=FileProvider.getUriForFile(
                        MainActivity.this,
                        "com.example.myapplication.fileprovider",
                        tmpFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
                launcher.launch(intent);
            });
            AlertDialog dialog=builder.create();
            dialog.show();
            Log.e("internal",getFilesDir().toString());
            Log.e("external", Environment.DIRECTORY_PICTURES);
        });
    }
}