package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    static private class MyListener implements SensorEventListener
    {
        private final TextView tvDisplay;

        MyListener(TextView tvDisplay)
        {
            this.tvDisplay=tvDisplay;
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            String sensorName="Sensor Name: "+sensorEvent.sensor.getName()+"\n";
            String sensorType="Sensor Type: "+sensorEvent.sensor.getStringType()+"\n\n";
            String UoM="Units of measure: ";
            String UoM_data="m/s^2";
            String display=null;
            String postfix="";
            switch(sensorEvent.sensor.getType())
            {
                case Sensor.TYPE_ACCELEROMETER:
                case Sensor.TYPE_GRAVITY:
                case Sensor.TYPE_LINEAR_ACCELERATION:
                    break;
                case Sensor.TYPE_GYROSCOPE:
                    UoM_data="rad/s";
                    break;
                case Sensor.TYPE_ROTATION_VECTOR:
                    UoM_data="Unitless";
                    postfix="\nscalar: "+sensorEvent.values[3];
                    break;
                case Sensor.TYPE_GAME_ROTATION_VECTOR:
                    UoM_data="Unitless";
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    UoM_data="μT";
                    break;
                case Sensor.TYPE_PROXIMITY:
                    UoM_data="cm";
                    display=String.format(Locale.getDefault(),"Distance from Object: %f",sensorEvent.values[0]);
                    break;
            }
            display=(display==null)?String.format(
                    Locale.getDefault(),
                    "x:%f\ny:%f\nz:%f",
                    sensorEvent.values[0],sensorEvent.values[1],sensorEvent.values[2]):display;
            tvDisplay.setText(sensorName+sensorType+UoM+UoM_data+"\n"+display+postfix);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int i) {}
    }

    private SensorManager manager;
    private MyListener listener;
    private int msgNum;
    private SensorEventListener extraListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnList=findViewById(R.id.btnList);
        Button btnAccelerometer=findViewById(R.id.btnAccelerometer);
        Button btnGravity=findViewById(R.id.btnGravity);
        Button btnGyroscope=findViewById(R.id.btnGyroscope);
        Button btnLinearAcc=findViewById(R.id.btnLinearAcc);
        Button btnRotationVector=findViewById(R.id.btnRotationVector);
        Button btnSignificantMotion=findViewById(R.id.btnSignificantMotion);
        Button btnGameRotationVector=findViewById(R.id.btnGameRotationVector);
        Button btnMagneticField=findViewById(R.id.btnMagneticField);
        Button btnProximity=findViewById(R.id.btnProximity);
        Button btnExtra=findViewById(R.id.btnExtra);

        manager=(SensorManager) getSystemService(SENSOR_SERVICE);
        btnList.setOnClickListener(view -> {
            List<Integer> types=new ArrayList<>(
                    Arrays.asList(
                            Sensor.TYPE_ALL,
                            //Motion sensor
                            Sensor.TYPE_ACCELEROMETER,Sensor.TYPE_GRAVITY,
                            Sensor.TYPE_GYROSCOPE,Sensor.TYPE_LINEAR_ACCELERATION,
                            Sensor.TYPE_ROTATION_VECTOR,Sensor.TYPE_SIGNIFICANT_MOTION,
                            Sensor.TYPE_STEP_COUNTER,Sensor.TYPE_STEP_DETECTOR,
                            //Position sensor
                            Sensor.TYPE_GAME_ROTATION_VECTOR,Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR,
                            Sensor.TYPE_MAGNETIC_FIELD,Sensor.TYPE_PROXIMITY,
                            //Environment sensor
                            Sensor.TYPE_AMBIENT_TEMPERATURE,Sensor.TYPE_LIGHT,
                            Sensor.TYPE_PRESSURE,Sensor.TYPE_RELATIVE_HUMIDITY));
            /*
                deprecated sensor
                Motion sensor
                - none
                Position sensor
                - Orientation
                Environment sensor
                - Temperature sensor
            */
            List<String> typesName=new ArrayList<>(
                    Arrays.asList(
                            "All",
                            //Motion sensor
                            "Accelerometer","Gravity","Gyroscope","Linear acceleration",
                            "Rotation vector","Significant motion","Step counter","Step detector",
                            //Position sensor
                            "Game rotation vector","Geomagnetic rotation vector",
                            "Magnetic field","Proximity",
                            //Environment sensor
                            "Ambient temperature","Light","Pressure","Relative humidity"));
            List<String> allSensor=new ArrayList<>();
            manager.getSensorList(Sensor.TYPE_ALL).forEach(sensor->allSensor.add(sensor.getName()));
            List<Sensor> sensorsList;
            AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
            StringBuilder strBuilder=new StringBuilder();
            for(int i=0;i<types.size();i++)
            {
                if(manager.getDefaultSensor(types.get(i))!=null)
                {
                    sensorsList=manager.getSensorList(types.get(i));
                    strBuilder.append("---").append(typesName.get(i)).append(" sensor list---\n");
                    sensorsList.forEach(sensor -> strBuilder.append(sensor.getName()).append("\n"));
                    if(i!=0)
                        sensorsList.forEach(sensor->allSensor.remove(sensor.getName()));
                }
                else
                {
                    strBuilder.append("---No ").append(typesName.get(i)).append(" sensor---\n");
                    Log.w("Warning", "No "+typesName.get(i));
                }
                strBuilder.append("\n");
            }
            strBuilder.append("---Rest sensor List---\n");
            allSensor.forEach(sensor-> strBuilder.append(sensor).append("\n"));
            builder.setMessage(strBuilder.toString()).show();
        });
        btnAccelerometer.setOnClickListener(generateBtnListener(Sensor.TYPE_ACCELEROMETER,"Accelerometer"));
        btnGravity.setOnClickListener(generateBtnListener(Sensor.TYPE_GRAVITY,"Gravity"));
        btnGyroscope.setOnClickListener(generateBtnListener(Sensor.TYPE_GYROSCOPE,"Gyroscope"));
        btnLinearAcc.setOnClickListener(generateBtnListener(Sensor.TYPE_LINEAR_ACCELERATION,"Linear Acceleration"));
        btnRotationVector.setOnClickListener(generateBtnListener(Sensor.TYPE_ROTATION_VECTOR,"Rotation Vector"));
        btnSignificantMotion.setOnClickListener(view -> {
            manager.unregisterListener(listener);
            AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
            Sensor sensor=manager.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION);
            if(sensor!=null)
            {
                builder.setTitle("Significant motion");
                TextView tvDisplay=new TextView(MainActivity.this);
                tvDisplay.setGravity(Gravity.CENTER);
                builder.setView(tvDisplay);

                msgNum=0;

                Handler handler=new Handler(Looper.myLooper())
                {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void handleMessage(@NonNull Message msg) {
                        super.handleMessage(msg);
                        if(msgNum>-1)
                        {
                            tvDisplay.setText("Trigger Num: "+msgNum);
                            manager.requestTriggerSensor((TriggerEventListener) msg.obj, sensor);
                        }
                    }
                };

                handler.sendMessage(handler.obtainMessage(0, new TriggerEventListener() {
                    @Override
                    public void onTrigger(TriggerEvent triggerEvent) {
                        if(msgNum>-1)
                        {
                            Toast.makeText(MainActivity.this, "Trigger.", Toast.LENGTH_SHORT).show();
                            msgNum++;
                            handler.sendMessage(handler.obtainMessage(0, this));
                        }
                    }
                }));
                builder.setPositiveButton("OK", (dialogInterface, i) -> msgNum=-1);
            }
            else
                builder.setTitle("Error").setMessage("No sensor.");
            builder.show();
        });
        btnGameRotationVector.setOnClickListener(generateBtnListener(Sensor.TYPE_GAME_ROTATION_VECTOR,"Game Rotation Vector"));
        btnMagneticField.setOnClickListener(generateBtnListener(Sensor.TYPE_MAGNETIC_FIELD,"Magnetic Field"));
        btnProximity.setOnClickListener(generateBtnListener(Sensor.TYPE_PROXIMITY,"Proximity"));

        btnExtra.setOnClickListener(view -> {
            manager.unregisterListener(listener);
            AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
            Sensor sensor=manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            if(sensor!=null)
            {
                SurfaceView svCanvas=new SurfaceView(MainActivity.this);
                SurfaceHolder holder=svCanvas.getHolder();
                holder.addCallback(new SurfaceHolder.Callback() {
                    @Override
                    public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
                        Canvas canvas=surfaceHolder.lockCanvas();
                        Point circle=new Point(20,20);
                        int radius=20;
                        Paint paint=new Paint();
                        paint.setColor(Color.WHITE);
                        canvas.drawCircle(circle.x,circle.y,radius,paint);
                        surfaceHolder.unlockCanvasAndPost(canvas);
                        Sensor sensor=manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
                        extraListener=new SensorEventListener() {
                            @Override
                            public void onSensorChanged(SensorEvent sensorEvent) {
                                Canvas canvas=surfaceHolder.lockCanvas();
                                canvas.drawColor(Color.BLACK);
                                int nextX=circle.x-(int)sensorEvent.values[0];
                                int nextY=circle.y+(int)sensorEvent.values[1];
                                if(nextX>=radius&&nextY>=radius&&nextX<canvas.getWidth()-radius&&nextY<canvas.getHeight()-radius)
                                {
                                    circle.x=nextX;
                                    circle.y=nextY;
                                }
                                canvas.drawCircle(circle.x,circle.y,radius,paint);
                                surfaceHolder.unlockCanvasAndPost(canvas);
                            }

                            @Override
                            public void onAccuracyChanged(Sensor sensor, int i) {}
                        };
                        manager.registerListener(extraListener,sensor,SensorManager.SENSOR_DELAY_NORMAL);
                    }

                    @Override
                    public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {}

                    @Override
                    public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {
                        manager.unregisterListener(extraListener,sensor);
                    }
                });
                builder.setView(svCanvas);
            }
            else
                builder.setTitle("Error").setMessage("No sensor.");
            builder.show();
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        manager.unregisterListener(listener);
        manager.unregisterListener(extraListener);
    }

    private View.OnClickListener generateBtnListener(int sensorType,String typeStr)
    {
        return view -> {
            manager.unregisterListener(listener);
            AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
            Sensor sensor=manager.getDefaultSensor(sensorType);
            if(sensor!=null)
            {
                builder.setTitle(typeStr+" Data");
                TextView tvDisplay=new TextView(MainActivity.this);
                tvDisplay.setGravity(Gravity.CENTER);
                listener=new MyListener(tvDisplay);
                manager.registerListener(listener,sensor,SensorManager.SENSOR_DELAY_NORMAL);
                builder.setView(tvDisplay);
            }
            else
                builder.setTitle("Error").setMessage("No "+typeStr+" sensor.");
            builder.show();
        };
    }
}