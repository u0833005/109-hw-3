package com.example.a0811pra1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    Button clear;
    Spinner spinner;
    TextView show;
    Button newbtn;
    String subject;
    ArrayList<String > data = new ArrayList<>();
    String[] datas ={"肉燥飯","雞腿飯","控肉飯","排骨飯"};

    public  void SetView(){
        listView =findViewById(R.id.listview);
        clear =findViewById(R.id.clear);
        spinner =findViewById(R.id.spinner);
        show =findViewById(R.id.show);
        newbtn =findViewById(R.id.newbtn);
    }
    public void SetData(){
        data.add("統計學");
        data.add("微積分");
        data.add("程式設計");
        data.add("管理學");
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        SetView();
        SetData();

        ArrayAdapter<String> arrayAdapter =new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1,data);
//        listView.setAdapter(arrayAdapter);
//        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                int now =position+1;
//                Log.e("點擊事件","點擊了第"+now+"個");
//            }
//        });


        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                int now1 =position+1;
                show.setText("點擊了第"+now1+"個"+"內容為"+data.get(position));
                subject =data.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.e("onNothingSelected","執行");

            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.clear();
                arrayAdapter.notifyDataSetChanged();
            }
        });
        newbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder b =new AlertDialog.Builder(MainActivity.this);
                b.setTitle("標題");
//                b.setMessage(subject);
                b.setItems(datas, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        show.setText("我想吃"+datas[i]);
                    }
                });
                b.setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("確定","run");
                        Toast toast=Toast.makeText(MainActivity.this,"確定",Toast.LENGTH_LONG);
                        toast.show();
                    }
                });
                b.setNegativeButton("取消",new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("取消","run");
                        Toast toast=Toast.makeText(MainActivity.this,"取消",Toast.LENGTH_LONG);
                        toast.show();
                    }
                });
                b.setNeutralButton("忽略", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("忽略","run");
                        Toast toast=Toast.makeText(MainActivity.this,"忽略",Toast.LENGTH_LONG);
                        toast.show();
                    }
                });

                AlertDialog dialog =b.create();
                dialog.show();
            }
        });

    }
}