package com.example.nn08112;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;   // x.appcompat

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    int a = 1;
    int b = 1;
    int c = 1;
    int d = 1;
    Toolbar toolbar;
    ListView lv_tc;
    ArrayList<String> teacherName = new ArrayList<>();
    ArrayList<Integer> teacherPicture = new ArrayList<>();
    ArrayList<String> teacherhi = new ArrayList<>();


    public void SetView(){
        toolbar=findViewById(R.id.toolbar);
        lv_tc = findViewById(R.id.tv_tclist);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_example,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
            case R.id.setting:
                Toast.makeText(MainActivity.this,"點擊了設定",Toast.LENGTH_SHORT).show();
                break;
            case R.id.help:
                Toast.makeText(MainActivity.this,"點擊了幫助",Toast.LENGTH_SHORT).show();
                break;
            case R.id.no:
                Toast.makeText(MainActivity.this,"點擊了不要",Toast.LENGTH_SHORT).show();
                break;
            case R.id.touch:
                Toast.makeText(MainActivity.this,"點擊了碰我",Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SetView();

        teacherhi.add("阿阿阿");
        teacherhi.add("打球囉");
        teacherhi.add("要來上課");
        teacherhi.add("你好");


        teacherName.add("職位:"+"副教授"+"\n"+"李志成"+" "+"電話"+"037-381506");
        teacherName.add("職位:"+"助理教授"+"\n"+"陳博智"+" "+"電話"+"037-381516");
        teacherName.add("職位:"+"副教授"+"\n"+"溫敏淦"+" "+"電話"+"037-381514");
        teacherName.add("職位:"+"副教授"+"\n"+"楊宗珂"+" "+"電話"+"037-381517");

        teacherPicture.add(R.drawable.teacher09);
        teacherPicture.add(R.drawable.teacher10);
        teacherPicture.add(R.drawable.teacher06);
        teacherPicture.add(R.drawable.teacher08);



        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24); //返回按鈕
        toolbar.setTitle("Toolbar Example");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitle("test");
        toolbar.setSubtitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar); //才能完全在上面顯示

//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        }
//        );
        BaseAdapter baseAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return teacherName.size();
            }

            @Override
            public Object getItem(int position) {
                return position;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View view, ViewGroup viewGroup) {
                View tcLayout = View.inflate(MainActivity.this,R.layout.teacherlayout,null);
                ImageView imv_tcPicture = (ImageView) tcLayout.findViewById(R.id.imv_tc);
                TextView tv_tcName = (TextView) tcLayout.findViewById(R.id.tv_tc);    //找到框框餒的id位置
                TextView tv_tcName2 = (TextView) tcLayout.findViewById(R.id.tv_tc2);  //找到框框餒的id位置
                imv_tcPicture.setImageResource(teacherPicture.get(position));
                tv_tcName.setText(teacherName.get(position));  //顯示
                tv_tcName2.setText(teacherhi.get(position)); //顯示
                return tcLayout;
            }
        };
        lv_tc.setAdapter(baseAdapter);

        lv_tc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (teacherName.get(position).equals("職位:"+"副教授"+"\n"+"李志成"+" "+"電話"+"037-381506")) {
                    Toast.makeText(MainActivity.this, "志成老師共點擊了" + (a++) + "次" + " " + teacherName.get(position), Toast.LENGTH_SHORT).show();
                } else if (teacherName.get(position).equals("職位:"+"助理教授"+"\n"+"陳博智"+" "+"電話"+"037-381516")) {
                    Toast.makeText(MainActivity.this, "博智老師共點擊了" + (b++) + "次" + " " + teacherName.get(position), Toast.LENGTH_SHORT).show();
                } else if (teacherName.get(position).equals("職位:"+"副教授"+"\n"+"溫敏淦"+" "+"電話"+"037-381514")) {
                    Toast.makeText(MainActivity.this, "敏淦老師共點擊了" + (c++) + "次" + " " + teacherName.get(position), Toast.LENGTH_SHORT).show();
                } else if(teacherName.get(position).equals("職位:"+"副教授"+"\n"+"楊宗珂"+" "+"電話"+"037-381517")){
                    Toast.makeText(MainActivity.this, "宗珂老師共點擊了" + (d++) + "次" + " " + teacherName.get(position), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
};






