package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<String> skills=new ArrayList<>(Arrays.asList("Web","Java","Android"));
        ListView listView=(ListView) findViewById(R.id.listView);
        ArrayAdapter<String> adapter=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,skills);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("您選擇的是");
                builder.setMessage(skills.get(i));
                AlertDialog dialog=builder.create();
                dialog.show();
            }
        });
        Spinner spinner=(Spinner) findViewById(R.id.spinner);
        TextView tvOutput=(TextView) findViewById(R.id.tvOutput);
        tvOutput.setGravity(Gravity.CENTER);
        ArrayList<String> languages=new ArrayList<>(Arrays.asList("Select one","C++","Python","Java"));
        ArrayAdapter<String> adapter_other=new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,languages);
        spinner.setAdapter(adapter_other);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tvOutput.setText((i!=0)?languages.get(i):"");
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }
}