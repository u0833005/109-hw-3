package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        String output="";
        switch (item.getItemId())
        {
            case R.id.settings:
                output="設定";
                break;
            case R.id.help:
                output="幫助";
                break;
            case R.id.about:
                output="關於";
                break;
            case R.id.exit:
                output="離開";
                break;
        }
        if(output!="")
            Toast.makeText(this,"您點擊了"+output,Toast.LENGTH_SHORT).show();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<String> teacherNames=new ArrayList<>(Arrays.asList("温敏淦","陳博智","張朝旭"));
        ArrayList<String> teacherPhones=new ArrayList<>(Arrays.asList("037-381514","037-381516","037-381520"));
        ArrayList<String> teacherTitle=new ArrayList<>(Arrays.asList("副教授","助理教授","副教授 兼 圖書館系統管理組長"));
        ArrayList<Integer> teacherResId=new ArrayList<>(Arrays.asList(R.drawable.teacher06,R.drawable.teacher10,R.drawable.teacher05));
        ListView listView=(ListView) findViewById(R.id.listView);
        BaseAdapter adapter=new BaseAdapter() {
            @Override
            public int getCount() {
                return teacherNames.size();
            }

            @Override
            public Object getItem(int i) {
                return i;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View card=View.inflate(MainActivity.this,R.layout.card,null);
                ImageView ivImage=(ImageView) card.findViewById(R.id.ivImage);
                TextView tvName=(TextView) card.findViewById(R.id.tvName);
                TextView tvPhone=(TextView) card.findViewById(R.id.tvPhone);
                TextView tvTitle=(TextView) card.findViewById(R.id.tvTitle);
                ivImage.setImageResource(teacherResId.get(i));
                tvName.setText(teacherNames.get(i));
                tvTitle.setText(teacherTitle.get(i));
                tvPhone.setText("連絡電話:"+teacherPhones.get(i));
                return card;
            }
        };
        ArrayList<Integer> teacherClickCounts=new ArrayList<>(Arrays.asList(0,0,0));
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                teacherClickCounts.set(i,teacherClickCounts.get(i).intValue()+1);
                Toast.makeText(MainActivity.this,teacherNames.get(i)+"已被點擊"+teacherClickCounts.get(i)+"次了",Toast.LENGTH_SHORT).show();
            }
        });
    }
}