package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.widget.Toolbar;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar=(Toolbar) findViewById(R.id.toolbar);
        toolbar.setBackgroundColor(Color.BLUE);
        toolbar.setNavigationIcon(R.drawable.ic_launcher_foreground);
        toolbar.setTitle("0811");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitle("Toolbar Practice");
        toolbar.setSubtitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        ArrayList<String> teacherName=new ArrayList<>(Arrays.asList("溫敏淦","陳博智","張朝旭"));
        ArrayList<Integer> teacherImageId=new ArrayList<>(Arrays.asList(R.drawable.teacher06,R.drawable.teacher10,R.drawable.teacher05));
        ListView listViewTeacher=(ListView) findViewById(R.id.listViewTeacher);
        BaseAdapter adapter=new BaseAdapter() {
            @Override
            public int getCount() {
                return teacherName.size();
            }

            @Override
            public Object getItem(int i) {
                return i;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View teacherLayout=View.inflate(MainActivity.this,R.layout.card,null);
                ImageView ivAvatar=(ImageView) teacherLayout.findViewById(R.id.ivAvatar);
                TextView tvName=(TextView) teacherLayout.findViewById(R.id.tvName);
                ivAvatar.setImageResource(teacherImageId.get(i));
                tvName.setText(teacherName.get(i));
                return teacherLayout;
            }
        };
        listViewTeacher.setAdapter(adapter);
        listViewTeacher.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(MainActivity.this,teacherName.get(i),Toast.LENGTH_SHORT).show();
            }
        });
    }
}