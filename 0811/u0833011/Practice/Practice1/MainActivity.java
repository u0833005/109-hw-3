package com.example.myapplication;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView=(ListView) findViewById(R.id.listview);
        String[] data={"一","二","三","四"};
        ArrayAdapter<String> adapter=new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,data);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(MainActivity.this,data[i],Toast.LENGTH_SHORT).show();
            }
        });

        Spinner spinner=(Spinner) findViewById(R.id.spinner);
        ArrayList<String> subject=new ArrayList<>(Arrays.asList("請選擇科目","資料庫","統計","管理學","計概"));
        ArrayAdapter<String> adapter_other=new ArrayAdapter<>(this, android.R.layout.simple_spinner_item,subject);
        spinner.setAdapter(adapter_other);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i!=0)
                    Toast.makeText(MainActivity.this,subject.get(i),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(MainActivity.this,"Nothing",Toast.LENGTH_SHORT).show();
            }
        });
        Button btnClear=(Button) findViewById(R.id.btnClear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                subject.clear();
                adapter_other.notifyDataSetChanged();
            }
        });

        Button btnDialog1=(Button) findViewById(R.id.btnDialog1);
        btnDialog1.setOnClickListener(view -> {
            AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Prompt");
            builder.setMessage("This is a prompt dialog.");
            DialogInterface.OnClickListener clickListener=new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    switch (i)
                    {
                        case DialogInterface.BUTTON_POSITIVE:
                            Toast.makeText(MainActivity.this,"You click 'OK'.",Toast.LENGTH_SHORT).show();
                            break;
                        case DialogInterface.BUTTON_NEGATIVE:
                            Toast.makeText(MainActivity.this,"You click 'Cancel'.",Toast.LENGTH_SHORT).show();
                            break;
                        case DialogInterface.BUTTON_NEUTRAL:
                            Toast.makeText(MainActivity.this,"You click 'Ignore'.",Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            };
            builder.setPositiveButton("OK",clickListener);
            builder.setNegativeButton("Cancel",clickListener);
            builder.setNeutralButton("Ignore",clickListener);
            AlertDialog dialog=builder.create();
            dialog.show();
        });
        Button btnDialog2=(Button) findViewById(R.id.btnDialog2);
        btnDialog2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("今天午餐要吃什麼");
                String[] lunch={"滷肉飯","蛋包飯","火腿炒飯","牛肉麵"};
                builder.setItems(lunch, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this,lunch[i],Toast.LENGTH_SHORT).show();
                    }
                });
                AlertDialog dialog=builder.create();
                dialog.show();
            }
        });

    }
}