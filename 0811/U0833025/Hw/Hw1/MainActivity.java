package com.example.hw1_0811;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView list;
    Spinner spinner;
    TextView text;

    ArrayList<String> Name = new ArrayList<>();
    String subject = new String();
    String[] datas={"請選擇","山河錯落","你是人間星光","星河滾燙","你是人間夢想"};

    public void setView(){
        list = findViewById(R.id.list);
        spinner = findViewById(R.id.spinner);
        text = findViewById(R.id.text);
    }

    public void setData(){
        Name.add("蒲熠星");
        Name.add("郭文韜");
        Name.add("齊思鈞");
        Name.add("周峻緯");
        Name.add("唐九洲");
        Name.add("邵明明");
        Name.add("曹恩齊");
        Name.add("何運晨");
        Name.add("石凱");
        Name.add("Timo");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setView();
        setData();

        ArrayAdapter<String> Arrayadapter = new ArrayAdapter<>(MainActivity.this,android.R.layout.simple_list_item_1 ,Name);

            list.setAdapter(Arrayadapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Log.e( "點擊事件","點擊了第"+position+"個選項");
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage(Name.get(position));
                builder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("確定","run");
                        Toast toast = Toast.makeText(MainActivity.this,"確定",Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("取消","run");
                        Toast toast = Toast.makeText(MainActivity.this,"取消",Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                    }
                });
                builder.setNeutralButton("忽略", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("忽略","run");
                        Toast toast = Toast.makeText(MainActivity.this,"忽略",Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(MainActivity.this,android.R.layout.simple_list_item_1 ,datas);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                int now_position = position+1;
                if(position!= 0){
                    text.setText(datas[position]);
                    subject = datas[position];
                }else {
                    text.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.e("onNothingSelected","執行");
            }
        });
    }
}