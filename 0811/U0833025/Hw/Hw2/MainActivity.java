package com.example.hw2_0811;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    ListView listview;
    ArrayList<String> Name = new ArrayList<>();
    ArrayList<String> Info = new ArrayList<>();
    ArrayList<Integer> Picture = new ArrayList<>();

    public void setView(){
        toolbar = findViewById(R.id.toolbar);
        listview = findViewById(R.id.listview);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            case R.id.course:
                Toast.makeText(MainActivity.this,"點擊了課程",Toast.LENGTH_SHORT).show();
                break;
            case R.id.tools:
                Toast.makeText(MainActivity.this,"點擊了工具",Toast.LENGTH_SHORT).show();
                break;
            case R.id.setting:
                Toast.makeText(MainActivity.this,"點擊了設定",Toast.LENGTH_SHORT).show();
                break;
            case R.id.help:
                Toast.makeText(MainActivity.this,"點擊了幫助",Toast.LENGTH_SHORT).show();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setView();

        Name.add("溫敏淦");
        Name.add("陳博智");
        Name.add("李志成");
        Name.add("張朝旭");
        Name.add("楊宗珂");
        Name.add("陳士杰");


        Info.add("電話：\t037-381514"+"\nEmail：\tmgwen@gm.nuu.edu.tw");
        Info.add("電話：\t037-381516"+"\nEmail：\tpcchen@nuu.edu.tw");
        Info.add("電話：\t037-381506"+"\nEmail：\tcclee@nuu.edu.tw");
        Info.add("電話：\t037-381520"+"\nEmail：\tcschang@nuu.edu.tw");
        Info.add("電話：\t037-381517"+"\nEmail：\tckyoung@nuu.edu.tw");
        Info.add("電話：\t037-381511"+"\nEmail：\tsjchen@nuu.edu.tw");

        Picture.add(R.drawable.mgwen);
        Picture.add(R.drawable.pcchen);
        Picture.add(R.drawable.cclee);
        Picture.add(R.drawable.cschang);
        Picture.add(R.drawable.ckyoung);
        Picture.add(R.drawable.sjchen);

        toolbar.setNavigationIcon(R.drawable.arrow_back);
        toolbar.setTitle("0811Hw2");
        toolbar.setSubtitle("chiayuan");
        setSupportActionBar(toolbar);

        BaseAdapter baseAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return Name.size();
            }

            @Override
            public Object getItem(int position) {
                return position;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View view, ViewGroup viewGroup) {
                View tcLayout = View.inflate(MainActivity.this,R.layout.teacher,null);
                ImageView imageView = (ImageView) tcLayout.findViewById(R.id.image);
                TextView textView = (TextView) tcLayout.findViewById(R.id.text);

                imageView.setImageResource(Picture.get(position));
                textView.setText(Name.get(position)+"\n"+Info.get(position));
                return tcLayout;
            }

        };
        listview.setAdapter(baseAdapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            int[] count = new int[6];
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                count[position]++;
                Toast.makeText(MainActivity.this,"點擊了"+Name.get(position)+"老師"+count[position]+"次",Toast.LENGTH_SHORT).show();
            }
        });
    }
}