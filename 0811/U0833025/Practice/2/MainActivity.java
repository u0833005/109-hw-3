package com.example.new_object_0811;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    ListView listView;
    ArrayList<String> teacherName = new ArrayList<>();
    ArrayList<Integer> teacherPicture = new ArrayList<>();


    public void SetView(){
        toolbar = findViewById(R.id.toolbar);
        listView = findViewById(R.id.listview);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_example,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
            case R.id.setting:
                Toast.makeText(MainActivity.this,"點擊了設定",Toast.LENGTH_SHORT).show();
                break;
            case R.id.help:
                Toast.makeText(MainActivity.this,"點擊了幫助",Toast.LENGTH_SHORT).show();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SetView();

        teacherName.add("陳博智");
        teacherName.add("李志成");
        teacherName.add("溫敏淦");

        teacherPicture.add(R.drawable.chen);
        teacherPicture.add(R.drawable.li);
        teacherPicture.add(R.drawable.wen);

        toolbar.setNavigationIcon(R.drawable.arrow_back);
        toolbar.setTitle("Toolbar Example");
        toolbar.setSubtitle("test");
        setSupportActionBar(toolbar);

//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
        BaseAdapter baseAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return teacherName.size();
            }

            @Override
            public Object getItem(int position) {
                return position;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View view, ViewGroup viewGroup) {
                View tcLayout = View.inflate(MainActivity.this,R.layout.teacherlayout,null);
                ImageView imageView = (ImageView) tcLayout.findViewById(R.id.image);
                TextView textView = (TextView) tcLayout.findViewById(R.id.text);

                imageView.setImageResource(teacherPicture.get(position));
                textView.setText(teacherName.get(position));
                return tcLayout;
            }

        };
        listView.setAdapter(baseAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(MainActivity.this,"點擊了"+teacherName.get(position)+"老師",Toast.LENGTH_SHORT).show();
            }
        });
    }
}