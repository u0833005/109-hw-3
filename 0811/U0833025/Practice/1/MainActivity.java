package com.example.object_0811;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    Spinner spinner;
    Button button;
    TextView textView;

    ArrayList<String> data = new ArrayList<>();
    String subject = new String();
    String[] datas={"肉燥飯","雞腿飯","控肉飯","排骨飯"};

    public void SetView(){
        listView = findViewById(R.id.listview);
        button = findViewById(R.id.button);
        spinner = findViewById(R.id.spinner);
        textView = findViewById(R.id.textView);
    }

    public void SetData(){
        data.add("資料庫程式設計");
        data.add("統計學");
        data.add("計算機概論");
        data.add("管理學");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SetView();
        SetData();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(MainActivity.this,android.R.layout.simple_list_item_1 ,data);

//                listView.setAdapter(arrayAdapter);
//
////        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(MainActivity.this,android.R.layout.simple_list_item_multiple_choice ,data);
////        listView.setAdapter(arrayAdapter);
////        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                Log.e( "點擊事件","點擊了第"+position+"個選項");
//            }
//        });

        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                int now_position = position+1;
                textView.setText("點擊了第"+position+"個選項，的內容"+data.get(position));
                subject = data.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.e("onNothingSelected","執行");
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                data.clear();   //清空
//                arrayAdapter.notifyDataSetChanged();    //更新

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("提示");
//                builder.setMessage(subject);
                builder.setItems(datas, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        textView.setText("我午餐想吃"+datas[which]);
                    }
                });

                builder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("確定","run");
                        Toast toast = Toast.makeText(MainActivity.this,"確定",Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("取消","run");
                    }
                });
                builder.setNeutralButton("忽略", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("忽略","run");
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });


    }
}