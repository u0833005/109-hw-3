package com.example.a0811p1;


import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listview;
    Button clear;
    Spinner spinner;
    TextView show;
    ArrayList<String> data = new ArrayList<>();
    String subject;
    String[] datas={"肉燥飯","雞腿飯","控肉飯","排骨飯"};



    public void SetView() {
        listview = findViewById(R.id.listview);
        clear = findViewById(R.id.clear);
        spinner = findViewById(R.id.spinner);
        show = findViewById(R.id.show);
    }

    public void Setdata() {
        data.add("資料庫程式設計");
        data.add("統計學");
        data.add("計算機概論");
        data.add("管理學");

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SetView();
        Setdata();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_checked, data);
//                listview.setAdapter(arrayAdapter);
//                listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
//                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//                        int now = position+1;
//                        Log.e("點擊事件","點擊了第"+position+"個選項");
//                    }
//                });

        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                int now_position = position + 1;
                show.setText("點擊了第" + now_position + "個選項,內容是" + data.get(position));
                subject="清除了"+data.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.e("nothing","執行");
            }


        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                    data.clear();
//                    arrayAdapter.notifyDataSetChanged();
                AlertDialog.Builder b=new AlertDialog.Builder(MainActivity.this);
                b.setTitle("標題");
//                b.setMessage(subject);
                b.setItems(datas, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        show.setText("我午餐要吃"+datas[i]);

                    }
                });
                b.setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
//                        Log.e("確定","run");
                        Toast toast=Toast.makeText(MainActivity.this,"中間->"+subject,Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.CENTER,0,0);
                        toast.show();
                    }
                });
                b.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
//                        Log.e("取消","run");
                        Toast toast=Toast.makeText(MainActivity.this,"左邊",Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.LEFT,0,0);
                        toast.show();
                    }
                });
                b.setNeutralButton("忽略", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("忽略","run");
                        Toast toast=Toast.makeText(MainActivity.this,"右邊",Toast.LENGTH_LONG);
                        toast.setGravity(Gravity.RIGHT,0,0);
                        toast.show();
                    }
                });
                AlertDialog dialog=b.create();
                dialog.show();
            }
        });
    }
}