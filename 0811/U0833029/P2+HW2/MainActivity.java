package com.example.a0811p2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    int a=0;
    int b=0;
    int c=0;
    int d=0;
    int e=0;
    Toolbar toolbar;
    ArrayList<String> teacherName=new ArrayList<>();
    ArrayList<String> teacherPhone=new ArrayList<>();
    ArrayList<String> teacherPosition=new ArrayList<>();
    ArrayList<Integer> teacherPicture=new ArrayList<>();
    ListView lv_tcList;
    public void SetView(){
        toolbar=findViewById(R.id.toolbar);
        lv_tcList=findViewById(R.id.lv_tcList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_example,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
            case R.id.setting:
                Toast.makeText(MainActivity.this,"點擊了設定",Toast.LENGTH_SHORT).show();
                break;
            case R.id.help:
                Toast.makeText(MainActivity.this,"點擊了幫助",Toast.LENGTH_SHORT).show();
                break;
            case R.id.hello:
                Toast.makeText(MainActivity.this,"點擊了Hello",Toast.LENGTH_SHORT).show();
                break;
            case R.id.hi:
                Toast.makeText(MainActivity.this,"點擊了Hi",Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SetView();
        teacherName.add("鎮東");
        teacherName.add("朝旭");
        teacherName.add("敏幹");
        teacherName.add("鴨鴨");
        teacherName.add("博志");
        teacherPhone.add("聯絡電話:037-381515");
        teacherPhone.add("聯絡電話:037-381520");
        teacherPhone.add("聯絡電話:037-381514");
        teacherPhone.add("聯絡電話:037-381506");
        teacherPhone.add("聯絡電話:037-381516");
        teacherPosition.add("教授 兼 管理學院院長");
        teacherPosition.add("副教授 兼 圖書館系統管理組長");
        teacherPosition.add("副教授");
        teacherPosition.add("副教授");
        teacherPosition.add("助理教授");
        teacherPicture.add(R.drawable.teacher03);
        teacherPicture.add(R.drawable.teacher05);
        teacherPicture.add(R.drawable.teacher06);
        teacherPicture.add(R.drawable.teacher09);
        teacherPicture.add(R.drawable.teacher10);


        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setTitle("Toolbar Example");
        toolbar.setTitleTextColor(Color.RED);
        toolbar.setSubtitle("test");
        toolbar.setSubtitleTextColor(Color.GREEN);
        setSupportActionBar(toolbar);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
        BaseAdapter baseAdapter=new BaseAdapter() {
            @Override
            public int getCount() {
                return teacherName.size();
            }

            @Override
            public Object getItem(int position) {
                return position;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View view, ViewGroup viewGroup) {
                View tcLayout=View.inflate(MainActivity.this,R.layout.teacherlayout,null);
                ImageView imv_tcPicture=(ImageView) tcLayout.findViewById(R.id.imv_tc);
                TextView tv_tcName=(TextView) tcLayout.findViewById(R.id.tv_tcname);
                TextView tv_tcphone=(TextView) tcLayout.findViewById(R.id.tv_tcphone);
                TextView tv_tcposition=(TextView) tcLayout.findViewById(R.id.tv_tcposition);
                imv_tcPicture.setImageResource(teacherPicture.get(position));
                tv_tcName.setText(teacherName.get(position));
                tv_tcphone.setText(teacherPhone.get(position));
                tv_tcposition.setText(teacherPosition.get(position));
                return tcLayout;
            }
        };
        lv_tcList.setAdapter(baseAdapter);
        lv_tcList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if(teacherName.get(position).equals("鎮東")){
                    a++;
                    Toast.makeText(MainActivity.this,"點擊了"+a+"次"+teacherName.get(position),Toast.LENGTH_SHORT).show();
                }else if(teacherName.get(position).equals("朝旭")){
                    b++;
                    Toast.makeText(MainActivity.this,"點擊了"+b+"次"+teacherName.get(position),Toast.LENGTH_SHORT).show();
                }else if(teacherName.get(position).equals("敏幹")){
                    c++;
                    Toast.makeText(MainActivity.this,"點擊了"+c+"次"+teacherName.get(position),Toast.LENGTH_SHORT).show();
                }else if(teacherName.get(position).equals("鴨鴨")){
                    d++;
                    Toast.makeText(MainActivity.this,"點擊了"+d+"次"+teacherName.get(position),Toast.LENGTH_SHORT).show();
                }else if(teacherName.get(position).equals("博志")){
                    e++;
                    Toast.makeText(MainActivity.this,"點擊了"+e+"次"+teacherName.get(position),Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}