package com.example.a0830;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.TextView;


public class MainActivity3 extends AppCompatActivity implements LocationListener{
    TextView localTextview,updateTextview;
    LocationManager locationManager;
    String bestLocation = null;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        localTextview = findViewById(R.id.localTextview);
        updateTextview = findViewById(R.id.updateTextview);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        bestLocation = locationManager.getBestProvider(new Criteria(),true);

        if(bestLocation.equals(LocationManager.PASSIVE_PROVIDER)){
            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }else{
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED){
                return;
            }
            locationManager.requestLocationUpdates(bestLocation,0,0 , this);
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        count++;
        Log.d(TAG,"onLocationChanged"+location.getLatitude()+","+location.getLongitude());
        localTextview.setText(location.getLatitude()+","+location.getLongitude());
        updateTextview.setText("更新次數"+count);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(TAG,"onStatusChanged");
    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {
        Log.d(TAG,"onProviderEnabled");
    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {
        Log.d(TAG,"onProviderDisabled");
    }
}