package com.example.a0830;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    TextToSpeech tts;
    Button btn;
    EditText edTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = findViewById(R.id.btn);
        edTv = findViewById(R.id.edTv);

        tts = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == tts.SUCCESS){
                    int result = tts.setLanguage(Locale.CHINESE);
                    tts.setPitch(0.5f);
                    tts.setSpeechRate(1);
                    if(result != TextToSpeech.LANG_COUNTRY_AVAILABLE && result != TextToSpeech.LANG_AVAILABLE){
                        Toast.makeText(MainActivity.this,"TTS不支援",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tts.speak(edTv.getText().toString(),TextToSpeech.QUEUE_ADD,null);
            }
        });
    }

    public void OnDestroy(){
        if(tts != null){
            tts.stop();
            tts.shutdown();
        }
        MainActivity.super.onDestroy();
    }
}