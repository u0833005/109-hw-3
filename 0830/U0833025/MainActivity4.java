package com.example.a0830;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

public class MainActivity4 extends AppCompatActivity {
    TextView tv_location, tv_update;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private Location lastlocation;
    private int count = 0;
    private final String TAG = "FusedActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        tv_location = findViewById(R.id.TV_location);
        tv_update = findViewById(R.id.TV_update);
        initFusedLocationProviderClient();
        initLocationCallback();
        initLocationRequest();
    }

    public void initFusedLocationProviderClient() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    lastlocation = location;
                }
            }
        });
    }

    private void initLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(5 * 1000);
        locationRequest.setFastestInterval(2 * 1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }

    private void initLocationCallback() {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);
                count++;
                for (Location location : locationResult.getLocations()) {
                    Log.d(TAG, location.toString());
                    lastlocation = location;
                    tv_location.setText(location.getLatitude() + "," + location.getLongitude());
                    tv_update.setText("更新次數："+count);
                }
                Uri mapUri = Uri.parse("google.streetview:cbll："+lastlocation.getLatitude()+","+lastlocation.getLongitude());
                // Uri mapUri = Uri.parse("google.streetview:cbll："+lastlocation.getLatitude()+","+lastlocation.getLongitude()+"&cbp=0,30,0,0,-15");
                //cbp=>0,bearing,0,zoom,tilt
                //Uri mapUri = Uri.parse("geo:"+lastlocation.getLatitude()+","+lastlocation.getLongitude()+"?z=20");
                //z=>縮放
                Intent intent = new Intent(Intent.ACTION_VIEW,mapUri);
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }
}