package com.example.myapplication;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private TextToSpeech myTTS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText etInput=findViewById(R.id.etInput);
        Button btnTTS=findViewById(R.id.btnTTS);
        Button btnSTT=findViewById(R.id.btnSTT);

        myTTS=new TextToSpeech(this, i -> {
            if(i==TextToSpeech.SUCCESS)
            {
                int result=myTTS.setLanguage(Locale.CHINESE);
                myTTS.setPitch(0.5f);
                myTTS.setSpeechRate(1.3f);
                if(result<TextToSpeech.SUCCESS)
                    Toast.makeText(MainActivity.this,result+" TTS暫不支援此語言的朗讀",Toast.LENGTH_SHORT).show();
            }
        });
        ActivityResultLauncher<Intent> myLauncher=registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if(result.getResultCode() == Activity.RESULT_OK)
                    {
                        Intent data = result.getData();
                        assert data != null;
                        ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        etInput.setText(text.get(0));
                    }
                });
        btnTTS.setOnClickListener(view -> {
            String input=etInput.getText().toString();
            myTTS.speak(input,TextToSpeech.QUEUE_ADD,null,null);
        });
        btnSTT.setOnClickListener(view -> {
            Intent intent=new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE,Locale.getDefault());
            try {
                myLauncher.launch(intent);
            }
            catch (Exception e)
            {
                Log.e("error",e.getMessage());
                Toast.makeText(MainActivity.this,"您的設備不支援STT",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        if(myTTS!=null)
        {
            myTTS.stop();
            myTTS.shutdown();
        }
        super.onDestroy();
    }
}