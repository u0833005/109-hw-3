package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    private int count;
    private TextView tvCoordinate;
    private TextView tvUpdate;
    private LocationManager manager;

    private int count2;
    private TextView tvCoordinate2;
    private TextView tvUpdate2;

    private EditText etLatitude;
    private EditText etLongitude;
    private EditText etFacility;
    private Button btnCurrent;
    private Button btnMap;
    private Button btnStreetView;

    private EditText etLandmark;
    private Button btnSearch;

    private EditText etDestination;
    private Button btnNavigation;

    private EditText etAddress;
    private Button btnGeoencoding;

    private EditText etOrigin;
    private EditText etDest;
    private Button btnDirection;

    private Handler mainHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvCoordinate = findViewById(R.id.tvCoordinate);
        tvUpdate = findViewById(R.id.tvUpdate);
        tvCoordinate2 = findViewById(R.id.tvCoordinate2);
        tvUpdate2 = findViewById(R.id.tvUpdate2);
        etLatitude = findViewById(R.id.etLatitude);
        etLongitude = findViewById(R.id.etLongitude);
        etFacility = findViewById(R.id.etFacility);
        btnCurrent = findViewById(R.id.btnCurrent);
        btnMap = findViewById(R.id.btnMap);
        btnStreetView = findViewById(R.id.btnStreetView);
        etDestination=findViewById(R.id.etDestination);
        btnNavigation=findViewById(R.id.btnNavigation);
        etLandmark=findViewById(R.id.etLandmark);
        btnSearch=findViewById(R.id.btnSearch);
        etAddress=findViewById(R.id.etAddress);
        btnGeoencoding=findViewById(R.id.btnGeoencoding);
        etOrigin=findViewById(R.id.etOrigin);
        etDest=findViewById(R.id.etDest);
        btnDirection=findViewById(R.id.btnDirection);

        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        mainHandler=new Handler(Looper.myLooper())
        {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                if (msg.what == 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("路線規劃");
                    builder.setMessage((String) msg.obj);
                    builder.create().show();
                } else {
                    Toast.makeText(MainActivity.this, msg.obj.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            return;
        manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, location -> {
            tvCoordinate.setText(String.format("%s,%s", location.getLatitude(), location.getLongitude()));
            count++;
            String text = "更新次數";
            tvUpdate.setText(String.format("%s %s", text, count));
        });

        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
        LocationRequest request = LocationRequest.create();
        request.setInterval(5000);
        request.setFastestInterval(2000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        client.requestLocationUpdates(request, new LocationCallback() {
            @Override
            public void onLocationResult(@NonNull LocationResult locationResult) {
                super.onLocationResult(locationResult);
                locationResult.getLocations().forEach(location -> {
                    tvCoordinate2.setText(String.format("%s,%s", location.getLatitude(), location.getLongitude()));
                    count2++;
                    String text = "更新次數";
                    tvUpdate2.setText(String.format("%s %s", text, count2));
                });
            }
        }, Looper.myLooper());
        btnCurrent.setOnClickListener(view -> {
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            client.getLastLocation().addOnSuccessListener(location -> {
                etLatitude.setText(String.valueOf(location.getLatitude()));
                etLongitude.setText(String.valueOf(location.getLongitude()));
            });
        });
        btnMap.setOnClickListener(view -> {
            try
            {
                String addition=(!etFacility.getText().toString().equals(""))?"?q="+etFacility.getText().toString():"";
                Uri uri=Uri.parse("geo:"+etLatitude.getText().toString()+","+etLongitude.getText().toString()+addition);
                Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            } catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        });
        btnSearch.setOnClickListener(view -> {
            try
            {
                Uri uri=Uri.parse("geo:?q="+etLandmark.getText().toString());
                Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        });
        btnStreetView.setOnClickListener(view -> {
            try
            {
                Uri uri=Uri.parse("google.streetview:cbll="+etLatitude.getText().toString()+","+etLongitude.getText().toString()+"&cbp=0,90,0,1,-90");
                Intent intent=new Intent(Intent.ACTION_VIEW,uri);
                intent.setPackage("com.google.android.apps.maps");
                startActivity(intent);
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        });
        btnNavigation.setOnClickListener(view -> {
            Uri uri=Uri.parse("google.navigation:q="+etDestination.getText().toString()+"&mode=d&avoid=h");
            Intent intent=new Intent(Intent.ACTION_VIEW,uri);
            intent.setPackage("com.google.android.apps.maps");
            startActivity(intent);
        });
        btnGeoencoding.setOnClickListener(view -> new Thread(() -> {
            try
            {
                URL url=new URL("https://maps.googleapis.com/maps/api/geocode/json?address="+etAddress.getText().toString()+"&key="+getString(R.string.MAPS_API_KEY));
                HttpsURLConnection connection=(HttpsURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                if(connection.getResponseCode()==HttpsURLConnection.HTTP_OK)
                {
                    InputStream is=connection.getInputStream();
                    BufferedReader br=new BufferedReader(new InputStreamReader(is));
                    StringBuilder src= new StringBuilder();
                    String temp;
                    while((temp=br.readLine())!=null)
                        src.append(temp);
                    JSONArray results=new JSONObject(src.toString()).getJSONArray("results");
                    JSONObject result=results.getJSONObject(0);
                    JSONObject geometry=result.getJSONObject("geometry");
                    JSONObject location=geometry.getJSONObject("location");
                    String lat=location.getString("lat");
                    String lng=location.getString("lng");
                    mainHandler.sendMessage(mainHandler.obtainMessage(0,"經度:"+lng+",緯度:"+lat));
                }
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }).start());
        btnDirection.setOnClickListener(view -> new Thread(() -> {
            try
            {
                URL url=new URL("https://maps.googleapis.com/maps/api/directions/json?origin="+etOrigin.getText().toString()+"&destination="+etDest.getText().toString()+"&language=zh_TW&key="+getString(R.string.MAPS_API_KEY));
                HttpsURLConnection connection=(HttpsURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                if(connection.getResponseCode()==HttpsURLConnection.HTTP_OK)
                {
                    InputStream is=connection.getInputStream();
                    BufferedReader br=new BufferedReader(new InputStreamReader(is));
                    StringBuilder src= new StringBuilder();
                    String temp;
                    while((temp=br.readLine())!=null)
                        src.append(temp);
                    JSONObject route=new JSONObject(src.toString()).getJSONArray("routes").getJSONObject(0);
                    JSONObject leg=route.getJSONArray("legs").getJSONObject(0);
                    JSONArray steps=leg.getJSONArray("steps");
                    StringBuilder output= new StringBuilder();
                    String htmlTagPattern="<[^>]+>";
                    for(int i=0;i<steps.length();i++)
                    {
                        JSONObject step=steps.getJSONObject(i);
                        output.append(step.getString("html_instructions").replaceAll(htmlTagPattern, "")).append("\n\n");
                    }
                    mainHandler.sendMessage(mainHandler.obtainMessage(1, output.toString()));
                }
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }).start());
    }
}