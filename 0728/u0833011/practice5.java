public class practice5
{
    //01 Knapsack Problem
    public static int[][] knapsackProblem(int[][] itemData,int weight)
    {
        int itemNum=itemData.length;
        int[][] packsData=new int[itemNum][weight];
        for(int i=0;i<weight;i++) //initial first item for all knapsack.weight <= weight
            packsData[0][i]=(i+1-itemData[0][0]<0)?0:itemData[0][1];
        for(int i=1;i<itemNum;i++)
        {
            for(int j=0;j<weight;j++)
            {
                if(j+1-itemData[i][0]<0)
                    packsData[i][j]=packsData[i-1][j];
                else
                {
                    int pre=packsData[i-1][j];
                    int cur=(j-itemData[i][0]<0)?itemData[i][1]:packsData[i-1][j-itemData[i][0]]+itemData[i][1];
                    packsData[i][j] = Math.max(pre,cur);
                }
            }
        }
        return packsData;
    }
    public static void main(String[] args)
    {
        int[][] itemData={ //weight,value
                {1,2},{2,4},{3,4},{4,5}
        };
        int weight=5;
        int[][] packsData=knapsackProblem(itemData,weight);
        for(int[] i:packsData)
        {
            for(int j:i)
                System.out.print(j+" ");
            System.out.println();
        }
        System.out.println("output:"+packsData[itemData.length-1][weight-1]);
        System.out.println();
        itemData=new int[][] {
                {5,8},{32,47},{17,43},{7,9},{6,4},
                {29,40},{2,6},{14,31},{6,17},{1,3}
        };
        weight=100;
        packsData=knapsackProblem(itemData,weight);
        for(int[] i:packsData)
        {
            for(int j:i)
                System.out.print(j+" ");
            System.out.println();
        }
        System.out.println("output:"+packsData[itemData.length-1][weight-1]);
    }
}
