import java.util.*;

public class practice1
{
    static class Node
    {
        private int data;
        private Node leftChild;
        private Node rightChild;

        public Node()
        {
            this.data=0;
            this.leftChild=null;
            this.rightChild=null;
        }
        public Node(int data)
        {
            this.data=data;
            this.leftChild=null;
            this.rightChild=null;
        }
        public String toString()
        {
            return "data: "+this.data;
        }
    }
    static class BST
    {
        private Node root;
        public BST()
        {
            root=null;
        }
        public BST(Node root)
        {
            this.root=root;
        }
        public boolean insertNode(int data)
        {
            Node previous=null;
            Node current=this.root;
            while(current!=null)
            {
                previous=current;
                if(data<current.data)
                    current=current.leftChild;
                else if(data>current.data)
                    current=current.rightChild;
                else
                    return false;
            }
            current=new Node(data);
            if(this.root!=null)
            {
                if(data<previous.data)
                    previous.leftChild=current;
                else
                    previous.rightChild=current;
            }
            else
                this.root=current;
            return true;
        }
        private void preorder(Node current)
        {
            if(current!=null)
            {
                visit(current);
                preorder(current.leftChild);
                preorder(current.rightChild);
            }
        }
        private void inorder(Node current)
        {
            if(current!=null)
            {
                inorder(current.leftChild);
                visit(current);
                inorder(current.rightChild);
            }
        }
        private void postorder(Node current)
        {
            if(current!=null)
            {
                postorder(current.leftChild);
                postorder(current.rightChild);
                visit(current);
            }
        }
        public void preorder()
        {
            preorder(this.root);
        }
        public void inorder()
        {
            inorder(this.root);
        }
        public void postorder()
        {
            postorder(this.root);
        }
        public void inorder(int i)
        {
            if(this.root==null)
                return;
            Stack<Node> stack=new Stack<Node>();
            Node current=this.root;
            while(current!=null||stack.size()>0)
            {
                while(current!=null)
                {
                    stack.push(current);
                    current=current.leftChild;
                }
                current=stack.pop();
                visit(current);
                current=current.rightChild;
            }
        }
        public void visit(Node a)
        {
            System.out.print(a.data+",");
        }
        public boolean dfs(int data,Node current)
        {
            if(current!=null) //postorder
            {
                if(current.data==data) //visit
                    return true;
                if(dfs(data,current.leftChild))
                    return true;
                if(dfs(data,current.rightChild))
                    return true;
            }
            return false;
        }
        public boolean dfs(int data)
        {
            return dfs(data,this.root);
        }
        public boolean bfs(int data)
        {
            List<Node> output=new ArrayList<Node>();
            Queue<Node> queue=new LinkedList<>();
            queue.offer(root);
            while(!queue.isEmpty())
            {
                Node temp=queue.poll();
                if(temp.data==data)
                    return true;
                if(temp.leftChild!=null)
                    queue.offer(temp.leftChild);
                if(temp.rightChild!=null)
                    queue.offer(temp.rightChild);
                output.add(temp);
            }
            return false;
        }
    }
    public static void main(String[] args)
    {
        BST tree=new BST();
        tree.insertNode(11);
        tree.insertNode(6);
        tree.insertNode(3);
        tree.insertNode(7);
        tree.insertNode(20);
        tree.insertNode(15);
        tree.insertNode(33);
        System.out.print("Preorder: ");
        tree.preorder();
        System.out.println();
        System.out.print("Inorder: ");
        tree.inorder();
        System.out.println();
        System.out.print("Postorder: ");
        tree.postorder();
        System.out.println();
        System.out.println("DFS: ");
        System.out.println(tree.dfs(15)?"Find!":"Not find:(");
        System.out.println(tree.dfs(1)?"Find!":"Not find:(");
        System.out.println("BFS:\n"+(tree.bfs(15)?"Find!":"Not find:("));
        System.out.println(tree.bfs(1)?"Find!":"Not find:(");
    }
}
