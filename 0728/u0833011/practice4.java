import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

public class practice4
{
    public static int[] twoSum(int[] nums,int target)
    {
        Map<Integer,Integer> map=new HashMap<>();
        for(int i=0;i<nums.length;i++)
        {
            int complement=target-nums[i];
            if(map.containsKey(complement))
                return new int[] {map.get(complement),i};
            map.put(nums[i],i);
        }
        return new int[] {};
    }
    public static void main(String[] args) {
        int[] nums={2,7,11,15};
        System.out.println(Arrays.toString(nums)+" target: "+9+"\n"+Arrays.toString(twoSum(nums,9))+"\n");

        nums=new int[] {1,9,11,15};
        System.out.println(Arrays.toString(nums)+" target: "+10+"\n"+Arrays.toString(twoSum(nums,10))+"\n");

        nums=new int[] {3,2,4};
        System.out.println(Arrays.toString(nums)+" target: "+6+"\n"+Arrays.toString(twoSum(nums,6)));
    }
}
