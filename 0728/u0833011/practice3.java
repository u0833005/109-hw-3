import java.util.List;
import java.util.LinkedList;

public class practice3
{
    static class Point
    {
        private final int x;
        private final int y;
        public Point(int x,int y)
        {
            this.x=x;
            this.y=y;
        }
        public String toString()
        {
            return "point("+this.x+","+this.y+")";
        }
    }
    static class MapBlock
    {
        public static int empty=0;
        public static int walked=2;
        public static int wall=1;
        public static int end=9; //line 116
        private int type;
        public MapBlock(int type)
        {
            this.type=type;
        }
        public boolean canGo()
        {
            return this.type!=walked && this.type!=wall;
        }
    }
    static class Map
    {
        private final MapBlock[][] mapData;
        private final int mapHeight;
        private final int mapWidth;
        private int startX;
        private int startY;
        private int endX;
        private int endY;
        private final List<Point> tempList=new LinkedList<>();
        private final List<Point> routeList=new LinkedList<>();
        public Map(int[][] mapData,int h,int w)
        {
            this.mapHeight=h;
            this.mapWidth=w;
            this.mapData=new MapBlock[h][w];
            for(int i=0;i<h;i++)
            {
                for(int j=0;j<w;j++)
                {
                    if(mapData[i][j]==8)
                    {
                        startX=i;
                        startY=j;
                    }
                    if(mapData[i][j]==9)
                    {
                        endX=i;
                        endY=j;
                    }
                    MapBlock tempBlock=new MapBlock(mapData[i][j]);
                    this.mapData[i][j]=tempBlock;
                }
            }
            System.out.println(startX+","+startY);
            System.out.println(endX+","+endY);
        }
        void dfs(int x,int y) //
        {
            int[][] direction={
                    {0,1},{1,0},{0,-1},{-1,0}
            };
            if(x==endX&&y==endY)
            {
                routeList.addAll(tempList);
                ((LinkedList<Point>)(routeList)).removeLast();
                return;
            }
            for(int i=0;i<4;i++)
            {
                int nextX=x+direction[i][0];
                int nextY=y+direction[i][1];
                if(nextX<0||nextX>this.mapHeight-1||nextY<0||nextY>this.mapWidth)
                    continue;
                if(this.mapData[nextX][nextY].canGo())
                {
                    tempList.add(new Point(nextX,nextY));
                    int tempType=this.mapData[nextX][nextY].type;
                    this.mapData[nextX][nextY].type=MapBlock.walked;
                    dfs(nextX,nextY);
                    ((LinkedList<Point>) (tempList)).removeLast();
                    this.mapData[nextX][nextY].type = tempType;
                }
            }
        }
        void printMap()
        {
            for(int i=0;i<mapHeight;i++)
            {
                for(int j=0;j<mapWidth;j++)
                    System.out.print(mapData[i][j].type);
                System.out.println();
            }
        }
        void printRoutedMap()
        {
            for(Point i : routeList)
            {
                mapData[i.x][i.y].type=MapBlock.walked;
            }
            //mapData[endX][endY].type=MapBlock.end;
            printMap();
        }

    }
    public static void main(String[] args) {
        int[][] mapData= {
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 8, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1},
                {1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1},
                {1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1},
                {1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1},
                {1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
        };
        Map myMap=new Map(mapData,9,13);
        myMap.printMap();
        myMap.dfs(myMap.startX,myMap.startY);
        System.out.println(myMap.routeList);
        //myMap.printMap();
        myMap.printRoutedMap();
    }
}
