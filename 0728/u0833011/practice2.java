public class practice2
{
    static class BigNumber
    {
        private String data;
        public BigNumber(String data)
        {
            this.data=data;
        }
        public void setData(String data)
        {
            this.data=data;
        }
        public BigNumber add(BigNumber another)
        {
            StringBuilder answer=new StringBuilder();
            int overflow=0;
            int notation=10;
            if(this.data.matches("[01]+")&&another.data.matches("[01]+"))
            {
                notation=2;
            }
            StringBuilder x=new StringBuilder(this.data);
            StringBuilder y=new StringBuilder(another.data);
            while(x.length()!=y.length())
            {
                if(x.length()>y.length())
                    y.insert(0,0);
                else
                    x.insert(0,0);
            }
            this.data=x.toString();
            another.data=y.toString();
            int max=x.length();
            for(int i=max-1;i>=0;i--)
            {
                int a=Character.getNumericValue(this.data.charAt(i));
                //int a=this.data.charAt(i)-48;
                int b=Character.getNumericValue(another.data.charAt(i));
                answer.append((a+b+overflow)%notation);
                overflow=(a+b+overflow>=notation)?1:0;
                /*
                    a<notation
                    b<notation
                    a+b<2*notation
                    although a+1=notation
                    because b<notation
                    vice versa
                 */
            }
            if(overflow!=0)
                answer.append(overflow);
            return new BigNumber(answer.reverse().toString());
        }
        public String toString()
        {
            return this.data;
        }
    }
    public static void main(String[] args)
    {
        BigNumber a=new BigNumber("12345678910");
        BigNumber b=new BigNumber("21474836499");
        System.out.println(a.add(b));
        a.setData("01010011111");
        b.setData("11111111111");
        System.out.println(a.add(b));
        a.setData("1000");
        b.setData("1024");
        System.out.println(a.add(b));
    }
}
