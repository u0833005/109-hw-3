import java.util.*;

public class Node {
    public int data;
    public Node left;
    public Node right;

    public Node(int data){  //constructor,初始
        this.data = data;
        this.left = null;
        this.right = null;
    }

    public Node(int data, Node left, Node right){
        this.data = data;
        this.left = left;
        this.right = right;
    }

    public Node copy() {    //複製節點
        return new Node(this.data);
    }

    public void setLeftRight(Node left ,Node right){    //設定左、右節點
        this.left = left;
        this.right = right;
    }

    public void preOrder(){     //前序(中-左-右)
        System.out.print(this.data + " ");  //中間,印出值
        if(this.left != null) this.left.preOrder(); //判斷左有無值,有則印出值
        if(this.right != null) this.right.preOrder();   //判斷右有無值,有則印出值
    }

    public void inOrder(){      //中序(左-中-右)
        if(this.left != null) this.left.inOrder();
        System.out.print(this.data + " ");
        if(this.right != null) this.right.inOrder();
    }

    public void postOrder(){    //後序(左-右-中)
        if(this.left != null) this.left.postOrder();
        if(this.right != null) this.right.postOrder();
        System.out.print(this.data + " ");
    }

    public static boolean Dfs(Node root, int data){     //深度優先搜尋
        Stack<Node> stacknode = new Stack<>();
        Node current = root;    //當前
        stacknode.add(root);    //放到stack
        while(current != null || !stacknode.isEmpty()){ //判斷是stack否為空&當前是否為null
            current = stacknode.pop();  //當前為stack pop出的值
            System.out.print(current.data+ " ");    //印出當前值
            if(current.data == data){   //判斷當前值是否等於搜尋值
                return true;
            }
            if(current.right != null){  //判斷左節點是否為null
                stacknode.push(current.right);  //將左節點值放入stack
            }
            if(current.left != null){   //判斷右節點是否為null
                stacknode.push(current.left);   //將右節點值放入stack
            }
        }
        return false;
    }

    public static boolean Bfs(Node root, int data){     //深度優先搜尋
        Queue<Node> queuenode = new LinkedList<>();
        //Queue<Node> queuenode = new Queue<>();
        Node curr = root;   //當前
        queuenode.add(root);  //放到queue
        while(curr != null || !queuenode.isEmpty()){    //判斷是queue否為空&當前是否為null
            curr = queuenode.remove();    //當前為queue poll出的值
            System.out.print(curr.data+ " ");   //印出當前值
            if(curr.data == data){  //判斷當前值是否等於搜尋值
                return true;
            }
            if(curr.left != null){  //判斷左節點是否為null
                queuenode.add(curr.left); //將左節點值放入queue
            }
            if(curr.right != null){ //判斷右節點是否為null
                queuenode.add(curr.right);    //將右節點值放入queue
            }
        }
        return false;
    }

    @Override
    public String toString(){
        return "[data:" + data + ",left:" + (left == null ? "null" : left.data ) + ",right:" + (right == null ? "null" : right.data );
    }


}
