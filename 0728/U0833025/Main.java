//import java.util.Queue;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {

        Node root = new Node(1);    //root節點

        Node a = new Node(2);
        Node b = new Node(3);
        root.setLeftRight(a,b);     //設定root節點的左(a)、右(b)節點

        Node c = new Node(4);
        Node d = new Node(5);
        a.setLeftRight(c,d);        //設定a節點的左(c)、右(d)節點

        Node e = new Node(6);
        Node f = new Node(7);
        b.setLeftRight(e,f);        //設定b節點的左(e)、右(f)節點

        System.out.println("前序preorder:");
        root.preOrder();

        System.out.println("\n"+"中序inorder:");
        root.inOrder();

        System.out.println("\n"+"後序postorder:" );
        root.postOrder();

        System.out.println("\n"+"深度優先搜尋DFS:" );
        root.Dfs(root,7);

        System.out.println("\n"+"廣度優先搜尋BFS:" );
        root.Bfs(root,7);
    }
}
