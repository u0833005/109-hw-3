import java.util.HashMap;
import java.util.Map;

public class HashMapHw {
    public static void main(String[] args) {
        int[] nums = {2, 7, 11, 15};
        int target = 9;

        Map<Integer, Integer> map = new HashMap<>();

        for(int i=0 ; i < nums.length; i++){
            int c = target - nums[i];
            if(map.containsKey(c)){
                System.out.println(map.get(c)+","+i);
            }
            map.put(nums[i],i);
        }
    }
}
