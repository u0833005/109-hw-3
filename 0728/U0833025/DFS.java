import java.util.Stack;

public class DFS {
    static void print(int[][] map){
        for(int[] a : map){
            for(int b : a){
                System.out.print(b + " ");
            }
            System.out.println();
        }
    }
    static class Point {
        public int x;
        public int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Point copy() {
            return new Point(this.x, this.y);
        }

        @Override
        public String toString() {
            return "[x" + x + ",y:" + y + "]";
        }
    }
    public static void dfs(int[][] map){
        boolean[][] pass = new boolean[map.length][map[0].length];
        Stack<Point> stack = new Stack<>();
        Point current = new Point(1,1);

        while (map[current.x][current.y] != 9){
            if((map[current.x+1][current.y]==0||map[current.x+1][current.y]==9)&&!pass[current.x+1][current.y]){
                //向右
                pass[current.x+1][current.y]=true;
                current.x +=1;
                stack.push(current.copy());
            }else if((map[current.x-1][current.y]==0||map[current.x-1][current.y]==9)&&!pass[current.x-1][current.y]){
                //向左
                pass[current.x-1][current.y]=true;
                current.x -=1;
                stack.push(current.copy());
            }else if((map[current.x][current.y+1]==0||map[current.x][current.y+1]==9)&&!pass[current.x][current.y+1]){
                //向下
                pass[current.x][current.y+1]=true;
                current.y +=1;
                stack.push(current.copy());
            }else if((map[current.x][current.y-1]==0||map[current.x][current.y-1]==9)&&!pass[current.x][current.y-1]){
                //向上
                pass[current.x][current.y-1]=true;
                current.y -=1;
                stack.push(current.copy());
            }else{
                //無路
                stack.pop();    //無法繼續行走的座標從stack中取出
                current = stack.peek().copy();  //將當前座標移動至取出的上一個座標
            }
        }
        for(Point p:stack){
            if(stack.lastElement() != p){
                map[p.x][p.y] = 2;
            }
        }
    }


    public static void main(String[] args) {
        int[][] map = {
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 8, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1},
                {1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1},
                {1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1},
                {1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1},
                {1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
        };

        print(map);
        dfs(map);
        System.out.println();
        print(map);
    }
}
