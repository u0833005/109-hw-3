import java.util.Stack;

public class BinarySearchTree {

    public static class BST{
        Node root;

        public BST() {      //constructor
            this.root = null;
        }

        void insert(int data){      //插入值
            this.root = insertRec(root, data);
        }

        Node insertRec(Node root, int data){    //遞迴
            if(root == null){   //第一個節點
                root = new Node(data);
                return root;
            }

            if(data < root.data){   //判斷大小
                root.left = insertRec(root.left, data);
            }else if(data > root.data){
                root.right = insertRec(root.right, data);
            }
            return root;
        }

        void inOrder(){     //中序(左-中-右)
            Node tmp = root;
            Stack<Node> stack = new Stack<>();

            while(tmp != null || !stack.isEmpty()){     //判斷是stack否為空&當前是否為null
                if(tmp != null){    //判斷當前是否為nll
                    stack.add(tmp); //放到stack
                    tmp = tmp.left;
                }else{
                    tmp = stack.pop();  //從stack取出
                    System.out.print(tmp.data+" "); //印出當前
                    tmp = tmp.right;
                }
            }
        }
    }
    public static void main(String[] args) {
        BST tree = new BST();

        tree.insert(50);
        tree.insert(30);
        tree.insert(20);
        tree.insert(40);
        tree.insert(70);
        tree.insert(60);
        tree.insert(80);

        tree.inOrder();
    }
}
