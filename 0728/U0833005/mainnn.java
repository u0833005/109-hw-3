public class mainnn {
    static class Student{
        private String name;
        private int age;
        public Student(String name,int age){
            this.age = age;
            this.name = name;
        }
        public String getName(){
            return this.name;
        }
        public int getAge(){
            return this.age;
        }
    }
    public static void main(String[] args) {
        Student a = new Student("Tom",15);
        Student b = new Student("Leo",18);

        Thread x = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (a){
                    System.out.println(a.getName()+" is "+a.getAge()+"year old");
                    synchronized (b){
                        System.out.println(b.getName()+" is "+b.getAge()+"year old");
                    }
                }
            }
        });
        Thread y = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (b){
                    System.out.println(b.getName()+" is "+b.getAge()+"year old");
                    synchronized (a){
                        System.out.println(a.getName()+" is "+a.getAge()+"year old");
                    }
                }
            }
        });

        x.start();
        y.start();
    }
}
