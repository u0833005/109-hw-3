public class Node {
    public int data;
    public Node left;
    public Node right;

    public Node(int data){
        this.data=data;
        this.left=null;
        this.right=null;
    }
    public Node(int data,Node left,Node right){
        this.data=data;
        this.left=left;
        this.right=right;
    }
    public void setLeftRight(Node left,Node right){
        this.left=left;
        this.right=right;
    }

//    public  void preOrder(){
//        System.out.println(this.data);
//        if(this.left != null) this.left.preOrder();
//        if(this.right != null) this.right.preOrder();
//    }
//    public void inOrder(){
//        if(this.left != null) this.left.preOrder();
//        System.out.println(this.data);
//        if(this.right != null) this.right.preOrder();
//    }
//    public void PostOrder(){
//        if(this.left != null) this.left.preOrder();
//        if(this.right != null) this.right.preOrder();
//        System.out.println(this.data);
//    }
}
