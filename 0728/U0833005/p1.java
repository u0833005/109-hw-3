public class p1 {
    public int rr;
    static class T1 extends Thread{
        public int r;
        public T1(int r){
            this.r = r;
        }
        @Override
        public void run(){
            for(int i = 1;i<=this.r;i+=2){
                System.out.println(getName() + ":" + i);
                    try {
                        Thread.sleep(200);
                        //t2.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
            }
        }
        public int getResult(){
            return this.r;
        }
    }
    static class T2 extends Thread{
        public int r;
        public T2(int r){
            this.r = r;
        }
        @Override
        public void run(){
            for(int i = 2;i<=this.r;i+=2){
                System.out.println(getName()+":"+i);
                try {
                    Thread.sleep(200);
                    //t1.join();
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
        public int getResult(){
            return this.r;
        }
    }


    static T1 t1 = new T1(100);
    static T2 t2 = new T2(100);
    //static Main.T2 t2 = new Main.T2();
    public static void main(String[] args) throws InterruptedException {
        t1.start();
        t2.start();
    }
}
