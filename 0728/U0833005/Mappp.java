import java.util.HashMap;
import java.util.Map;

public class Mappp {
    public static void main(String[] args) {
        int[] nums = {2,7,11,15};
        int target = 9;

        Map<Integer, Integer> numMap = new HashMap<>();
        for(int i = 0;i<nums.length;i++){
            int c = target - nums[i];
            if(numMap.containsKey(c)){
                System.out.println(numMap.get(c)+","+i);
                break;
            }
            numMap.put(nums[i],i);
        }
    }
}
