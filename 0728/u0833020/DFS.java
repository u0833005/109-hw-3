import java.util.Stack;

public class DFS {

    static void print(int[][] map) {
        for (int[] a : map) {
            for (int b : a) {
                System.out.print(b + " ");
            }
            System.out.println();
        }
    }

    static class Point {
        public int x;
        public int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Point copy() {
            return new Point(this.x, this.y);
        }

        @Override
        public String toString() {
            return "[x:" + x + ",y:" + y + "]";
        }
    }

    public static void dfs(int[][] map) {
        boolean[][] pass = new boolean[map.length][map[0].length]; //避免往後走
        Stack<Point> stack = new Stack<>(); //走過路徑，有走過的數字
        Point current = new Point(1, 1); //記當前當下的點

        while (map[current.x][current.y] != 9) { //直到找到9終點
            if ((map[current.x + 1][current.y] == 0 || map[current.x + 1][current.y] == 9) && !pass[current.x + 1][current.y]) {
                pass[current.x + 1][current.y] = true;
                current.x += 1;
                stack.push(current.copy());
            } else if ((map[current.x][current.y + 1] == 0 || map[current.x][current.y + 1] == 9) && !pass[current.x][current.y + 1]) {
                pass[current.x][current.y + 1] = true;
                current.y += 1;
                stack.push(current.copy());
            } else if ((map[current.x - 1][current.y] == 0 || map[current.x - 1][current.y] == 9) && !pass[current.x - 1][current.y]) {
                pass[current.x - 1][current.y] = true;
                current.x -= 1;
                stack.push(current.copy());
            } else if ((map[current.x][current.y - 1] == 0 || map[current.x][current.y - 1] == 9) && !pass[current.x][current.y - 1]) {
                pass[current.x][current.y - 1] = true;
                current.y -= 1;
                stack.push(current.copy());
            } else {
                stack.pop(); //周圍都是死路，後頭改成true了
                current = stack.peek().copy(); //回上一步
            }
        }
        for(Point p: stack){
            if(stack.lastElement()!=p){
                map[p.x][p.y]=2;

            }
        }
    }

    public static void main (String[]args) {

        int[][] map = {
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                {1, 8, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1},
                {1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1},
                {1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 1},
                {1, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 1},
                {1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 9, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
                print(map);
                dfs(map);
                System.out.println();
                print(map);
    }
}