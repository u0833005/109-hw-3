import java.util.HashMap;
import java.util.Map;

public class HashMappractice {

    public static void main(String[] args) {
        int[] nums = {2,7,11,15};
        int target = 9;

        Map<Integer,Integer> numMap = new HashMap<>(); //泛型 宣告Map

        for(int i=0;i<nums.length;i++){
            int c= target-nums[i]; //key
            if(numMap.containsKey(c)){ //map裡有沒有這個key
                System.out.println(numMap.get(c)+","+i);
                break;
            }
            numMap.put(nums[i],i);
        }
    }
}
