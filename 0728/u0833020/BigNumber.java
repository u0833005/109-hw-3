package pop;

import java.util.Scanner;

public class BigNumber {

    public static String bigNumber(String a, String b) {
        StringBuilder answer = new StringBuilder();
        int max = Math.max(a.length(), b.length());
        int overflow = 0;
        int notation = 10;

        if (!a.matches("[2-9]+") && a.matches("[0-1]+") && !b.matches("[2-9]+") && a.matches("[0-1]+")) {
            notation = 2;
        }

        if (a.length() > b.length()) {
            StringBuilder x = new StringBuilder(a);
            StringBuilder y = new StringBuilder(b);
            while (x.length() > y.length()) {
                y.insert(0, 0);
            }
            a = x.toString();
            b = y.toString();
        } else if (a.length() < b.length()) {
            StringBuilder x = new StringBuilder(a);
            StringBuilder y = new StringBuilder(b);
            while (x.length() > y.length()) {
                x.insert(0, 0);
            }
            a = x.toString();
            b = y.toString();
        }
        for (int i = max - 1; i >= 0; i--) {
            int x = Character.getNumericValue(a.charAt(i));
            int y = Character.getNumericValue(b.charAt(i));

            if (x + y + overflow >= notation) {
                answer.append((x + y + overflow) % notation);
                overflow = (x + y + overflow) / notation;
            } else {
                answer.append(x + y + overflow);
                overflow = 0;
            }
            if (i == 0 && overflow != 0) {
                answer.append(overflow);
            }
        }
        return answer.reverse().toString();
    }
        public static void main (String[]args)
        {
        String x, y;
        String answer;
        Scanner sc = new Scanner(System.in);

        x = sc.next();
        y = sc.next();

        answer = bigNumber(x, y);
        System.out.println(answer);
        }
}
