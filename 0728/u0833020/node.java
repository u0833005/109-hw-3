import org.w3c.dom.Node;

public class node {

    public int data;
    public node left;
    public node right;

    public node(int data){
        this.data = data;
        this.left = null;
        this.right = null;
    }

    public  node (int data,node left,node right){
        this.data = data;
        this.left = left;
        this.right = right;
    }

    public  void setLeftRight(node left , node right){
        this.left=left;
        this.right=right;
    }


    public  void preOrder(){
        System.out.println(this.data);
        if(this.left != null) this.left.preOrder();
        if(this.right != null) this.right.preOrder();
    }
    public  void  inorder(){
        if(this.left != null) this.left.preOrder();
        System.out.println(this.data);
        if(this.right != null) this.right.preOrder();
    }

    public void postOrder(){
        if(this.left != null) this.left.preOrder();
        if(this.right != null) this.right.preOrder();
        System.out.println(this.data);
    }

    @Override
    public  String toString(){
        return "[data:"+data+",left:" +(left == null ? "null" : left.data)+
                ",right: " +(right == null ? "null" : right.data)+"]";
    }



}
