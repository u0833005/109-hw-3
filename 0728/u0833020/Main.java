import java.util.LinkedList;
import java.util.List;

public class Main {

    //List<node> list = new LinkedList<>();

    public static void main(String[] args) {

        node root = new node (1);
        node a = new node(2);
        node b = new node  (3);
        root.setLeftRight(a,b);

        node c = new node(4);
        node d = new node  (5);
        a.setLeftRight(c,d);

        node e = new node(6);
        node f = new node  (7);
        b.setLeftRight(e,f);

        root.preOrder();
    }
}
