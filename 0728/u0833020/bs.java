public class bs {

    public  static  int binarySearch(int[]arr,int k){
        int low = 0,high = arr.length - 1;

        while (high > low){
            int mid = low + (high - low) /2;
            if(arr[mid]>k){
                high = mid;
            }else  {
                low = mid +1;
            }

        }
        if (arr.length == 0){
            return  -1;
        }else{
            return arr [low] == k ? low :-1;
        }


    }


    public static void main(String[] args) {
        int[] arr = {5, 7, 8,10,15,20,30,41,55};
        System.out.println(binarySearch(arr,20));

    }

}
