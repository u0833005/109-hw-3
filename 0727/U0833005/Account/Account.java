package Account;

public class Account {
        public String accountUser;
        public double balance;

        public Account() {
            this("empty", 0.0);
        }

        public Account(String accountNumber, double balance) {
            this.accountUser = accountNumber;
            this.balance = balance;
        }

        public String getAccountNumber() {
            return accountUser;
        }

        public double getBalance() {
            return balance;
        }

        public void credit(int money) {
            balance += money;
        }

        public void debit(int money) {
            balance -= money;
        }
}

