package Account;

public class SavingAccount extends Account {
    public double interestRate;

    public SavingAccount(String accountUser, double balance) {
        this.accountUser = accountUser;
        this.balance = balance;
    }

    public void setinterestRate() {
        this.interestRate = 0.05;
    }

    public void setinterestRate(double itr) {
        this.interestRate = itr;
    }

    public double getInterestRate() {
        return this.interestRate;
    }

    public double calculateinterest() {
        return this.interestRate * this.balance;
    }
    public void getcalculateinterest() {
        this.balance = this.balance+this.interestRate * this.balance;
    }
}
