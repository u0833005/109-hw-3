package Account;

public class CheckAccount extends Account {
    public double transactionFee;

    public CheckAccount(String accountNumber, double balance) {
        this.accountUser = accountNumber;
        this.balance = balance;
    }

    public void countFee() {
        this.transactionFee = 0.01;
    }

    public void countFee(double transactionFee) {
        this.transactionFee = transactionFee;
    }

    public double getTransactionFee() {
        return this.transactionFee;
    }

    public void credit(int money) {
        balance += money - money * this.transactionFee;
    }

    public void debit(int money) {
        balance -= money + money * this.transactionFee;
    }
}
