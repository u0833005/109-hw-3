import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;

class AccountComparator implements Comparator<Account>
{
    public int compare(Account a,Account b)
    {
        return a.getName().compareTo(b.getName());
    }
}

class Account implements Comparable<Account>
{
    private String name;
    private int balance;
    public Account(String name,int balance)
    {
        this.name=name;
        this.balance=balance;
    }
    public String toString()
    {
        return "Account("+this.name+","+this.balance+")";
    }
    public int compareTo(Account another)
    {
        return this.balance-another.balance;
    }
    public String getName()
    {
        return name;
    }
}

public class practice2
{
    public static void main(String[] args)
    {
        List<Account> accountList=new ArrayList<Account>();
        accountList.add(new Account("Mary",30));
        accountList.add(new Account("Gary",78));
        accountList.add(new Account("Terry",66));
        accountList.add(new Account("Ben",1200));
        accountList.add(new Account("Alice",18));
        Collections.sort(accountList);
        System.out.println(accountList);
        Collections.sort(accountList,new AccountComparator());
        System.out.println(accountList);
    }
}
