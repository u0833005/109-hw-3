class Vector
{
    private float x;
    private float y;
    public Vector(float x,float y)
    {
        this.x=x;
        this.y=y;
    }
    public void add(Vector another)
    {
        this.x+=another.x;
        this.y+= another.y;
    }
    public void add(float anotherX,float anotherY)
    {
        Vector another=new Vector(anotherX,anotherY);
        this.add(another);
    }
    public void subtract(Vector another)
    {
        this.x-=another.x;
        this.y-=another.y;
    }
    public void subtract(float anotherX,float anotherY)
    {
        Vector another=new Vector(anotherX,anotherY);
        this.subtract(another);
    }
    public void multiply(int scale)
    {
        this.x*=scale;
        this.y*=scale;
    }
    public float dot(Vector another)
    {
        return this.x*another.x+this.y*another.y;
    }
    public void divide(int scale)
    {
        this.x/=scale;
        this.y/=scale;
    }
    public boolean equal(Vector another)
    {
        if(another.x==this.x&&another.y==this.y)
            return true;
        return false;
    }
    public String toString()
    {
        return "Vector("+this.x+","+this.y+")";
    }
    //static
    public static Vector add(Vector a,Vector b)
    {
        return new Vector(a.x+b.x,a.y+b.y);
    }
    public static Vector subtract(Vector a,Vector b)
    {
        return new Vector(a.x-b.x,a.y-b.y);
    }
}
class Todo
{
    private int day;
    private int month;
    private String content;
    private boolean done;
    public Todo(int d,int m,String content,boolean isDone)
    {
        this.day=d;
        this.month=m;
        this.content=content;
        this.done=isDone;
    }
    public String toString()
    {
        return this.month+"/"+this.day+" "+this.content+((this.done)?"完成":"還沒完成");
    }
}
public class practice1
{
    public static void main(String[] args)
    {
        Vector a=new Vector(3,4);
        Vector b=new Vector(5,6);
        System.out.println(a);
        a.add(b);
        System.out.println(a);
        a.subtract(b);
        System.out.println(a);
        a.multiply(2);
        System.out.println(a);
        a.divide(2);
        System.out.println(a);
        Vector c=new Vector(3,4);
        System.out.print("a與c");
        System.out.println((a.equal(c))?"相同":"不同");
        System.out.print("a與b");
        System.out.println((a.equal(b))?"相同":"不同");
        System.out.println(a.dot(b)+"\n");
        //static
        System.out.println(Vector.add(a,b)+"\n");

        Todo myTodo=new Todo(27,7,"學習Java",false);
        System.out.println(myTodo);
    }
}