public class homework
{
    static class Account
    {
        private int balance;
        public Account()
        {
            this.balance=0;
        }
        public Account(int balance)
        {
            this.balance=balance;
        }
        public void credit(int money)
        {
            this.balance+=money;
        }
        public boolean debit(int money)
        {
            if(money>this.balance)
                return false;
            this.balance-=money;
            return true;
        }
        public int getBalance()
        {
            return balance;
        }
    }
    static class SavingAccount extends Account
    {
        private double interestRate;
        public static double defaultRate=0.05;
        public SavingAccount()
        {
            this.interestRate=defaultRate;
        }
        public SavingAccount(int balance,double interestRate)
        {
            super(balance);
            this.interestRate=interestRate;
        }
        public void setInterestRate()
        {
            this.interestRate=defaultRate;
        }
        public void setInterestRate(double interestRate)
        {
            this.interestRate=interestRate;
        }
        public int calculateInterest()
        {
            return (int)(this.getBalance()*this.interestRate);
        }
    }
    static class CheckAccount extends Account
    {
        private double transactionFee;
        static int chargeFee(int money)
        {
            if(money>=10000)
                return (int)(money*0.05);
            return (int)(money*0.02); //money<10000
        }
        public void credit(int money)
        {
            super.credit(money-chargeFee(money));
        }
        public boolean debit(int money)
        {
            if(super.debit(money+chargeFee(money)))
                return true;
            return false;
        }
    }
    public static void main(String[] args)
    {
        SavingAccount a=new SavingAccount();
        a.credit(600000);
        System.out.println(a.getBalance());
        a.debit(50000);
        System.out.println(a.getBalance());
        System.out.println(a.calculateInterest()+"\n");
        CheckAccount b=new CheckAccount();
        b.credit(80000);
        System.out.println(b.getBalance());
        b.debit(5000);
        System.out.println(b.getBalance());
    }
}
