import java.util.Scanner;

public  class hw0727 {
    public static class Account{
        float balance=1000;
        public void credit(float i){
            this.balance+=i;
            System.out.println("存額:"+balance);
        }
        public void debit(float j){
            this.balance-=j;
            System.out.println("存額:"+balance);
        }
    }
    public static class SavingAccount extends Account{
        double interestRate;

        public void setInterestRate(double k){
            this.interestRate=k;
        }
        public void getInterestRate(){
            System.out.println("利率為:"+this.interestRate);
        }
        public void calculateInterest(){
            System.out.println("一年利息為:"+this.balance*this.interestRate);
        }
    }
    public static class CheckAccount extends Account{
        double transactionFee;
        public void chargeFee(){

        }
        public void credit(float i){
            this.balance=i;
            if(i<5000){
                transactionFee=0.1;
            }else{transactionFee=0.2;}
            System.out.println("手續費:"+(this.balance*transactionFee));
            System.out.println("存額:"+(this.balance-this.balance*transactionFee));
        }
        public void debit(float j){
            this.balance-=j;
            if(j<5000){
                transactionFee=0.1;
            }else{transactionFee=0.2;}
            System.out.println("手續費:"+(j*transactionFee));
            System.out.println("餘額:"+(this.balance-j*transactionFee));
        }
    }


    public static void main(String[] args) {
        System.out.println("您現在有1000元");
        System.out.println("請輸入存款金額");
        Scanner client = new Scanner(System.in);
        float add=client.nextFloat();
        SavingAccount x=new SavingAccount();
        x.credit(add);
        System.out.println("請輸入提款金額");
        float min=client.nextFloat();
        x.debit(min);
        System.out.println("請輸入利率");
        double rate=client.nextDouble();
        x.setInterestRate(rate);
        x.getInterestRate();
        x.calculateInterest();
        System.out.println("請輸入支票存款金額");
        CheckAccount a1=new CheckAccount();
        float yy=client.nextFloat();
        a1.credit(yy);
        float zz=client.nextFloat();
        a1.debit(zz);
    }
}
