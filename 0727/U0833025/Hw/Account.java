package Hw;

import java.util.Scanner;
abstract class Account {

    public double balance = 0; //餘額
    abstract void credit(double C);   //存款
    abstract void debit(double D);    //提款

    public static class SavingAccount extends Account{ //儲蓄帳戶
        public double cashC ;
        public double cashD ;
        public  double defaultRate = 0.05;

        void credit(double cashC){
            this.cashC=cashC;
            System.out.println("存款"+cashC);
        }
        void debit(double cashD){
            this.balance=this.cashC-cashD;
            System.out.println("餘額"+this.balance);
        }


        void setInterestRate(){  //利率
            System.out.println("利率"+this.defaultRate);
        }
        void setInterestRate(double Rate){  //利率
            this.defaultRate=Rate;
            System.out.println("利率"+this.defaultRate);
        }

        void calculateInterest(){    //計算利息
            System.out.println("利息"+this.balance*this.defaultRate);
        }
    }

    public static class CheckAccount extends Account{  //支票帳戶
        public double checkC ;
        public double checkD ;
        public double transactionFee;

        public void credit(double checkC){
            this.checkC=checkC-this.transactionFee;
            System.out.println("存款"+this.checkC);
        }

        public void debit(double checkD){
            this.balance=this.checkC-checkD;
            System.out.println("餘額"+(this.balance-this.transactionFee));
        }

        public void chargeFee(double money){  //計算費率
            if(money>5000){
                this.transactionFee=money*0.5;
                System.out.println("手續費"+this.transactionFee);
            }else{
                this.transactionFee=money*0.2;
                System.out.println("手續費"+this.transactionFee);
            }
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("請輸入存款金額:");
        SavingAccount A=new SavingAccount();
        double cashC = sc.nextDouble();
        A.credit(cashC);
        System.out.println("請輸入提款金額:");
        double cashD = sc.nextDouble();
        A.debit(cashD);
        A.setInterestRate();
        A.calculateInterest();

        System.out.println("請輸入支票存款金額:");
        CheckAccount B=new CheckAccount();
        double checkC = sc.nextDouble();
        B.chargeFee(checkC);
        B.credit(checkC);
        System.out.println("請輸入支票提款金額:");
        double checkD = sc.nextDouble();
        B.chargeFee(checkD);
        B.debit(checkD);

    }

}
