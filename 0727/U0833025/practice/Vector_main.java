package test;

public class Vector_main {
    public static void main(String[] args) {
        Vector x = new Vector(2,3);
        Vector y = new Vector(2,3);

        x.add(y);
        System.out.println(x.toString());
        System.out.println(Vector.add(x,y).toString());

        x.sub(y);
        System.out.println(x.toString());
        System.out.println(Vector.sub(x,y).toString());

        x.mul(y);
        System.out.println(x.toString());
        System.out.println(Vector.mul(x,y).toString());

        x.div(y);
        System.out.println(x.toString());
        System.out.println(Vector.div(x,y).toString());

    }
}
