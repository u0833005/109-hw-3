package extend;

public class Animal {
    public void move(){
        System.out.println("移動...");
    }
//}
    public static class Dog extends Animal{
        public void move(){
            System.out.println("跑...");
        }
    }
    public static class Bird extends Animal{
        public void move(){
            System.out.println("飛...");
        }
    }

    public static class Man extends Animal{
        public void move(){
            System.out.println("走...");
        }
    }

    public static void main(String[] args) {
        Animal a = new Animal();
        a.move();
        Animal b = new Dog();
        b.move();
        Animal c = new Bird();
        c.move();
        Animal d = new Man();
        d.move();
    }
}