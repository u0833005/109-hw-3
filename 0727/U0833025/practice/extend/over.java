package extend;

public class over {
    public static class A{
        void printInfor(){
            System.out.println("chia");
        }
    }
    public static class B extends A{
        void printInfor(){
            System.out.println("yuan");
        }
    }
    public static class C extends A{

    }

    public static void main(String[] args) {
        C c = new C();
        c.printInfor();
        B b = new B();
        b.printInfor();
    }
}
