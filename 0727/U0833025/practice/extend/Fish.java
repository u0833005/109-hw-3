package extend;

abstract class Fish {
    abstract void food();

    public static class Clownfish extends Fish {
        void food() {
            System.out.println("藻類");
        }
    }

    public static class Goldfish extends Fish {
        void  food() {
            System.out.println("小蝦");
        }
    }

    public static class Blowfish extends Fish {
        void  food() {
            System.out.println("貝類");
        }
    }

    public static void main(String[] args) {
        Fish a = new Clownfish();
        a.food();
        Fish b = new Goldfish();
        b.food();
        Fish c = new Blowfish();
        c.food();
    }
}