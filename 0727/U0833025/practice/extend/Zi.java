package extend;

public class Zi extends Fu {
    int num = 15 ;
    String name;
    public  Zi(){
        this.num=20;
        this.name="chiayuan";
    }
    public  Zi(int i){
        this();
        this.num=i;
    }
    public void showNum (){
        int num = 5;
        System.out.println(num);
        System.out.println(this.num);
        System.out.println(super.num);
    }
    public void printInfo(){
        System.out.println("姓名:"+name+"\n"+"年齡:"+num);
    }
    public static void main(String[] args) {
        Zi a=new Zi(10);
        a.showNum();
        a.printInfo();
    }
}
