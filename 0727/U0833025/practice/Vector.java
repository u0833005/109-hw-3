package test;

public class Vector {
    public float a;
    public float b;

    public Vector(float a,float b){
        this.a = a;
        this.b = b;
    }
    public String toString(){
        return "("+this.a+","+this.b+")";
    }

    public void add(Vector x){
        this.a += x.a;
        this.b += x.b;
    }
    public static Vector add(Vector x,Vector y){
        return new Vector(x.a+y.a,x.b+ y.b);
    }

    public void sub(Vector x){
        this.a -= x.a;
        this.b -= x.b;
    }
    public static Vector sub(Vector x,Vector y){
        return new Vector(x.a-y.a,x.b- y.b);
    }

    public void mul(Vector x){
        this.a *= x.a;
        this.b *= x.b;
    }
    public static Vector mul(Vector x,Vector y){
        return new Vector(x.a*y.a,x.b*y.b);
    }

    public void div(Vector x){
        this.a /= x.a;
        this.b /= x.b;
    }
    public static Vector div(Vector x,Vector y){
        return new Vector(x.a/y.a,x.b/y.b);
    }

    public void dot(Vector x){
        this.a *= x.a;
        this.b *= x.b;
    }
    public static Vector dot(Vector x,Vector y){
        return new Vector(x.a*y.a,x.b*y.b);
    }


}
