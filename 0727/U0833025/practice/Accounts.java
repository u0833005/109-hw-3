import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Accounts implements Comparable<Accounts>{
    public String name;
    public int balance;

    public Accounts(String name, int balance){
        this.name = name;
        this.balance = balance;
    }

    @Override
    public int compareTo(Accounts other) {
        return this.balance - other.balance;
    }

    public String toString(){
        return "/**/姓名:"+this.name+"=>帳戶餘額:"+this.balance;
    }

    public static void main(String[] args) {
        List accounts = Arrays.asList(
                new Accounts("Mary",30),
                new Accounts("Gary",78),
                new Accounts("Tery",66),
                new Accounts("Ben",1200),
                new Accounts("Amy",18)
        );
        Collections.sort(accounts);
        System.out.println(accounts);
        Collections.sort(accounts, new AccountComparator());
        System.out.println(accounts);
    }
}
