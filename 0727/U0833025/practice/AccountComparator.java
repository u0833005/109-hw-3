import java.util.Comparator;

public class AccountComparator implements Comparator<Accounts> {
    @Override
    public int compare(Accounts pre, Accounts temp){
        return - pre.compareTo(temp);
    }
}
