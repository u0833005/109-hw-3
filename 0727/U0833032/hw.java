import hwnewpackage.newpkg.Account;
import hwnewpackage.newpkg.SavingAccount;
import hwnewpackage.newpkg.CheckAccount;
import java.util.Scanner;
public class hw {
    public static void main(String[] args) {
        Scanner user = new Scanner(System.in);
        System.out.println("請輸入帳戶金額");
        int balance = user.nextInt();
        Account a = new Account(balance);
        System.out.println("請輸入存款金額");
        int credit = user.nextInt();
        a.credit(credit);
        System.out.println("帳戶金額: " + a.getBalance());
        System.out.println("請輸入提款金額");
        int debit = user.nextInt();
        a.debit(debit);
        System.out.println("帳戶金額: " + a.getBalance());
        System.out.println("請輸入銀行利率");
        double rate = user.nextDouble();

        if (rate==0) {
            SavingAccount c = new SavingAccount(balance);
            System.out.println("帳戶金額: " + c.getBalance());
            System.out.println("預期利息: " + c.calculateInterest());
        }else {
            SavingAccount b =new SavingAccount(balance,rate);
            System.out.println("帳戶金額: " + b.getBalance());
            System.out.println("預期利息: " + b.calculateInterest());

        }

        CheckAccount d = new CheckAccount(balance);
        System.out.println("請輸入支票存款金額");
        int checredit = user.nextInt();
        d.chcredit(checredit);
        System.out.println("扣除完手續費: " + d.getTransactionFee());
        System.out.println("請輸入支票提款金額");
        int chedebit = user.nextInt();
        d.chdebit(chedebit);
        System.out.println("扣除完手續費: " + d.getTransactionFee());






    }
}
