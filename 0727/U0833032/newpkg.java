package hwnewpackage;
public class newpkg{
       public static class Account {
           public int balance;
           public Account(){
               this.balance = 0;
           }
           public Account(int balance){
               this.balance = balance;
           }
           public double getBalance(){
               return balance;
           }
           public void credit(int money) {
               balance += money;
           }
           public void debit(int money) {
               balance -= money;
           }
       }
           public static class SavingAccount extends Account {
               private double interestRate;
               public  SavingAccount(int balance) {
                   this.balance=balance;
                   this.interestRate = 0.05;
               }
               public SavingAccount(int balance, double interestRate) {
                   super(balance);
                   this.interestRate = interestRate;
               }
               public void setInterestRate() {
                   this.interestRate = 0.05;
               }
               public void setInterestRate(double interestRate) {
                   this.interestRate = interestRate;
               }
               public double calculateInterest() {
                   return (this.getBalance() * this.interestRate);
               }

           }

           public static class CheckAccount extends Account {
               private  int money;
               private double transactionFee;
               public CheckAccount(int balance) {
                   this.balance = balance;
                   this.transactionFee = 0.01;
               }

               public void chcredit(int money) {
                   balance += money-money*this.transactionFee;
               }
               public void chdebit(int money) {
                   balance -= money+money*this.transactionFee;
               }
               public double getTransactionFee(){
                   return balance;
               }

           }
}

