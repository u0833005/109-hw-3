package newpackage;

public class Vector {
    private float x;
    private float y;

    public Vector(float x,float y){
        this.x=x;
        this.y=y;
    }
    public void add(Vector b){
        this.x+=b.x;
        this.y+=b.y;
    }
    public static Vector add(Vector a, Vector b){
        return new Vector(a.x+b.x,a.y+b.y);
    }

    public String toString(){
        return "(" + this.x+","+this.y+")";
    }
 //減
    public void subtract(Vector b){
        this.x-=b.x;
        this.y-=b.y;
    }
    public static Vector substract(Vector a, Vector b){
        return  new Vector(a.x-b.x,a.y-b.y);
    }
//乘
    public void multiply(Vector b){
        this.x*=b.x;
        this.y*=b.y;
    }
    public static Vector multiply(Vector a, Vector b){
        return  new Vector(a.x*b.x,a.y*b.y);
    }
 //除
    public void divide(Vector b){
     this.x/=b.x;
     this.y/=b.y;
    }
    public static Vector divide(Vector a, Vector b){
        return  new Vector(a.x/b.x,a.y/b.y);
    }
}
