import java.util.Scanner;

public class hw0727 {

    public static class Account{ //父類別
        float balance; //存款餘額
        public void credit(float i){
            this.balance+=i;
            System.out.println("存款"+this.balance);
        }
        public void debit(float k){
            this.balance-=k;
            System.out.println("存款"+this.balance);
   }
}

    public static class SavingAccount extends Account  { //子類別
        double interestRate = 0.3;
        //public void setInterestRate(){};
        public void getInterestRate(){
            System.out.println("利率是"+interestRate);
        }
        public void calculateInterest(){

            System.out.println("年利率"+interestRate);
            System.out.println("一年利息"+this.balance*this.interestRate);

   }
 }

    public  static class CheckAccount extends  Account { //子類別
        Double transactionFee; //手續費
        public void page(float y){  //支票
            this.balance = y;
            System.out.println("支票金額"+balance);
        }
        public void credit(float w){    //支票存款
            transactionFee=0.001;
            System.out.println("手續費"+this.balance*transactionFee);
            System.out.println("存額"+(this.balance-this.balance*transactionFee));

        }
        public void debit(float x){     //支票提款
            transactionFee=0.001;
            System.out.println("手續費"+10);
            System.out.println("餘額"+(this.balance-this.balance*transactionFee-x-10));
        }
}


    public static void main(String[] args){
        double interestRate = 0.3;
        System.out.println("輸入存款金額");
        Scanner ap = new Scanner(System.in);
        float increase = ap.nextFloat();

        SavingAccount earn = new SavingAccount();
        earn.credit(increase);
        System.out.println("輸入提款金額");
        float out = ap.nextFloat();
        earn.debit(out);
        earn.calculateInterest(); //顯示函式內容 年利率 利息

        System.out.println("請輸入支票存款金額");
        CheckAccount pp = new CheckAccount();
        float paper = ap.nextFloat();
        pp.page(paper);
        pp.credit(paper);

        System.out.println("請輸入支票提款金額");
        float paper1 = ap.nextFloat();
        pp.debit(paper1);





   }

}

