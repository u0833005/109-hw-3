import java.util.*;

public class Account implements Comparable<Account>{
    public String name;
    public int balence;
    public Account(String name,int balence){
        this.name=name;
        this.balence=balence;
    }
    @Override
    public int compareTo(Account other){
        return this.balence-other.balence;
    }

    @Override
    public String toString(){
        return "帳號"+name+"餘額"+balence+" ";
    }

    public static void main(String[] args) {
        List account= Arrays.asList(
                new Account("Mary",30),
                new Account("Gary",78),
                new Account("Tery",66),
                new Account("Ben",1200),
                new Account("Alice",18)

        );
        Collections.sort(account);
        System.out.println(account);


    }

}
