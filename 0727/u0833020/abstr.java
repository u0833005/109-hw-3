public class abstr {
    abstract class cat {
        abstract void eat();
    }

    class Acat extends cat {
        void eat() {
            System.out.println("mm");
        }
    }

    class Bcat extends cat {
        void eat() {
            System.out.println("nn");
        }
    }

    class Ccat extends cat {
        void eat() {
            System.out.println("ss");
        }
    }

}



