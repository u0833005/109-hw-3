<?php
    interface Shape
    {
        function area();
        function perimeter();
    };
    class mytriangle implements Shape
    {
        var $side;
        function __construct($side)
        {
            $this->side=$side;
        }
        function area()
        {
            return $this->side/2*$this->side*sqrt(2);
        }
        function perimeter()
        {
            return $this->side*3;
        }
    }
    class myrectangle implements Shape
    {
        var $width,$height;
        function __set($varname,$value)
        {
            $this->$varname=$value;
        }
        function __get($varname)
        {
            return $this->$varname;
        }
        function area()
        {
            return $this->width*$this->height;
        }
        function perimeter()
        {
            return ($this->width+$this->height)*2;
        }
    };
    class myRoundShape implements Shape
    {
        var $radius;
        function __construct($r)
        {
            $this->radius=$r;
        }
        function area()
        {
            return $this->radius*$this->radius*PI();
        }
        function perimeter()
        {
            return $this->radius*2*PI();
        }
    }
    $side=10;
    $tri=new mytriangle($side);
    echo "邊長為{$side}長度單位的正三角形，其面積為{$tri->area()}平方單位，周長為{$tri->perimeter()}長度單位。<br/>";

    $width=20;
    $height=30;
    $rect=new myrectangle;
    $rect->width=$width;
    $rect->height=$height;
    echo "寬度為{$rect->width}長度單位且高度為{$rect->height}長度單位的矩形，其面積為{$rect->area()}平方單位，周長為{$rect->perimeter()}長度單位。<br/>";


    $radius=20;
    $round=new myRoundShape($radius);
    echo "半徑為{$round->radius}長度單位的圓形，其面積為{$round->area()}平方單位，周長為{$round->perimeter()}長度單位。";
?>