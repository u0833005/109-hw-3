<?php
    $filename="homework1.php";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>書店庫存系統網站</title>
    </head>
    <body>
        <h2>書局庫存系統網站</h2>
        <table>
            <tr>
                <th><a href="./<?php echo $filename; ?>">產品目錄</a></th>
                <th><a href="./<?php echo $filename; ?>?p=1">庫存管理</a></th>
                <th><a href="./<?php echo $filename; ?>?p=2">進貨紀錄</a></th>
                <th><a href="./<?php echo $filename; ?>?p=3">出貨紀錄</a></th>
            </tr>
        </table>
        <div>
            <?php
                date_default_timezone_set("Asia/Taipei");
                $link=mysqli_connect("localhost","root","Mm64096409","u0833011");
                if(!isset($_GET['p']))
                {
            ?>
                <table border="1">
                    <tr>
                        <th>書名</th>
                        <th>價格</th>
                        <th>剩餘數量</th>
                    </tr>
            <?php
                    $sql="SELECT bookName,bookPrice,inventoryNum FROM booksdata WHERE isPutOn=TRUE";
                    $result=mysqli_query($link,$sql);
                    while($row=mysqli_fetch_assoc($result))
                    {
            ?>
                    <tr>
                        <td><?php echo $row['bookName']?></td>
                        <td><?php echo $row['bookPrice']?></td>
                        <td><?php echo $row['inventoryNum'] ?></td>
                    </tr>
            <?php
                    }
                    mysqli_free_result($result);
                    mysqli_close($link);
            ?>
                </table>
            <?php
                }
                else if($_GET['p']=="1")
                {
                    if(!isset($_GET['method']))
                    {
            ?>
                <table border="1">
                    <tr>
                        <th>ID</th>
                        <th>書名</th>
                        <th>價格</th>
                        <th>庫存數量</th>
                        <th>是否上架</th>
                        <th>操作</th>
                    </tr>
            <?php
                        $sql="SELECT * FROM booksdata";
                        $result=mysqli_query($link,$sql);
                        while($row=mysqli_fetch_assoc($result))
                        {
            ?>
                    <tr>
                        <td><?php echo $row['bookID'] ?></td>
                        <td><?php echo $row['bookName'] ?></td>
                        <td><?php echo $row['bookPrice'] ?></td>
                        <td><?php echo $row['inventoryNum'] ?></td>
                        <td><?php echo ($row['isPutOn'])?"是":"否"; ?></td>
                        <td>
                            <a href="./<?php echo $filename; ?>?p=1&method=purchase&id=<?php echo $row['bookID']?>">進貨</a>
                            <a href="./<?php echo $filename; ?>?p=1&method=shipment&id=<?php echo $row['bookID']?>">出貨</a>
                            <a href="./<?php echo $filename; ?>?p=1&method=toggle&id=<?php echo $row['bookID']?>">上下架</a>
                            <a href="./<?php echo $filename; ?>?p=1&method=modify&id=<?php echo $row['bookID']?>">修改</a>
                            <a href="./<?php echo $filename; ?>?p=1&method=delete&id=<?php echo $row['bookID']?>">刪除</a>
                        </td>
                    </tr>
            <?php
                        }
                        mysqli_free_result($result);
                        mysqli_close($link);
            ?>
                </table>
                <a href="./<?php echo $filename; ?>?p=1&method=add">新增產品</a>
            <?php
                    }
                    else
                    {
                        if($_GET['method']=="add")
                        {
                            if(!isset($_GET['addnewbook']))
                            {
            ?>
                <form method="get" action="./<?php echo $filename; ?>">
                    <input type="hidden" name="p" value="1"/>
                    <input type="hidden" name="method" value="add"/>
                    <input type="hidden" name="addnewbook" value="addnewbook"/>
                    書名:<input type="text" name="bookname"/><br/>
                    價格:<input type="number" name="bookprice"><br/>
                    <input type="submit" value="新增"/>
                </form>
            <?php
                            }
                            else
                            {
                                $bookname=$_GET['bookname'];
                                $bookprice=$_GET['bookprice'];
                                $sql="INSERT INTO booksdata (bookName,bookPrice) VALUES ('{$bookname}',{$bookprice})";
                                mysqli_query($link,$sql);
                                mysqli_close($link);
                                header("Location: {$filename}?p=1");
                            }
                        }
                        else if($_GET['method']=="purchase")
                        {
                            if(!isset($_GET['purchaseNewbook']))
                            {
            ?>
                <form method="get" action="./<?php echo $filename; ?>">
                    <input type="hidden" name="p" value="1"/>
                    <input type="hidden" name="method" value="purchase"/>
                    <input type="hidden" name="purchaseNewbook" value="purchaseNewbook"/>
                    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"/>
                    進貨數量:<input type="number" name="num"/>
                    <input type="submit" value="進貨"/>
                </form>
            <?php
                            }
                            else
                            {
                                $sql="UPDATE booksdata SET inventoryNum=inventoryNum+{$_GET['num']} WHERE bookID={$_GET['id']}";
                                mysqli_query($link,$sql);
                                $sql="SELECT bookName FROM booksdata WHERE bookID={$_GET['id']}";
                                $result=mysqli_query($link,$sql);
                                $bookName=(mysqli_fetch_assoc($result))['bookName'];
                                mysqli_free_result($result);
                                $sql="INSERT INTO purchaserecords (bookName,purchaseNum,purchaseDate) VALUES('{$bookName}',{$_GET['num']},'" . date("Y-m-d H:i:s")."')";
                                echo $sql;
                                mysqli_query($link,$sql);
                                echo mysqli_error($link);
                                mysqli_close($link);
                                header("Location: {$filename}?p=1");
                            }
                        }
                        else if($_GET['method']=="shipment")
                        {
                            if(!isset($_GET['shipmentNewBook']))
                            {
            ?>
                <form method="get" action="./<?php echo $filename; ?>">
                    <input type="hidden" name="p" value="1"/>
                    <input type="hidden" name="method" value="shipment"/>
                    <input type="hidden" name="shipmentNewBook" value="shipmentNewBook"/>
                    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"/>
                    出貨數量:<input type="number" name="num"/>
                    <input type="submit" value="出貨"/>
                </form>
            <?php
                            }
                            else
                            {
                                $sql="UPDATE booksdata SET inventoryNum=inventoryNum-{$_GET['num']} WHERE bookID={$_GET['id']}";
                                mysqli_query($link,$sql);
                                $sql="SELECT bookName FROM booksdata WHERE bookID={$_GET['id']}";
                                $result=mysqli_query($link,$sql);
                                $bookName=(mysqli_fetch_assoc($result))['bookName'];
                                mysqli_free_result($result);
                                $sql="INSERT INTO shipmentrecords (bookName,shipmentNum,shipmentDate) VALUES('{$bookName}',{$_GET['num']},'" . date("Y-m-d H:i:s")."')";
                                echo $sql;
                                mysqli_query($link,$sql);
                                echo mysqli_error($link);
                                mysqli_close($link);
                                header("Location: {$filename}?p=1");
                            }
                        }
                        else if($_GET['method']=="toggle")
                        {
                            $sql="UPDATE booksdata SET isPutOn=1-isPutOn WHERE bookID=" . $_GET['id'];
                            mysqli_query($link,$sql);
                            mysqli_close($link);
                            header("Location: {$filename}?p=1");
                        }
                        else if($_GET['method']=="delete")
                        {
                            $sql="DELETE FROM booksdata WHERE bookID=" . $_GET['id'];
                            mysqli_query($link,$sql);
                            mysqli_close($link);
                            header("Location: {$filename}?p=1");
                        }
                        else if($_GET['method']=="modify")
                        {
                            if(!isset($_GET['modifyNewBook']))
                            {
                                $sql="SELECT * FROM booksdata WHERE bookID={$_GET['id']}";
                                $result=mysqli_query($link,$sql);
                                $row=mysqli_fetch_assoc($result);
            ?>
                <form method="get" action="./<?php echo $filename; ?>">
                    <input type="hidden" name="p" value="1"/>
                    <input type="hidden" name="method" value="modify"/>
                    <input type="hidden" name="modifyNewBook" value="modifyNewBook"/>
                    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>"/>
                    書本名稱:<input type="text" name="bookName" value="<?php echo $row['bookName']?>"/><br/>
                    價格:<input type="number" name="bookPrice" value="<?php echo $row['bookPrice']; ?>"/><br/>
                    <input type="submit" value="修改"/>
                </form>
            <?php
                            }
                            else
                            {
                                $sql="UPDATE booksdata SET bookName='{$_GET['bookName']}', bookPrice={$_GET['bookPrice']} WHERE bookID={$_GET['id']}";
                                mysqli_query($link,$sql);
                                mysqli_close($link);
                                header("Location: ./{$filename}?p=1");
                            }
                        }
                    }
                }
                else if($_GET['p']=="2")
                {
            ?>
                <table border="1">
                    <tr>
                        <th>紀錄順序</th>
                        <th>書名</th>
                        <th>進貨數量</th>
                        <th>進貨時間</th>
                    </tr>
            <?php
                    $sql="SELECT * FROM purchaserecords";
                    $result=mysqli_query($link,$sql);
                    while($row=mysqli_fetch_assoc($result))
                    {
            ?>
                    <tr>
                        <td><?php echo $row['recordID']; ?></td>
                        <td><?php echo $row['bookName']; ?></td>
                        <td><?php echo $row['purchaseNum']; ?></td>
                        <td><?php echo $row['purchaseDate']; ?></td>
                    </tr>
            <?php
                    }
                    mysqli_free_result($result);
                    mysqli_close($link);
            ?>
                </table>
            <?php
                }
                else if($_GET['p']=="3")
                {
            ?>
                <table border="1">
                    <tr>
                        <th>紀錄順序</th>
                        <th>書名</th>
                        <th>出貨數量</th>
                        <th>出貨時間</th>
                    </tr>
            <?php
                    $sql="SELECT * FROM shipmentrecords";
                    $result=mysqli_query($link,$sql);
                    while($row=mysqli_fetch_assoc($result))
                    {
            ?>
                    <tr>
                        <td><?php echo $row['recordID']; ?></td>
                        <td><?php echo $row['bookName']; ?></td>
                        <td><?php echo $row['shipmentNum']; ?></td>
                        <td><?php echo $row['shipmentDate']; ?></td>
                    </tr>
            <?php
                    }
                    mysqli_free_result($result);
                    mysqli_close($link);
            ?>
                </table>
            <?php
                }
            ?>
        </div>
    </body>
</html>