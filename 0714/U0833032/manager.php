<!DOCTYPE html>
<html>
<script src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<head>
   <meta charset="utf-8" />
   <title>管理者頁面</title>
</head>

<?php
// 是否是表單送回
if (isset($_POST["add"])) {
   // 開啟MySQL的資料庫連接
   $link = @mysqli_connect("localhost", "root", "")
      or die("無法開啟MySQL資料庫連接!<br/>");
   mysqli_select_db($link, "shop");  // 選擇資料庫
   // 建立新增記錄的SQL指令字串
   $sql = "INSERT INTO books (bookid, booktitle,bookprice, bookauthor,stock) VALUES ('";
   $sql .= $_POST["bookid"] . "','" . $_POST["booktitle"] . "','";
   $sql .= $_POST["bookprice"] . "','" . $_POST["bookauthor"]. "','" . $_POST["stock"] . "')";
   echo "<b>SQL指令: $sql</b><br/>";
   //送出UTF8編碼的MySQL指令
   mysqli_query($link, 'SET NAMES utf8');
   if (mysqli_query($link, $sql)) // 執行SQL指令
      echo "資料庫新增記錄成功, 影響記錄數: " .
         mysqli_affected_rows($link) . "<br/>";
   else
      die("資料庫新增記錄失敗<br/>");
   mysqli_close($link);      // 關閉資料庫連接
}
?>

<?PHP
if (isset($_POST["name"])) {
   // 開啟MySQL的資料庫連接
   $link = @mysqli_connect("localhost", "root", "")
      or die("無法開啟MySQL資料庫連接!<br/>");
   mysqli_select_db($link, "shop");  // 選擇資料庫
   // 建立刪除記錄的SQL指令字串
   $sql = "DELETE FROM books ";
   $sql .= " WHERE bookid = '" . $_POST["ID"] . "'";
   echo "<b>SQL指令: $sql</b><br/>";
   //送出UTF8編碼的MySQL指令
   mysqli_query($link, 'SET NAMES utf8');
   if (mysqli_query($link, $sql)) // 執行SQL指令
      echo "資料庫刪除記錄成功, 影響記錄數: " .
         mysqli_affected_rows($link) . "<br/>";
   else
      die("資料庫刪除記錄失敗<br/>");
   mysqli_close($link);      // 關閉資料庫連接
}
?>

<form name="menu" method="post" action="manager.php">
   <table align="center" bgcolor="#FFFDD0">
      <caption>新增產品:</caption>
      <tr>
         <td>
            <font size="2">編號:</font>
         </td>
         <td><input type="text" name="bookid" size="15" maxlength="10" />
         </td>
      </tr>
      <tr>
         <td>
            <font size="2">圖書名稱:</font>
         </td>
         <td><input type="text" name="booktitle" size="15" maxlength="10" />
         </td>
      </tr>
      <tr>
         <td>
            <font size="2">價格:</font>
         </td>
         <td><input type="text" name="bookauthor" size="15" maxlength="10" />
         </td>
      </tr>
      <tr>
         <td>
            <font size="2">作者:</font>
         </td>
         <td><input type="text" name="bookprice" size="15" maxlength="10" />
         </td>
      </tr>
	        <tr>
         <td>
            <font size="2">庫存:</font>
         </td>
         <td><input type="text" name="stock" size="15" maxlength="10" />
         </td>
      </tr>
      <tr>
         <td colspan="2" align="center">
            <input name="add" type="submit" value="新增" />
         </td>
      </tr>

   </table>

</form>
<hr/>| <a href="shop.php">網路商店</a>
| <a href="shoppingcart.php">檢視購物車內容</a> |
<a href="manager.php">管理頁面</a>
<body bgcolor="#FFFDD0" text="blue">
   <center>
      <table border="1">
         <tr bgcolor="#CC99FF">
            <td>編號</td>
            <td>名稱</td>
            <td>價格</td>
            <td>作者</td>
			<td>庫存</td>
            <td>刪除</td>
            <td>修改</td>
         </tr>

         <?php
         // 插入函式庫的PHP檔案
         require_once("dataAccess.php");
         // 建立dataAccess物件的資料庫連接
         $dao = new dataAccess(
            "localhost",
            "root",
            "",
            "shop"
         );
         $sql = "SELECT * FROM books";  // 建立SQL指令字串
         $dao->fetchDB($sql);  // 執行SQL查詢指令字串
         $flag = false;
         // 顯示資料庫內容
         while ($row = $dao->getRecord()) {
            if ($flag) {
               $flag = false;
               $color = "#E6E6FA";
            } else {
               $flag = true;
               $color = "#FFFDD0";
            }
            // 顯示選購商品的表單
         ?>

            <form action="manager.php" method="post">
               <input type="hidden" name="ID" value="<?php echo $row["bookid"] ?>" />
               <input type="hidden" name="Name" value="<?php echo $row["booktitle"] ?>" />
               <input type="hidden" name="Price" value="<?php echo $row["bookprice"]; ?>" />
               <input type="hidden" name="bookauthor" value="<?php echo $row["bookauthor"]; ?>" />
               <input type="hidden" name="stock" value="<?php echo $row["stock"]; ?>" />
               <tr bgcolor="<?php echo $color ?>">
                  <td><?php echo $row["bookid"] ?></td>
                  <td><?php echo $row["booktitle"] ?></td>
                  <td><?php echo $row["bookprice"] ?></td>
                  <td><?php echo $row["bookauthor"] ?></td>
				  <td><?php echo $row["stock"] ?></td>

                  <td valign="top">
                     <input name="name" type="hidden" value="<?php echo $row["bookid"] ?>" />
                     <input name="name1" type="submit" value="刪除" />
                  </td>
            </form>
            <td>
               <form action="insert.php" method="post">
                  <input name="Update" type="hidden" value="<?php echo $row["bookid"] ?>" />
                  <input name="Update1" type="submit" value="修改" />
               </form>
            </td>
            </tr>
         <?php
         } ?>
      </table>
   </center>
   <input type="button" value="查詢" id="show"></br>
   <p class="word"></p>
    <script>
      $(function() {
               $("#show").on("click", function() {        
                  $.get("http://localhost:8080/data.php",function(data){    
                        let search = $(".search").val();
                        let ID = $(".bookid").val();
                              $(".word").html(data);                      
                        
                  });            
               });               
      });                   
   </script>
   
</body>

</html>