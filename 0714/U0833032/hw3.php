<?php
interface Shape{
    function area();
    function perimeter();   
}

class rectangle implements Shape{
    var $sidex;
    var $sidey;
    function __construct($sidex,$sidey){
        $this->sidex=$sidex;
        $this->sidey=$sidey;
    }
    function area(){
        return "矩形的面積是:" .($this->sidex*$this->sidey);
    }
    function perimeter(){
        return "矩形的周長是:" .($this->sidex*2+$this->sidey*2);
    }
}

class circle implements Shape{
    var $radius;
    function __construct($radius){
        $this->radius=$radius;
    }
    function area(){
        return "圓形面積是:".($this->radius*$this->radius*pi());
    }
    function perimeter(){
        return "圓形周長是:".($this->radius*pi());
    }
}

$rect=new rectangle(10,5);
echo $rect->area() . "<br/>";
echo $rect->perimeter() . "<br/>";

$circle=new circle(5);
echo $circle->area() ."<br/>";
echo $circle->perimeter() ."<br/>";
?>