<?php
    interface Shape{
        function area();
        function perimeter();
    }
    class Triangle implements Shape{
        var $side;
        function __construct($side){
            $this->side=$side;
        }
        function area(){
            return $this->side/2*$this->side*sqrt(2);
        }
        function perimeter(){
            return $this->side*3;
        }
    }
    class Circles implements Shape{
        var $radius;
        function __construct($radiu){
            $this->radiu=$radiu;
        }
        function area(){
            return $this->radiu*$this->radiu*PI();
        }
        function perimeter(){
            return $this->radiu*2*PI();
        }
    }
   class Squares implements Shape{
        var $lside;
        var $sside;
        function __construct($lside,$sside){
            $this->lside=$lside;
            $this->sside=$sside;
        }
        function area(){
            return $this->lside*$this->sside;
        }
        function perimeter(){
            return ($this->lside+$this->sside)*2;
        }
    }
    $tri=new Triangle(1);
    $cir=new Circles(1);
    $squ=new Squares(1,1);
    echo "正三角形的面積是：".$tri->area()."<br/>"."正三角形周長是：".$tri->perimeter()."<br/>";
    echo "圓形的面積是：".$cir->area()."<br/>"."圓形周長是：".$cir->perimeter()."<br/>";
    echo "矩形的面積是：".$squ->area()."<br/>"."矩形周長是：".$squ->perimeter()."<br/>";
?>