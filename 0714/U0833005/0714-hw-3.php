<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"></meta>
	<title></title>
</head>
<body>
<?php
    
    interface Shape{
        function area();
        function perimeter();
    }
    class res implements Shape{
        var $sidex,$sidey;
        function __construct($sidex,$sidey){
            $this->sidex=$sidex;
            $this->sidey=$sidey;
        }
        function area(){
            return "矩形面積是:".($this->sidex*$this->sidey);
        }
        function perimeter(){
            return "矩形周長是:".($this->sidex*2+$this->sidey*2);
        }
    }
    class cir implements Shape{
        var $side;
        function __construct($side){
            $this->side=$side;
        }
        function area(){
            return "圓形面積是:".($this->side/2*$this->side/2*pi());
        }
        function perimeter(){
            return "圓形周長是:".($this->side*pi());
        }
    }
    $r = new res(10,5);
    $c = new cir(10);
    echo $r->area()."<br/>";
    echo $r->perimeter()."<br/>";
    echo $c->area()."<br/>";
    echo $c->perimeter();
?>
</body>
</html>