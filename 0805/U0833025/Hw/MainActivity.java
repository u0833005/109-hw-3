package com.example.todo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    DatePickerDialog.OnDateSetListener dateSetListener; //date監聽器
    TimePickerDialog.OnTimeSetListener timeSetListener; //time監聽器
//    EditText todo;
//    TextView date,time;
//    Button btnDate,btnTime,btnWrite,btnRead,btnDelete;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView date = findViewById(R.id.date);
        TextView time = findViewById(R.id.time);
        Button btnDate = findViewById(R.id.btnDate);
        Button btnTime = findViewById(R.id.btnTime);

        EditText todo = findViewById(R.id.todo);
        Button btnWrite = findViewById(R.id.btnWrite);
        Button btnRead = findViewById(R.id.btnRead);
        Button btnDelete = findViewById(R.id.btnDelete);
        TextView textView = findViewById(R.id.textView);

        btnDate.setOnClickListener(v -> {
            Calendar calender = Calendar.getInstance();

            int year = calender.get(Calendar.YEAR);  //從cal中讀出YEAR
            int month = calender.get(Calendar.MONTH);    //從cal中讀出MONTH
            int day = calender.get(Calendar.DAY_OF_MONTH);   //從cal中讀出DAY_OF_MONTH

            DatePickerDialog Datedialog = new DatePickerDialog
                    (MainActivity.this, dateSetListener, year,month,day);
            //產生DatePickerDialog物件
            Datedialog.show();
            //顯示
        });

        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;  //存時為0~11，故要+1
                String Date = year+"/"+month+"/"+day;
                date.setText(Date);
            }
        };

        btnTime.setOnClickListener(v -> {
            Calendar c = Calendar.getInstance();

            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            TimePickerDialog Timedialog = new TimePickerDialog(MainActivity.this,timeSetListener,hour,minute, DateFormat.is24HourFormat(MainActivity.this));
            Timedialog.show();
        });

        timeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                if(minute<10){
                    String Time = hour + ":0" + minute;
                    time.setText(Time);
                }else{
                    String Time = hour + ":" + minute;
                    time.setText(Time);
                }
            }
        };



        btnWrite.setOnClickListener(v -> {
            FileOutputStream fos;   //寫檔串流

            try{
                fos = openFileOutput("Test.txt", Context.MODE_PRIVATE); //MODE_PRIVATE只有本應用程式可存取
                fos.write(date.getText().toString().getBytes());
                fos.write(time.getText().toString().getBytes());
                fos.write(todo.getText().toString().getBytes());
                fos.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        });

        btnRead.setOnClickListener(v -> {
            FileInputStream fis;    //讀檔串流
            BufferedInputStream bis;    //一段一段讀檔

            try{
                fis = openFileInput("Test.txt");
                bis = new BufferedInputStream(fis);
                byte[] bufferByte = new byte[200];  //宣告200byte陣列
                textView.setText("");   //先清空textView

                while (true){
                    int flag = bis.read(bufferByte);    //讀了多少byte
                    if(flag == -1) break;   //-1為讀完
                    else{
                        textView.append(new String(bufferByte,0,flag));
                    }
                }
                bis.close();
                fis.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        });

        btnDelete.setOnClickListener(v -> {
            File file = new File(getFilesDir() + "/" + "Test.txt"); //應用程式儲存的路徑
            if(file.exists()){
                file.delete();
                textView.setText("Delete");
            }else{
                textView.setText("file not find");
            }
        });
    }
}