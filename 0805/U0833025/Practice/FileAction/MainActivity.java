package com.example.fileaction;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText edit = findViewById(R.id.edit);
        Button read = findViewById(R.id.read);
        Button write = findViewById(R.id.write);
        Button delete = findViewById(R.id.delete);
        TextView textView = findViewById(R.id.textView);

        write.setOnClickListener(v -> {
            FileOutputStream fos;   //寫檔串流

            try{
                fos = openFileOutput("Test.txt", Context.MODE_PRIVATE); //MODE_PRIVATE只有本應用程式可存取
                fos.write(edit.getText().toString().getBytes());
                fos.close();
                //File file = new File(getFilesDir() + "/" + fos);
            }catch (IOException e){
                e.printStackTrace();
            }
        });

        read.setOnClickListener(v -> {
            FileInputStream fis;    //讀檔串流
            BufferedInputStream bis;    //一段一段讀檔

            try{
                fis = openFileInput("Test.txt");
                bis = new BufferedInputStream(fis);
                byte[] bufferByte = new byte[200];  //宣告200byte陣列
                textView.setText("");   //先清空textView

                while (true){
                    int flag = bis.read(bufferByte);    //讀了多少byte
                    if(flag == -1) break;   //-1為讀完
                    else{
                        textView.append(new String(bufferByte,0,flag));
                    }
                }
                bis.close();
                fis.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        });

        delete.setOnClickListener(v -> {
            File file = new File(getFilesDir() + "/" + "Test.txt"); //應用程式儲存的路徑
            if(file.exists()){
                file.delete();
                textView.setText("Delete");
            }else{
                textView.setText("file not find");
            }
        });


    }
}