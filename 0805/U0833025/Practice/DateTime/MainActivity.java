package com.example.datetime;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;

import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    DatePickerDialog.OnDateSetListener dateSetListener; //date監聽器
    TimePickerDialog.OnTimeSetListener timeSetListener; //time監聽器
//    Button time,date;
//    TextView textTime,textDate,text;
//    CheckBox check,check2,check3,check4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        setTitle("Hello");  //更改標題
//
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);  //返回鍵
//        getSupportActionBar().setLogo(R.drawable.loupe);  //logo
//        getSupportActionBar().setDisplayUseLogoEnabled(true);  //logo
//
//        TextView textTime = findViewById(R.id.textTime);
//        Button time = findViewById(R.id.time);
//        TextView textDate = findViewById(R.id.textDate);
//        Button date = findViewById(R.id.date);
//
//        date.setOnClickListener(v -> {
//            Calendar cal = Calendar.getInstance();
//
//            int year = cal.get(Calendar.YEAR);  //從cal中讀出YEAR
//            int month = cal.get(Calendar.MONTH);    //從cal中讀出MONTH
//            int day = cal.get(Calendar.DAY_OF_MONTH);   //從cal中讀出DAY_OF_MONTH
//
//            DatePickerDialog dialog = new DatePickerDialog
//                    (MainActivity.this, dateSetListener, year,month,day);
//            //產生DatePickerDialog物件
//            dialog.show();
//            //顯示
//        });
//
//        dateSetListener = new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
//                month = month + 1;  //存時為0~11，故要+1
////                Log.e("日期為:",+year+"/"+month+"/"+day);  //將值傳送至logcat/error
////                Log.d("日期為:",+year+"/"+month+"/"+day);  //將值傳送至logcat/debug
//                String date = +year+"/"+month+"/"+day;
//                textDate.setText(date);
//            }
//        };
//
//        time.setOnClickListener(v -> {
//            Calendar c = Calendar.getInstance();
//
//            int hour = c.get(Calendar.HOUR_OF_DAY);
//            int minute = c.get(Calendar.MINUTE);
//
//            TimePickerDialog timedialog = new TimePickerDialog(MainActivity.this,timeSetListener,hour,minute, DateFormat.is24HourFormat(MainActivity.this));
//            timedialog.show();
//        });
//
//        timeSetListener = new TimePickerDialog.OnTimeSetListener() {
//            @Override
//            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
//                if(minute<10){
//                    String time = hour + ":0" + minute;
//                    textTime.setText(time);
//                }else{
//                    String time = hour + ":" + minute;
//                    textTime.setText(time);
//                }
//            }
//        };//timeSetListener
        CheckBox check = findViewById(R.id.check);
        TextView text = findViewById(R.id.textView2);
        CheckBox check2 = findViewById(R.id.check2);
        CheckBox check3 = findViewById(R.id.check3);
        CheckBox check4 = findViewById(R.id.check4);

//        check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                text.setText(isChecked?"true":"false");
//            }
//        });

        CompoundButton.OnCheckedChangeListener checkBoxOnCheckedChange = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton ButtonView, boolean isChecked) {
                if(isChecked){
                    text.setText(ButtonView.getText().toString()+"被選取");
                }else{
                    text.setText(ButtonView.getText().toString()+"被取消");
                }
            }
        };

        check.setOnCheckedChangeListener(checkBoxOnCheckedChange);
        check2.setOnCheckedChangeListener(checkBoxOnCheckedChange);
        check3.setOnCheckedChangeListener(checkBoxOnCheckedChange);
        check4.setOnCheckedChangeListener(checkBoxOnCheckedChange);


    }//onCreate
//    CompoundButton.OnCheckedChangeListener checkBoxOnCheckedChange = new CompoundButton.OnCheckedChangeListener() {
//        @Override
//        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//            if(isChecked){
//                Toast.makeText(getApplicationContext(),buttonView.getText()+"被選取",Toast.LENGTH_LONG).show();
//            }else{
//                Toast.makeText(getApplicationContext(),buttonView.getText()+"被取消",Toast.LENGTH_LONG).show();
//            }
//        }
//    };

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item){
//        int id = item.getItemId();
//        if(id == android.R.id.home){
//            finish();
//        }else if(id == R.id.hello){
//
//        }
//        return super.onOptionsItemSelected(item);
//    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu){
////        getMenuInflater().inflate(R.menu.menu, menu);
////        return true;
//        SubMenu subMenu = menu.addSubMenu("");
//        subMenu.add("H1")
//                .setIcon(R.drawable.loupe)
//                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem menuItem) {
//                        return false;
//                    }
//                });
//        subMenu.add("H2")
//                .setTitle("H2")
//                .setIcon(R.drawable.loupe)
//                .setOnMenuItemClickListener(item->true)
//                .setShowAsAction(MenuItem.SHOW_AS_ACTION_WITH_TEXT);
//
//        MenuItem item = subMenu.getItem();
//        item.setIcon(R.drawable.loupe);
//        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS|MenuItem.SHOW_AS_ACTION_WITH_TEXT);
//
//        return true;
//    }
}//MainActivity