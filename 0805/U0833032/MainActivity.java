package com.example.a0805hw;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    DatePickerDialog.OnDateSetListener dateSetListener;
    TimePickerDialog.OnTimeSetListener timeSetListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView choosedate =findViewById(R.id.choosedate);
        TextView choosetime =findViewById(R.id.choosetime);
        EditText input =findViewById(R.id.input);
        Button btndate =findViewById(R.id.btndate);
        Button btntime =findViewById(R.id.btntime);
        Button btndelete =findViewById(R.id.btndelete);
        Button btnnew =findViewById(R.id.btnnew);
        Button btnread =findViewById(R.id.btnread);
        TextView load =findViewById(R.id.load);

        btndate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cla = Calendar.getInstance();
                int year=cla.get(Calendar.YEAR);
                int month=cla.get(Calendar.MONTH);
                int day = cla.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog=new DatePickerDialog(
                        MainActivity.this,dateSetListener,year,month,day
                );
                dialog.show();
            }
        });
        dateSetListener =new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month=month+1;//0-11
                String date= +year+"/"+month+"/"+day;
                choosedate.setText(date);
            }
        };

        btntime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c =Calendar.getInstance();
                int hour =c.get(Calendar.HOUR_OF_DAY);
                int min = c.get(Calendar.MINUTE);
                TimePickerDialog dialog =new TimePickerDialog(
                        MainActivity.this,timeSetListener,hour,min, DateFormat.is24HourFormat(MainActivity.this)

                );
                dialog.show();
            }
        });
        timeSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                if(minute<10) {
                    Log.d("onTimeSet:hh/mm:", +hour + "/" + minute);
                    String time = hour + ":0" + minute;
                    choosetime.setText(time);

                }else {
                    Log.d("onTimeSet:hh/mm:", +hour + "/" + minute);
                    String time = hour + ":" + minute;
                    choosetime.setText(time);
                }

            }
        };

        btnnew.setOnClickListener(view->{
            FileOutputStream fos;
            try{
                fos=openFileOutput("Test.txt", Context.MODE_PRIVATE);
                fos.write(choosedate.getText().toString().getBytes());
                fos.write(choosetime.getText().toString().getBytes());
                fos.write(input.getText().toString().getBytes());
                fos.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        });

        btnread.setOnClickListener(v->{
            FileInputStream fis;
            BufferedInputStream bis;

            try{
                fis = openFileInput("Test.txt");
                bis = new BufferedInputStream(fis);
                byte[] bufferBytes = new byte[200];
                load.setText("");

                while(true){
                    int flag =bis.read(bufferBytes);//讀了多少byte
                    if(flag==-1){
                        break;
                    }else{
                        load.append(new String(bufferBytes,0,flag));
                    }
                }
                bis.close();
                fis.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        });

        btndelete.setOnClickListener(v->{
            File file =new File(getFilesDir()+"/"+"Test.txt");//https://www.itread01.com/content/1542555550.html
            if (file.exists()){
                file.delete();
                load.setText("delete!");
            }else{
                load.setText("file not found");
            }
        });

    }
}