package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.icu.util.TimeZone;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView output=(TextView)findViewById(R.id.output);
        EditText input=(EditText)findViewById(R.id.input);

        Button btnC=(Button)findViewById(R.id.buttonC);
        Button btn0=(Button)findViewById(R.id.button0);
        Button btn1=(Button)findViewById(R.id.button1);
        Button btn2=(Button)findViewById(R.id.button2);
        Button btn3=(Button)findViewById(R.id.button3);
        Button btn4=(Button)findViewById(R.id.button4);
        Button btn5=(Button)findViewById(R.id.button5);
        Button btn6=(Button)findViewById(R.id.button6);
        Button btn7=(Button)findViewById(R.id.button7);
        Button btn8=(Button)findViewById(R.id.button8);
        Button btn9=(Button)findViewById(R.id.button9);

        Button btnSubstract=(Button)findViewById(R.id.buttonAbstract); //-
        Button btn_Substract=(Button)findViewById(R.id.buttonSubstract); //-
        Button btnLeft=(Button)findViewById(R.id.buttonLeft); //(
        Button btnRight=(Button)findViewById(R.id.buttonRight); //)
        Button btnDivide=(Button)findViewById(R.id.buttonDivide); // divide
        Button btnProduct=(Button)findViewById(R.id.buttonProduct); //*
        Button btnAdd=(Button)findViewById(R.id.buttonAdd); //+
        Button btnDisappear=(Button)findViewById(R.id.buttonDisappear); //disappear
        Button btnEnd=(Button)findViewById(R.id.buttonEnd); //calculate

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine=manager.getEngineByName("js");

        btnC.setOnClickListener(v -> {
            input.setText("");
            output.setText("結果顯示");
        });
        btn0.setOnClickListener(v -> input.append("0"));
        btn1.setOnClickListener(v -> input.append("1"));
        btn2.setOnClickListener(v -> input.append("2"));
        btn3.setOnClickListener(v -> input.append("3"));
        btn4.setOnClickListener(v -> input.append("4"));
        btn5.setOnClickListener(v -> input.append("5"));
        btn6.setOnClickListener(v -> input.append("6"));
        btn7.setOnClickListener(v -> input.append("7"));
        btn8.setOnClickListener(v -> input.append("8"));
        btn9.setOnClickListener(v -> input.append("9"));

        btnSubstract.setOnClickListener(v -> input.append("-"));
        btn_Substract.setOnClickListener(v -> input.append("-"));
        btnLeft.setOnClickListener(v -> input.append("("));
        btnRight.setOnClickListener(v -> input.append(")"));
        btnDivide.setOnClickListener(v -> input.append("/"));
        btnProduct.setOnClickListener(v -> input.append("*"));
        btnAdd.setOnClickListener(v -> input.append("+"));

        btnDisappear.setOnClickListener(view -> input.setText(input.getText().toString().replaceFirst(".$","")));
        btnEnd.setOnClickListener(view -> {
            try {
                List<String> inorderExp=new ArrayList<>();
                String temp="";
                String userInput=input.getText().toString();
                for(int i=0;i<userInput.length();i++)
                {
                    if(userInput.charAt(i)<48) //operator
                    {
                        if(temp.length()>0)
                        {
                            inorderExp.add(temp);
                            temp="";
                        }
                        inorderExp.add(String.valueOf(userInput.charAt(i)));
                    }
                    else //operand
                        temp+=userInput.charAt(i);
                }
                if(temp.length()>0)
                    inorderExp.add(temp);

                List<String> isp=new ArrayList<>(Arrays.asList("(","1","+","-","4","*","/"));
                List<String> icp=new ArrayList<>(Arrays.asList("0","+","-","3","*","/"));
                /* rule
                    symbol: "(",")","+","-","*","/"
                    isp>=icp loop pop;
                    icp>isp push;
                    icp press isp
                    +,- press  (
                    *,/ press +,-,(
                 */
                Stack<String> stack=new Stack<>();
                List<String> postorderExp=new ArrayList<>();
                List<String> symbols=new ArrayList<>(Arrays.asList("+","-","*","/"));
                for(String token : inorderExp)
                {
                    if(token.equals("("))
                        stack.push(token);
                    else if(token.equals(")"))
                    {
                        while(true)
                        {
                            temp=stack.pop();
                            if(!temp.equals("("))
                                postorderExp.add(temp);
                            else
                                break;
                        }
                    }
                    else if(symbols.contains(token)) //a kind of symbol
                    {
                        while(!stack.isEmpty())
                        {
                            temp=stack.peek();
                            if(isp.indexOf(temp)>=icp.indexOf(token))
                                postorderExp.add(stack.pop());
                            else //isp<icp
                                break;
                        }
                        stack.push(token);
                    }
                    else //number
                        postorderExp.add(token);
                }
                while(!stack.isEmpty())
                    postorderExp.add(stack.pop());
                Stack<String> finalStack=new Stack<>();
                for(String token:postorderExp)
                {
                    if(symbols.contains(token))
                    {
                        double b=Double.valueOf(finalStack.pop());
                        double a=Double.valueOf(finalStack.pop());
                        switch (token)
                        {
                            case "+":
                                finalStack.push(String.valueOf(a+b));
                                break;
                            case "-":
                                finalStack.push(String.valueOf(a-b));
                                break;
                            case "*":
                                finalStack.push(String.valueOf(a*b));
                                break;
                            case "/":
                                finalStack.push(String.valueOf(a/b));
                        }
                    }
                    else
                        finalStack.push(token);
                }
                //output.setText(String.valueOf((double)engine.eval(input.getText().toString())));
                output.setText(finalStack.pop());
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
                output.setText("無法計算");
            }
        });
    }
}