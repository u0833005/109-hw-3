package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText etInput=(EditText)findViewById(R.id.etInput);
        TextView tvOutput=(TextView)findViewById(R.id.tvOutput);
        Button btnRead=(Button)findViewById(R.id.btnRead);
        Button btnWrite=(Button)findViewById(R.id.btnWrite);
        Button btnDelete=(Button)findViewById(R.id.btnDelete);
        btnWrite.setOnClickListener(v -> {
            OutputStream fos=null;
            BufferedOutputStream bos=null;
            try
            {
                fos=openFileOutput("test.txt", Context.MODE_PRIVATE);
                bos=new BufferedOutputStream(fos);
                bos.write(etInput.getText().toString().getBytes());
                bos.close(); //auto flush
                fos.close();
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
            }
        });
        btnRead.setOnClickListener(v ->
        {
            InputStream fis=null;
            BufferedInputStream bis=null;
            int bufferSize=1024; //buffer size is 1KB
            byte[] buffer=new byte[bufferSize];
            tvOutput.setText("");
            try
            {
                fis=openFileInput("test.txt");
                bis=new BufferedInputStream(fis);
                while(bis.read(buffer)!=-1)
                    tvOutput.append(new String(buffer));
                bis.close();
                fis.close();
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
                tvOutput.setText("file");
                Toast.makeText(getBaseContext(),"File doesn't exist",Toast.LENGTH_SHORT).show();
            }
        });
        btnDelete.setOnClickListener(v ->
        {
            File file=new File(getFilesDir()+"/test.txt");
            if(file.exists())
            {
                file.delete();
                Toast.makeText(getBaseContext(),"File deleted",Toast.LENGTH_SHORT).show();
            }
            else
                Toast.makeText(getBaseContext(),"File doesn't exist",Toast.LENGTH_SHORT).show();
        });
    }
}