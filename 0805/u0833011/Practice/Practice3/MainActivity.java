package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tvDate=(TextView) findViewById(R.id.tvDate);
        TextView tvTime=(TextView) findViewById(R.id.tvTime);
        Button btnDate=(Button) findViewById(R.id.btnDate);
        Button btnTime=(Button) findViewById(R.id.btnTime);
        DatePickerDialog.OnDateSetListener dateSetListener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                String date=i+"/"+(i1+1)+"/"+i2;
                tvDate.setText(date);
            }
        };
        TimePickerDialog.OnTimeSetListener timeSetListener=new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                String time=i+":"+i1;
                tvTime.setText(time);
            }
        };
        btnDate.setOnClickListener(v ->
        {
            Calendar cal= Calendar.getInstance(); //get current date
            int year=cal.get(Calendar.YEAR);
            int month=cal.get(Calendar.MONTH);
            int day=cal.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dateDialog=new DatePickerDialog(this,dateSetListener,year,month,day);
            dateDialog.show();
        });
        btnTime.setOnClickListener(v ->
        {
            Calendar cal=Calendar.getInstance();
            int hour=cal.get(Calendar.HOUR_OF_DAY);
            int minute=cal.get(Calendar.MINUTE);
            TimePickerDialog timeDialog=new TimePickerDialog(this,timeSetListener,hour,minute,true);
            timeDialog.show();
        });
    }
}