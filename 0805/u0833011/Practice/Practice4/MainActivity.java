package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("CheckBox Practice");
        CheckBox cbIsEat=(CheckBox) findViewById(R.id.cbIsEat);
        TextView tvIsEat=(TextView) findViewById(R.id.tvIsEat);
        TextView tvOutput=(TextView) findViewById(R.id.tvOutput);
        CheckBox cbGood=(CheckBox) findViewById(R.id.cbGood);
        CheckBox cbNormal=(CheckBox) findViewById(R.id.cbNormal);
        CheckBox cbBad=(CheckBox) findViewById(R.id.cbBad);
        List<CheckBox> cbList=new ArrayList<>(Arrays.asList(cbGood,cbNormal,cbNormal));
        cbGood.setVisibility(View.INVISIBLE);
        cbNormal.setVisibility(View.INVISIBLE);
        cbBad.setVisibility(View.INVISIBLE);
        cbIsEat.setOnClickListener(view -> {
            if(cbIsEat.isChecked())
            {
                cbGood.setVisibility(View.VISIBLE);
                cbNormal.setVisibility(View.VISIBLE);
                cbBad.setVisibility(View.VISIBLE);
                tvOutput.setText("好吃嗎?");
            }
            else{
                cbGood.setVisibility(View.INVISIBLE);
                cbNormal.setVisibility(View.INVISIBLE);
                cbBad.setVisibility(View.INVISIBLE);
                tvOutput.setText("");
            }
            tvIsEat.setText((cbIsEat.isChecked())?"吃了":"還沒啦XD");
        });
        CompoundButton.OnCheckedChangeListener myListener=new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                cbList.forEach(i -> i.setChecked(false));
                compoundButton.setChecked(b);
                if(b)
                {
                    switch(compoundButton.getText().toString())
                    {
                        case "好吃":
                            Toast.makeText(MainActivity.this,"謝謝你的評語",Toast.LENGTH_SHORT).show();
                            break;
                        case "普通":
                            Toast.makeText(MainActivity.this,"我們會更加好的!",Toast.LENGTH_SHORT).show();
                            break;
                        case "難吃":
                            Toast.makeText(MainActivity.this,"我們會改進的",Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        };
        cbGood.setOnCheckedChangeListener(myListener);
        cbNormal.setOnCheckedChangeListener(myListener);
        cbBad.setOnCheckedChangeListener(myListener);

    }
}