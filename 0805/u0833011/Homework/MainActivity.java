package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("待辦事項(Todo)");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_launcher_foreground);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView tvDate=(TextView) findViewById(R.id.tvDate);
        TextView tvTime=(TextView) findViewById(R.id.tvTime);
        Button btnDate=(Button) findViewById(R.id.btnDate);
        Button btnTime=(Button) findViewById(R.id.btnTime);
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        builder.setTitle("當前所有待辦事件");
        AlertDialog dialog=builder.create();
        DatePickerDialog.OnDateSetListener dateSetListener=new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                String date=digitToString(i)+"/"+digitToString(i1+1)+"/"+digitToString(i2);
                tvDate.setText(date);
            }
        };
        TimePickerDialog.OnTimeSetListener timeSetListener=new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                String time=digitToString(i)+":"+digitToString(i1);
                tvTime.setText(time);
            }
        };
        btnDate.setOnClickListener(v ->
        {
            Calendar cal= Calendar.getInstance(); //get current date
            int year=cal.get(Calendar.YEAR);
            int month=cal.get(Calendar.MONTH);
            int day=cal.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dateDialog=new DatePickerDialog(this,dateSetListener,year,month,day);
            dateDialog.show();
        });
        btnTime.setOnClickListener(v ->
        {
            Calendar cal=Calendar.getInstance();
            int hour=cal.get(Calendar.HOUR_OF_DAY);
            int minute=cal.get(Calendar.MINUTE);
            TimePickerDialog timeDialog=new TimePickerDialog(this,timeSetListener,hour,minute,true);
            timeDialog.show();
        });
        EditText etTodo=(EditText) findViewById(R.id.etTodo);
        Button btnAdd=(Button) findViewById(R.id.btnAdd);
        Button btnRead=(Button) findViewById(R.id.btnRead);
        Button btnDelete=(Button) findViewById(R.id.btnDelete);
        btnAdd.setOnClickListener(v ->
        {
            OutputStream fos=null;
            BufferedOutputStream bos=null;
            try
            {
                fos=openFileOutput("todo.txt", Context.MODE_APPEND);
                bos=new BufferedOutputStream(fos);
                bos.write((tvDate.getText().toString()+" "+tvTime.getText().toString()+" "+etTodo.getText().toString()+"\n").getBytes()); //TODO
                bos.close(); //auto flush
                fos.close();
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
            }
        });
        btnRead.setOnClickListener(v ->
        {
            InputStream fis=null;
            BufferedInputStream bis=null;
            int bufferSize=1024; //buffer size is 1KB
            byte[] buffer=new byte[bufferSize];
            try
            {
                fis=openFileInput("todo.txt");
                bis=new BufferedInputStream(fis);
                StringBuilder strBuilder=new StringBuilder();
                while(bis.read(buffer)!=-1)
                    strBuilder.append(new String(buffer));
                String output=strBuilder.toString();
                bis.close();
                fis.close();
                dialog.setMessage(output);
                dialog.show();
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
                Toast.makeText(getBaseContext(),"File doesn't exist",Toast.LENGTH_SHORT).show();
            }
        });
        btnDelete.setOnClickListener(v ->
        {
            File file=new File(getFilesDir()+"/todo.txt");
            if(file.exists())
            {
                file.delete();
                Toast.makeText(getBaseContext(),"Todo deleted",Toast.LENGTH_SHORT).show();
            }
            else
                Toast.makeText(getBaseContext(),"Todo file doesn't exist",Toast.LENGTH_SHORT).show();
        });
    }
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu,menu);
        menu.add(Menu.NONE,1234,2,"組織資訊"); //deprecated  return type: MenuItem
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                break;
            case R.id.about:
                Toast.makeText(getBaseContext(),"由田凱維技術支持",Toast.LENGTH_SHORT).show();
                break;
            case 1234:
                Toast.makeText(getBaseContext(),"來自NUU",Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public static String digitToString(int digit)
    {
        return (digit>10)?String.valueOf(digit):"0"+String.valueOf(digit);
    }
}