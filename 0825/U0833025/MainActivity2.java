package com.example.a1129;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class MainActivity2 extends AppCompatActivity {

    private static List<String> chat = new ArrayList<>();
    private static ArrayAdapter<String> adapter;

    public BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.hasExtra("newMessage")){
                Log.e("broadcast",intent.getStringExtra("newMessage"));
                chat.add(MainActivity.friend+":"+intent.getStringExtra("newMessage"));
                adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        EditText etInput = findViewById(R.id.et_input);
        ImageButton btnSend = findViewById(R.id.btn_send);
        ListView lvChat = findViewById(R.id.lv_chat);

        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if(!task.isSuccessful()){
                    Log.e("Firebase","fetch token failed");
                }else{
                    Log.e("Token",task.getResult());
                }
            }
        });

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, new IntentFilter("Message"));

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, chat);
        lvChat.setAdapter(adapter);

        btnSend.setOnClickListener((v)->{
            sendNotifiction(etInput.getText().toString());
            etInput.setText("");
        });
    }

    private void sendNotifiction(String message){
        new Thread(()->{
            try{
                URL url =new URL("http://192.168.43.171/11.29/send_notification.php");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setUseCaches(false);

                List<Pair<String,String>> params = new ArrayList<>();
                params.add(new Pair<>("sender",MainActivity.currentAccount));
                params.add(new Pair<>("recevier",MainActivity.friend));
                params.add(new Pair<>("content",message));

                OutputStream outputStream = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));

                writer.write(MainActivity.getQuery(params));
                writer.flush();
                writer.close();
                outputStream.close();

                connection.connect();

                if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                    chat.add(MainActivity.currentAccount + ":" + message);
                    runOnUiThread(()->adapter.notifyDataSetChanged());
                }else{
                    runOnUiThread(()-> Toast.makeText(this,"發送失敗",Toast.LENGTH_SHORT).show());
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}