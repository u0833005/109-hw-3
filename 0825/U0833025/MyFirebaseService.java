package com.example.a1129;

import android.content.Intent;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if(remoteMessage.getNotification() != null){
            Log.e("title",remoteMessage.getNotification().getTitle());
            Log.e("body",remoteMessage.getNotification().getBody());

            notifyNewMessage(remoteMessage.getNotification().getBody());
        }
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);

    }

    public void notifyNewMessage(String message){
        Intent intent = new Intent("Message");

        intent.putExtra("newMessage", message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

    }
}
