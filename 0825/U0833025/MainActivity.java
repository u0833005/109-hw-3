package com.example.a1129;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static String currentAccount = "";
    public static String friend = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText etAccount = findViewById(R.id.et_account);
        EditText etPassword = findViewById(R.id.et_password);
        EditText etFriend = findViewById(R.id.et_friend);
        Button btnLogin = findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(v-> login(etAccount.getText().toString(),etPassword.getText().toString(),etFriend.getText().toString()));


//        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
//            @Override
//            public void onComplete(@NonNull Task<String> task) {
//                if(!task.isSuccessful()){
//                    Log.e("Firebase","fetch token failed");
//                }else{
//                    Log.e("Token",task.getResult());
//                }
//            }
//        });
    }


    public void login(String account, String password, String friend){
        new Thread(()->{
//            Log.e("test",etAccount.getText().toString());
            try{
                URL url = new URL("http://192.168.43.171/11.29/login.php");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setUseCaches(false);

                List<Pair<String,String>> params = new ArrayList<>();
                params.add(new Pair<>("account",account));
                params.add(new Pair<>("password",password));

                OutputStream outputStream = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));

                writer.write(getQuery(params));
                writer.flush();
                writer.close();
                outputStream.close();

                connection.connect();

                if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                    InputStream inputStream = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));

                    if(reader.readLine().equals("OK")){
                        MainActivity.currentAccount = account;
                        MainActivity.friend = friend;
                        startActivity(new Intent(this, MainActivity2.class));
                    }else if(reader.readLine().equals("ERROR")){
                        runOnUiThread(()-> Toast.makeText(this, "帳號密碼錯誤", Toast.LENGTH_SHORT).show());
                    }
                }else{
                    Toast.makeText(this, "錯誤", Toast.LENGTH_SHORT).show();
                }
            }catch (IOException e){
                e.printStackTrace();
            }

        }).start();
    }

    public static String getQuery(List<Pair<String,String>> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Pair<String,String> pair : params){
            if(first){
                first = false;
            }else{
                result.append("&");
            }
            result.append(URLEncoder.encode(pair.first, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second, "UTF-8"));

        }
        return result.toString();
    }
}