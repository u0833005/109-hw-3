package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        ListView lvOutput=findViewById(R.id.lvOutput);
        EditText etMsg=findViewById(R.id.etMsg);
        Button btnSend=findViewById(R.id.btnSend);

        ArrayList<String> dataArray=new ArrayList<>();

        ArrayAdapter<String> adapter=new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,dataArray);
        lvOutput.setAdapter(adapter);

        btnSend.setOnClickListener(view -> new Thread(() -> {
            try
            {
                URL url = new URL("http://" + MainActivity.serverIP + "/test/send_notification.php");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                HashMap<String,String> params=new HashMap<>();
                params.put("sender",getIntent().getStringExtra("account"));
                params.put("receiver",getIntent().getStringExtra("friend"));
                params.put("content",etMsg.getText().toString());
                OutputStreamWriter osWriter=new OutputStreamWriter(connection.getOutputStream());
                osWriter.write(MainActivity.getQuery(params));
                osWriter.flush();
                connection.connect();
                int statusCode=connection.getResponseCode();
                if(statusCode==HttpURLConnection.HTTP_OK)
                {
                    dataArray.add(getIntent().getStringExtra("account")+": "+etMsg.getText().toString());
                    runOnUiThread(adapter::notifyDataSetChanged);
                }
                else
                {
                    runOnUiThread(() -> Toast.makeText(MainActivity2.this,"訊息發送失敗",Toast.LENGTH_SHORT).show());
                }
                etMsg.setText("");
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }).start());

        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
            if(task.isSuccessful())
            {
                Log.e("Firebase",task.getResult());
                Toast.makeText(MainActivity2.this,task.getResult(),Toast.LENGTH_SHORT).show();
            }
            else
                Log.e("Error","Firebase fetch failed");
        });
        BroadcastReceiver broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.hasExtra("msg"))
                {
                    dataArray.add(getIntent().getStringExtra("friend")+": "+intent.getStringExtra("msg"));
                    adapter.notifyDataSetChanged();
                }
            }
        };
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,new IntentFilter("Message"));
    }
}