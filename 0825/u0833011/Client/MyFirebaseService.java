package com.example.myapplication;

import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        if(remoteMessage.getNotification()!=null)
        {
            Log.e("title",remoteMessage.getNotification().getTitle());
            Log.e("body",remoteMessage.getNotification().getBody());
            notifyNewMessage(remoteMessage.getNotification().getBody());
        }
        super.onMessageReceived(remoteMessage);
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
    }

    public void notifyNewMessage(String message)
    {
        Intent intent=new Intent("Message");
        intent.putExtra("msg",message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
