package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    public static String serverIP="192.168.1.12";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> Log.e("token",task.getResult()));

        EditText etAccount=findViewById(R.id.etAccount);
        EditText etPassword=findViewById(R.id.etPassword);
        EditText etFriend=findViewById(R.id.etFriend);
        Button btnLogin=findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(view -> new Thread(() -> {
            try
            {
                URL url = new URL("http://"+serverIP+"/test/login.php");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                HashMap<String,String> params=new HashMap<>();
                params.put("account",etAccount.getText().toString());
                params.put("password",etPassword.getText().toString());
                OutputStreamWriter osWriter=new OutputStreamWriter(connection.getOutputStream());
                osWriter.write(getQuery(params));
                osWriter.flush();
                connection.connect();
                int statusCode=connection.getResponseCode();
                if(statusCode==HttpURLConnection.HTTP_OK)
                {
                    InputStream is=connection.getInputStream();
                    BufferedReader br=new BufferedReader(new InputStreamReader(is));
                    String result=br.readLine(); //1 or 0
                    if(result.equals("1"))
                    {
                        Intent intent=new Intent(this,MainActivity2.class);
                        intent.putExtra("account",etAccount.getText().toString());
                        intent.putExtra("friend",etFriend.getText().toString());
                        startActivity(intent);
                    }
                    else
                        runOnUiThread(() -> Toast.makeText(MainActivity.this,"帳號或密碼錯誤，請重新輸入",Toast.LENGTH_SHORT).show());
                }

            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }).start());
    }
    public static String getQuery(HashMap<String,String> map)
    {
        StringBuilder builder=new StringBuilder();
        map.forEach((key,value)->{
            try {
                builder.append("&").append(key).append("=").append(URLEncoder.encode(value, "UTF-8"));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });
        return builder.toString();
    }
}