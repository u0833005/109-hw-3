package com.example.chiayuan0810;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class second_layout extends AppCompatActivity {
    Button Main;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_layout);

        Button Main = findViewById(R.id.Main);
        Main.setOnClickListener(view -> {
            //協助應用間的交互與
            Intent intent = new Intent();
            intent.setClass(second_layout.this,MainActivity.class);
            startActivity(intent);
        });
    }
}