package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager manager=getSupportFragmentManager();
        BottomNavigationView bt_navigation=(BottomNavigationView) findViewById(R.id.bt_navigation);
        bt_navigation.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int layoutId=R.layout.fragment_home;
                switch(item.getItemId())
                {
                    case R.id.btnSearch:
                        layoutId=R.layout.fragment_search;
                        break;
                    case R.id.btnHome:
                        layoutId=R.layout.fragment_home;
                        break;
                    case R.id.btnSettings:
                        layoutId=R.layout.fragment_settings;
                        break;
                }
                displayFragment(layoutId,manager);
                return true;
            }
        });
        bt_navigation.setSelectedItemId(R.id.btnHome);
        displayFragment(R.layout.fragment_home,manager);
    }
    private void displayFragment(int layoutId,FragmentManager manager)
    {
        FragmentTransaction transaction=manager.beginTransaction();
        transaction.replace(R.id.frag_container,new Fragment(layoutId));
        transaction.commit();
    }
}