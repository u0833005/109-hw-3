package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class myPageAdapter extends FragmentStateAdapter
{
    private final int tabCount;
    public myPageAdapter(@NonNull FragmentActivity fragmentActivity,int tabCount) {
        super(fragmentActivity);
        this.tabCount=tabCount;
    }
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        Fragment myFragment=null;
        switch(position)
        {
            case 0:
                myFragment=new myFragment(R.layout.tab1);
                break;
            case 1:
                myFragment=new myFragment(R.layout.tab2);
                break;
            case 2:
                myFragment=new myFragment(R.layout.tab3);
                break;
        }
        return myFragment;
    }
    @Override
    public int getItemCount() {
        return tabCount;
    }
}
