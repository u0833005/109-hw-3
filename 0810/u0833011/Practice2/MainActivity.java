package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabLayout tabLayout=(TabLayout) findViewById(R.id.tabLayout);
        ViewPager2 viewPager2=(ViewPager2) findViewById(R.id.viewPager2);
        TabItem tab1=(TabItem) findViewById(R.id.tab1);
        TabItem tab2=(TabItem) findViewById(R.id.tab2);
        TabItem tab3=(TabItem) findViewById(R.id.tab3);
        myPageAdapter pageAdapter=new myPageAdapter(this,tabLayout.getTabCount());
        viewPager2.setAdapter(pageAdapter);
        TabLayoutMediator myMediator=new TabLayoutMediator(tabLayout,viewPager2,true,new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText("TAB"+(position+1));
            }
        });
        myMediator.attach();
    }
}