package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class myFragment extends Fragment
{
    public myFragment(int layoutId)
    {
        super(layoutId);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button btn1=(Button) view.findViewById(R.id.button1);
        Button btn2=(Button) view.findViewById(R.id.button2);
        Button btn3=(Button) view.findViewById(R.id.button3);
        Button btnSecond=(Button) view.findViewById(R.id.btnSecond);
        btn1.setOnClickListener(view1 -> ((MainActivity)getActivity()).setFragment(0));
        btn2.setOnClickListener(view1 -> ((MainActivity)getActivity()).setFragment(1));
        btn3.setOnClickListener(view1 -> ((MainActivity)getActivity()).setFragment(2));
        btnSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toSecond=new Intent(getActivity(),second_layout.class);
                startActivity(toSecond);
            }
        });
    }
}
