package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private ViewPager2 container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myFragmentStateAdapter fragmentStateAdapter=new myFragmentStateAdapter(this);
        fragmentStateAdapter.addFragment(new myFragment(R.layout.fragment1));
        fragmentStateAdapter.addFragment(new myFragment(R.layout.fragment2));
        fragmentStateAdapter.addFragment(new myFragment(R.layout.fragment3));
        container=(ViewPager2) findViewById(R.id.container);
        container.setAdapter(fragmentStateAdapter);
    }
    public void setFragment(int index)
    {
        container.setCurrentItem(index);
    }
}