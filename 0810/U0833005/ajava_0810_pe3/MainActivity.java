package com.example.ajava_0810_pe3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView=findViewById(R.id.btn);
        bottomNavigationView.setOnNavigationItemSelectedListener(naListener);
    }
    BottomNavigationView.OnNavigationItemSelectedListener naListener=new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment selectfrag=null;
            switch (menuItem.getItemId()){
                case R.id.bt_home:
                    selectfrag=new home_fragment();
                    break;
                case R.id.bt_search:
                    selectfrag=new search_fragment();
                    break;
                case R.id.bt_setting:
                    selectfrag=new setting_fragment();
                    break;
            }
                    getSupportFragmentManager().beginTransaction().replace(R.id.frc,selectfrag).commit();
            return true;
        }
    };
}