package com.example.a0810p1;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.ArrayList;
import java.util.List;

public class SectionsStatesPagerAdapter extends FragmentStateAdapter{
    List<Fragment> mfragmentlist=new ArrayList<>();
    List<String> mfragmenttitlelist=new ArrayList<>();
    public SectionsStatesPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    public void addfragment(Fragment fragment,String title){
        mfragmentlist.add(fragment);
        mfragmenttitlelist.add(title);
    }
    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return mfragmentlist.get(position);
    }
    @Override

    public int getItemCount() {
        return mfragmentlist.size();
    }
}
