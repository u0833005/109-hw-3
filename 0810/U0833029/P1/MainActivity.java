package com.example.a0810p1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {
    SectionsStatesPagerAdapter sectionsStatesPagerAdapter;
    ViewPager2 viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sectionsStatesPagerAdapter=new SectionsStatesPagerAdapter(this);
        viewPager=findViewById(R.id.container);
        setupViewPager(viewPager);
    }
    private void setupViewPager(ViewPager2 viewPager){
        Log.e("setupViewPager","setupViewPager");
        SectionsStatesPagerAdapter adapter=new SectionsStatesPagerAdapter(this);
        adapter.addfragment(new fragment1(),"fragment1");
        adapter.addfragment(new fragment2(),"fragment2");
        adapter.addfragment(new fragment3(),"fragment3");
        viewPager.setAdapter(adapter);
    }
    public void setViewPager(int fragmentNumber){
        Log.e("setViewPager","fragmentNumber");
        viewPager.setCurrentItem(fragmentNumber);
    }
}