package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SqlDatabaseHelper sqlDB=new SqlDatabaseHelper(this);

        EditText etName=findViewById(R.id.etName);
        EditText etPhone=findViewById(R.id.etPhone);
        Button btnAdd=findViewById(R.id.btnAdd);
        Button btnDelAll=findViewById(R.id.btnDelAll);
        Button btnQueryAll=findViewById(R.id.btnQueryAll);
        TextView tvOutput=findViewById(R.id.tvOutput);

        btnAdd.setOnClickListener(view -> {
            String name=etName.getText().toString();
            String phone=etPhone.getText().toString();
            etName.setText("");
            etPhone.setText("");
            sqlDB.insertData(name,phone);
            Toast.makeText(MainActivity.this,"插入成功",Toast.LENGTH_SHORT).show();
        });
        btnDelAll.setOnClickListener(view -> {
            sqlDB.deleteAllData();
            Toast.makeText(MainActivity.this,"刪除成功",Toast.LENGTH_SHORT).show();
        });
        btnQueryAll.setOnClickListener(view -> {
            ArrayList<String> dataArray= sqlDB.queryData();
            StringBuilder temp= new StringBuilder();
            for(String data : dataArray)
                temp.append(data).append("\n");
            tvOutput.setText(temp.toString());
        });

    }
}