package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class SqlDatabaseHelper extends SQLiteOpenHelper {

    static public String tableName="membersData";
    static public int version=1;

    private SQLiteDatabase db_handler;

    public SqlDatabaseHelper(@Nullable Context context) {
        super(context, tableName, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql="CREATE TABLE "+tableName+"( id INTEGER PRIMARY KEY autoincrement NOT NULL , name TEXT, phoneNum TEXT)";
        sqLiteDatabase.execSQL(sql);
        db_handler=sqLiteDatabase;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void insertData(String name,String phoneNum)
    {
        db_handler=getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put("name",name);
        cv.put("phoneNum",phoneNum);
        db_handler.insert(tableName,null,cv);
    }

    public void deleteAllData()
    {
        db_handler=getWritableDatabase();
        String sql="DELETE FROM "+tableName;
        db_handler.execSQL(sql);
    }

    public ArrayList<String> queryData()
    {
        ArrayList<String> data=new ArrayList<>();
        String temp;
        db_handler=getReadableDatabase();
        Cursor cursor=db_handler.rawQuery("SELECT * FROM "+tableName,null);
        cursor.moveToFirst();
        for(int i=0;i<cursor.getCount();i++)
        {
            int id= cursor.getInt(0);
            String name=cursor.getString(1);
            String phoneNum=cursor.getString(2);
            temp="ID: "+ id +", NAME: "+name+", Phone: "+phoneNum;
            data.add(temp);
            cursor.moveToNext();
        }
        cursor.close();
        return data;
    }
}
