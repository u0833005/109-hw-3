package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText etAccount=findViewById(R.id.etAccount);
        EditText etPassword=findViewById(R.id.etPassword);
        Button btnStore=findViewById(R.id.btnGet);
        Button btnJump=findViewById(R.id.btnJump);

        SharedPreferences sharedPreferences=getSharedPreferences("data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        btnStore.setOnClickListener(view -> {
            String account=etAccount.getText().toString();
            String password=etPassword.getText().toString();
            editor.putString("account",account);
            editor.putString("password",password);
            editor.apply();
        });
        btnJump.setOnClickListener(view -> {
            Intent intent=new Intent(MainActivity.this,MainActivity2.class);
            startActivity(intent);
        });
    }
}