package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        EditText etAccount=findViewById(R.id.etAccount);
        EditText etPassword=findViewById(R.id.etPassword);
        Button btnGet=findViewById(R.id.btnGet);

        SharedPreferences sharedPreferences=getSharedPreferences("data", Context.MODE_PRIVATE);

        btnGet.setOnClickListener(view -> {
            String account=sharedPreferences.getString("account","");
            String password=sharedPreferences.getString("password","");
            etAccount.setText(account);
            etPassword.setText(password);
        });

    }
}