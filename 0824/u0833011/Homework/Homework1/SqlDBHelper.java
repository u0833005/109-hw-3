package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;

public class SqlDBHelper extends SQLiteOpenHelper {

    private SQLiteDatabase db_handler;

    public SqlDBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql="CREATE TABLE "+getDatabaseName()+"( id INTEGER PRIMARY KEY autoincrement NOT NULL , name TEXT, num INTEGER, price INTEGER)";
        sqLiteDatabase.execSQL(sql);
        db_handler=sqLiteDatabase;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void insertData(String name,int num,int price)
    {
        db_handler=getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put("name",name);
        cv.put("num",num);
        cv.put("price",price);
        db_handler.insert(getDatabaseName(),null,cv);
    }

    public void deleteData(int id)
    {
        db_handler=getWritableDatabase();
        String[] whereArgs={String.valueOf(id)};
        db_handler.delete(getDatabaseName(),"id=?",whereArgs);
    }

    public void updateData(int id,String name,int num,int price)
    {
        db_handler=getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put("name",name);
        cv.put("num",num);
        cv.put("price",price);
        String[] whereArgs={String.valueOf(id)};
        db_handler.update(getDatabaseName(),cv,"id=?",whereArgs);
    }

    public HashMap<String,Object> querySingle(int id)
    {
        db_handler=getReadableDatabase();
        String[] selectionArgs={String.valueOf(id)};
        Cursor cursor=db_handler.query(getDatabaseName(),null,"id=?",selectionArgs,null,null,null);
        cursor.moveToFirst();
        HashMap<String,Object> result=new HashMap<>();
        result.put("name",cursor.getString(1));
        result.put("num",cursor.getInt(2));
        result.put("price",cursor.getInt(3));
        cursor.close();
        return result;
    }

    public ArrayList<String> queryAllData()
    {
        ArrayList<String> data=new ArrayList<>();
        String temp;
        db_handler=getReadableDatabase();
        Cursor cursor=db_handler.rawQuery("SELECT * FROM "+getDatabaseName(),null);
        cursor.moveToFirst();
        for(int i=0;i<cursor.getCount();i++)
        {
            int id= cursor.getInt(0);
            String name=cursor.getString(1);
            int num=cursor.getInt(2);
            int price=cursor.getInt(3);
            temp="ID: "+ id +"\n產品名稱: "+name+"\n數量: "+num+"\n價錢: "+price;
            data.add(temp);
            cursor.moveToNext();
        }
        cursor.close();
        return data;
    }
}
