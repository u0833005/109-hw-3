package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SqlDBHelper sqlDBHelper=new SqlDBHelper(this,"productsData",null,1);

        EditText etID=findViewById(R.id.etID);
        EditText etProductName=findViewById(R.id.etProductName);
        EditText etNum=findViewById(R.id.etNum);
        EditText etPrice=findViewById(R.id.etPrice);
        Button btnAdd=findViewById(R.id.btnAdd);
        Button btnDel=findViewById(R.id.btnDel);
        Button btnUpdate=findViewById(R.id.btnUpdate);
        Button btnQuery=findViewById(R.id.btnQuery);
        ListView lvOutput=findViewById(R.id.lvOutput);

        btnAdd.setOnClickListener(view -> {
            String name=etProductName.getText().toString();
            int num=Integer.parseInt(etNum.getText().toString());
            int price=Integer.parseInt(etPrice.getText().toString());
            sqlDBHelper.insertData(name,num,price);
            Toast.makeText(MainActivity.this,"新增成功",Toast.LENGTH_SHORT).show();
            ArrayList<String> data=sqlDBHelper.queryAllData();
            lvOutput.setAdapter(new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1,data));
        });
        btnDel.setOnClickListener(view -> {
            int id=Integer.parseInt(etID.getText().toString());
            sqlDBHelper.deleteData(id);
            Toast.makeText(MainActivity.this,"刪除成功",Toast.LENGTH_SHORT).show();
            ArrayList<String> data=sqlDBHelper.queryAllData();
            lvOutput.setAdapter(new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1,data));
        });
        btnUpdate.setOnClickListener(view -> {
            int id=Integer.parseInt(etID.getText().toString());
            String name=etProductName.getText().toString();
            int num=Integer.parseInt(etNum.getText().toString());
            int price=Integer.parseInt(etPrice.getText().toString());
            sqlDBHelper.updateData(id,name,num,price);
            Toast.makeText(MainActivity.this,"修改成功",Toast.LENGTH_SHORT).show();
            ArrayList<String> data=sqlDBHelper.queryAllData();
            lvOutput.setAdapter(new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1,data));
        });
        btnQuery.setOnClickListener(view -> {
            int id=Integer.parseInt(etID.getText().toString());
            HashMap<String,Object> result=sqlDBHelper.querySingle(id);
            etProductName.setText((String) result.get("name"));
            etNum.setText(String.valueOf(result.get("num")));
            etPrice.setText(String.valueOf(result.get("price")));
        });
        ArrayList<String> data=sqlDBHelper.queryAllData();
        lvOutput.setAdapter(new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1,data));
    }

}