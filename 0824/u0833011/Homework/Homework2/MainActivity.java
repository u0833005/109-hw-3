package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final String serverIp="192.168.1.12";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText etName=findViewById(R.id.etName);
        EditText etAccount=findViewById(R.id.etAccount);
        EditText etPassword=findViewById(R.id.etPassword);
        Button btnSubmit=findViewById(R.id.btnSubmit);
        Button btnLoad=findViewById(R.id.btnLoad);
        ListView lvOutput=findViewById(R.id.lvOutput);

        Handler mainHandler=new Handler(Looper.myLooper())
        {
            @SuppressWarnings("unchecked")
            @Override
            public void handleMessage(@NonNull Message msg) {
                switch (msg.what)
                {
                    case 0:
                        Toast.makeText(MainActivity.this,(String) msg.obj,Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        ArrayAdapter<String> adapter=new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1,(ArrayList<String>) msg.obj);
                        lvOutput.setAdapter(adapter);
                        break;
                    default:
                        super.handleMessage(msg);
                }
            }
        };

        btnSubmit.setOnClickListener(view -> {
            String name=etName.getText().toString();
            String account=etAccount.getText().toString();
            String password=etPassword.getText().toString();
            new Thread(() -> {
                try
                {
                    URL url=new URL("http://"+serverIp+"/test/account.php");
                    HttpURLConnection connection=(HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("POST");
                    connection.setDoOutput(true);
                    connection.setUseCaches(false);
                    connection.connect();
                    OutputStreamWriter osWriter=new OutputStreamWriter(connection.getOutputStream());
                    String data="name="+URLEncoder.encode(name,"UTF-8")+"&account="+URLEncoder.encode(account,"UTF-8")+"&password="+URLEncoder.encode(password,"UTF-8");
                    osWriter.write(data);
                    osWriter.flush();
                    int statusCode=connection.getResponseCode();
                    if(statusCode==HttpURLConnection.HTTP_OK)
                    {
                        InputStream is=connection.getInputStream();
                        BufferedReader br=new BufferedReader(new InputStreamReader(is));
                        String result=br.readLine();
                        mainHandler.sendMessage(mainHandler.obtainMessage(0,result));
                        System.out.println(result);
                    }
                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                }
            }).start();
        });
        btnLoad.setOnClickListener(view -> new Thread(() -> {
            try
            {
                URL url=new URL("http://"+serverIp+"/test/account.php");
                HttpURLConnection connection=(HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.setUseCaches(false);
                connection.connect();
                int statusCode=connection.getResponseCode();
                if(statusCode==HttpURLConnection.HTTP_OK)
                {
                    InputStream is=connection.getInputStream();
                    BufferedReader br=new BufferedReader(new InputStreamReader(is));
                    String result=br.readLine();
                    ArrayList<String> data=new ArrayList<>();
                    JSONArray jsonArray=new JSONArray(result);
                    for(int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject object=jsonArray.getJSONObject(i);
                        String temp="姓名: "+object.getString("name")+
                                "\n帳號: "+object.getString("account")+
                                "\n密碼: "+object.getString("password");
                        data.add(temp);
                    }
                    mainHandler.sendMessage(mainHandler.obtainMessage(1,data));
                }
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
            }
        }).start());
    }
}