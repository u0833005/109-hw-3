package com.example.a0824_3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class MainActivity extends AppCompatActivity {
    TextView t1,t2,t3;
    EditText e1,e2,e3;
    Button btn1,btn2;

    public void setView(){
        t1 = findViewById(R.id.t1);
        t2 = findViewById(R.id.t2);
        t3 = findViewById(R.id.t3);

        e1 = findViewById(R.id.e1);
        e2 = findViewById(R.id.e2);
        e3 = findViewById(R.id.e3);

        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setView();



        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = e1.getText().toString();
                String account = e2.getText().toString();
                String password = e3.getText().toString();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            URL url = new URL("http://10.0.2.2/8.24/insert.php");

                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                            connection.setRequestMethod("POST");
                            connection.setDoOutput(true);
                            connection.setDoInput(true);
                            connection.setUseCaches(false);
                            connection.connect();
                            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                            String data = "";

                            data += "&" + URLEncoder.encode("name","UTF-8") + "=" + URLEncoder.encode(name,"UTF-8")
                                    + "&" + URLEncoder.encode("account","UTF-8") + "=" + URLEncoder.encode(account,"UTF-8")
                                    + "&" +URLEncoder.encode("password","UTF-8") + "=" + URLEncoder.encode(password,"UTF-8");
                            Log.e("data",data);
                            wr.write(data);
                            wr.flush();
                            int responseCode = connection.getResponseCode();
                            Log.e("post response code",connection.getResponseCode()+" ");
                        } catch (Exception e){
                            Log.e("connect",e.getMessage());
                        }
                    }
                }).start();
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            URL url = new URL("http://10.0.2.2/8.24/getData.php");

                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                            connection.setRequestMethod("POST");
                            connection.setDoOutput(true);
                            connection.setDoInput(true);
                            connection.setUseCaches(false);
                            connection.connect();
                            int responseCode = connection.getResponseCode();

                            if(responseCode == HttpURLConnection.HTTP_OK){
                                InputStream inputStream = connection.getInputStream();
                                BufferedReader bufReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8),8);
                                String result = bufReader.readLine();
                                Log.i("result",result);
                            }
                        } catch (Exception e){
                            Log.e("e",e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
    }
}