import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.net.ServerSocket;
import java.net.Socket;

public class homeworkServer
{
    public static void main(String[] args)
    {
        try
        {
            ServerSocket server=new ServerSocket(1234);
            System.out.println("Start listening on port "+server.getLocalPort());
            Socket client=server.accept();
            System.out.println("Connect Success!");
            DataInputStream dis=new DataInputStream(client.getInputStream());
            DataOutputStream dos=new DataOutputStream(client.getOutputStream());
            String question=generateNum();
            System.out.println("real answer:"+question);
            while(true)
            {
                String userAnswer=dis.readUTF();
                if(userAnswer.length()!=4)
                {
                    dos.writeUTF("Please enter 4 digit number");
                    continue;
                }
                if(!checkAnswer(userAnswer))
                {
                    dos.writeUTF("Please enter 4 unique number");
                    continue;
                }
                int a=0,b=0;
                for(int i=0;i<4;i++)
                {
                    for(int j=0;j<4;j++)
                    {
                        if(question.charAt(i)==userAnswer.charAt(j))
                        {
                            if(i==j)
                                a++;
                            else
                                b++;
                        }
                    }
                }
                if(a==4)
                {
                    dos.writeUTF("4A! Congratulation! New Game Create!");
                    question=generateNum();
                    System.out.println("real answer:"+question);
                }
                else
                    dos.writeUTF(a+"A"+b+"B:) Continue to Guess!");
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    public static String generateNum()
    {
        String output="";
        Set<Integer> set=new HashSet<>();
        Random random=new Random();
        while(set.size()!=4)
            set.add(random.nextInt(10));
        for(Integer i:set)
            output+=i;
        return output;
    }
    public static boolean checkAnswer(String answer)
    {
        String[] tokens=answer.split("");
        Set<String> set=new HashSet<>();
        for(String token:tokens)
            set.add(token);
        return (set.size()==4)?true:false;
    }
}
