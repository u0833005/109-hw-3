import java.net.Socket;
import java.util.Scanner;
import java.net.ServerSocket;
import java.io.DataInputStream;
import java.io.DataOutputStream;

public class practiceServer
{
    public static void main(String[] args)
    {
        try
        {
            ServerSocket server=new ServerSocket(1234);
            Scanner myScanner=new Scanner(System.in);
            System.out.println("Start listening on port "+server.getLocalPort());
            Socket client=server.accept();
            System.out.println("Connect Success!");
            DataInputStream dis=new DataInputStream(client.getInputStream());
            DataOutputStream dos=new DataOutputStream(client.getOutputStream());
            Thread myThread=new Thread(
                new Runnable()
                {
                    public void run()
                    {
                        while(true)
                        {
                            try
                            {
                                System.out.println("Client: "+dis.readUTF());
                            }
                            catch(Exception e)
                            {
                                System.out.println(e.getMessage());
                                break;
                            }
                        }
                    }
                });
            myThread.start();
            while(true)
            {
                String line=myScanner.nextLine();
                dos.writeUTF(line);
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
