import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

public class practiceClient
{
    public static void main(String[] args) throws Exception
    {
        Socket client=new Socket("localhost",1234);
        Scanner myScanner=new Scanner(System.in);
        DataInputStream dis=new DataInputStream(client.getInputStream());
        DataOutputStream dos=new DataOutputStream(client.getOutputStream());
        Thread myThread=new Thread(
            new Runnable()
            {
                public void run()
                {
                    while(true)
                    {
                        try
                        {
                            System.out.println("Server: "+dis.readUTF());
                        }
                        catch(Exception e)
                        {
                            System.out.println(e.getMessage());
                            break;
                        }
                    }
                }
            });
        myThread.start();
        while(true)
        {
            String line=myScanner.nextLine();
            dos.writeUTF(line);
        }
    }
}