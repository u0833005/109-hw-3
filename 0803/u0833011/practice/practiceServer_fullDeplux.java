import java.util.List;
import java.util.ArrayList;
import java.net.Socket;
import java.net.ServerSocket;
import java.io.DataInputStream;
import java.io.DataOutputStream;

public class practiceServer_fullDeplux
{
    public static void main(String[] args)
    {
        try
        {
            ServerSocket server=new ServerSocket(1234);
            System.out.println("Start listening on port "+server.getLocalPort());
            List<Socket> clients=new ArrayList<>();
            while(true)
            {
                Socket client=server.accept();
                System.out.println("New client enter!");
                synchronized(clients)
                {
                    clients.add(client);
                }
                DataInputStream dis=new DataInputStream(client.getInputStream());
                Thread myThread=new Thread(new Runnable(){
                    public void run()
                    {
                        while(true)
                        {
                            String line;
                            try
                            {
                                line=dis.readUTF();
                            }
                            catch(Exception e)
                            {
                                System.out.println(e.getMessage());
                                clients.remove(client);
                                break;
                            }
                            broadcast(clients,line);
                        }
                    }
                });
                myThread.start();
            }
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
    public static void broadcast(List<Socket> clients,String message)
    {
        synchronized(clients)
        {
            for(Socket client:clients)
            {
                try
                {
                    DataOutputStream dos=new DataOutputStream(client.getOutputStream());
                    dos.writeUTF(message);
                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                }
            }
        }
    }
}
