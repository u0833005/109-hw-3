import java.net.Socket;
import java.util.Scanner;
import java.io.DataInputStream;
import java.io.DataOutputStream;

public class practiceClient_fullDeplux
{
    public static void main(String[] args)
    {
        try
        {
            Socket client=new Socket("localhost",1234);
            Scanner myScanner=new Scanner(System.in);
            System.out.print("Please enter your username: ");
            String userName=myScanner.nextLine();
            DataInputStream dis=new DataInputStream(client.getInputStream());
            DataOutputStream dos=new DataOutputStream(client.getOutputStream());
            Thread myThread=new Thread(
            new Runnable()
            {
                public void run()
                {
                    while(true)
                    {
                        try
                        {
                            System.out.println(dis.readUTF());
                        }
                        catch(Exception e)
                        {
                            System.out.println(e.getMessage());
                            break;
                        }
                    }
                }
            });
        myThread.start();
        String line="";
        while(line!="end")
        {
            line=myScanner.nextLine();
            dos.writeUTF(userName+": "+line);
        }
        myScanner.close();
        dos.close();
        dis.close();
        client.close();
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
