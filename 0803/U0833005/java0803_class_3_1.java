import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class java0803_class_3_1 {

    static ServerSocket serverSocket;
    static Socket socket;
    public static void main(String[] args) {
        try {
            serverSocket = new ServerSocket(2205);
            System.out.println("server open");
            socket = serverSocket.accept();
            System.out.println("client touch");

            Sender sender = new Sender(socket);
            Receiver receiver = new Receiver("Server",socket);
            sender.start();
            receiver.start();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public static class Sender extends Thread{
        public DataOutputStream dos;
        public Socket socket;

        public Sender(Socket socket){
            this.socket=socket;
        }
        @Override
        public void run(){
            try {
                dos = new DataOutputStream(socket.getOutputStream());
                Scanner sc = new Scanner(System.in);
                while (socket.isConnected()){
                    String output = sc.nextLine();
                    dos.writeUTF(output);
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
    public static class Receiver extends Thread{
        public String name;
        public DataInputStream dis;
        public Socket socket;
        public Receiver(String name,Socket socket){
            this.name = name;
            this.socket = socket;
        }
        @Override
        public void run(){
            try {
                dis = new DataInputStream(socket.getInputStream());

                while (socket.isConnected()){
                    System.out.println(this.name+":"+dis.readUTF());
                }

            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
