import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.Random;
import java.util.ArrayList;

public class java0803_class_2_1 {

    static int ran(int val){
        Random ran1 = new Random();
        int ans1 = val/1000;
        int ans2 = val/100-(val/1000*10);
        int ans3 = val/10-(val/100*10);
        int ans4 = val-(val/10*10);
        if(ans1==ans2||ans1==ans3||ans1==ans4||ans2==ans3||ans2==ans4||ans3==ans4){
            int as = ran1.nextInt(8999)+1000;
            return ran(as);
        }
        else{
            return val;
        }
    }

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(3000);
        Socket socket;
        socket = serverSocket.accept();

        DataInputStream dis = new DataInputStream(socket.getInputStream());
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

        Random ran1 = new Random();
        int as = ran1.nextInt(8999)+1000;
        int ans = ran(as);
        int ans1 = ans/1000;
        int ans2 = ans/100-(ans/1000*10);
        int ans3 = ans/10-(ans/100*10);
        int ans4 = ans-(ans/10*10);
        dos.writeInt(ans);

        int open =0;
        while (open == 0) {

            int gt = dis.readInt();
            int gt1 = gt/1000;
            int gt2 = gt/100-(gt/1000*10);
            int gt3 = gt/10-(gt/100*10);
            int gt4 = gt-(gt/10*10);
            int B=0;
            if(ans1==gt2||ans1==gt3||ans1==gt4){
                B+=1;
            }
            if(ans2==gt1||ans2==gt3||ans2==gt4){
                B+=1;
            }
            if(ans3==gt1||ans3==gt2||ans3==gt4){
                B+=1;
            }
            if(ans4==gt1||ans4==gt2||ans4==gt3){
                B+=1;
            }
            int A=0;
            if(ans1==gt1){
                A+=1;
            }
            if(ans2==gt2){
                A+=1;
            }
            if(ans3==gt3){
                A+=1;
            }
            if(ans4==gt4){
                A+=1;
            }
            dos.writeUTF(A+"A,"+B+"B");
            /*if(A==4){
                dos.writeUTF("恭喜答對,遊戲結束");
            }*/
        }
    }
}
