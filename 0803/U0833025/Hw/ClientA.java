import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class ClientA {
    static Socket socket;
    static String str;
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) throws IOException {
        try{
            socket = new Socket(InetAddress.getByName("localhost"),2000);
            Send s = new Send();
            s.start();
            Connect c = new Connect();
            c.start();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public static class Send extends Thread{
        @Override
        public void run(){
            try{
                DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
                while(true){
                    str = sc.next();
                    outputStream.writeUTF(str);
                    if(str.equals("exit")){
                        System.out.println("BYE!!!!");
                        break;
                    }
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
    public static class Connect extends Thread{
        //@Override
        public void run(){
            try {
                DataInputStream inputStream = new DataInputStream(socket.getInputStream());
                while(true){
                    String receive = inputStream.readUTF();
                    System.out.println(receive);
                }
            }catch (IOException e){
                e.printStackTrace();
            }

        }

    }
}
