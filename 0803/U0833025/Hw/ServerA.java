import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
//DataOutputStream outstream = new DataOutputStream(socket.getOutputStream());

public class ServerA {
    static ServerSocket serverSocket;
    static Socket sockets;
    static List<String> Name = new ArrayList<>();   //存姓名
    static List<Receive> receives = new ArrayList<>();  //存聊天紀錄
    public static void main(String[] args) {
        try{
            serverSocket = new ServerSocket(2000);
            System.out.println("Sever connection");

            while(true){
                sockets = serverSocket.accept();
                System.out.println("Client connection "+sockets.getInetAddress().getHostAddress());
                DataInputStream instream = new DataInputStream(sockets.getInputStream());

                String name = instream.readUTF();
                Name.add(name);
                Receive r = new Receive(name, sockets);

                receives.add(r);
                r.start();

                for(int i = 0; i < Name.size(); i++){
                    if(Name.get(i).equals(name)){
                        System.out.println(name+"JOIN!!!!");
                        receives.get(i).outstream.writeUTF("Welcome!!!!"+name);
                    }else{
                        receives.get(i).outstream.writeUTF(name+" Join!!!!");
                    }
                }
            }

        } catch (IOException e){
            e.printStackTrace();
        }


    }

    public static class Receive extends Thread{
        public DataOutputStream outstream;
        public String name;
        public Socket socket;


        public Receive(String n, Socket sockets)throws IOException{
            name = n;
            socket = sockets;
            outstream = new DataOutputStream(sockets.getOutputStream());
        }
        @Override
        public void run(){
            try{
                while(true){
                    DataInputStream instream = new DataInputStream(sockets.getInputStream());

                    String received = instream.readUTF();
                    if(received.equals("exit")){
                        for(int i = 0; i < Name.size(); i++){
                            if(Name.get(i).equals(name)){
                                System.out.println("離開");
                            }else{
                                receives.get(i).outstream.writeUTF(name+"離開");
                            }
                        }
                        Name.remove(name);
                        receives.remove(this);
                    }else{
                        for(int i = 0; i < Name.size(); i++) {
                            if (Name.get(i).equals(name)) {
                                System.out.println(name + "=>message:" + received);
                            } else {
                                receives.get(i).outstream.writeUTF(name + "=>message:" + received);

                            }
                        }
                    }
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
