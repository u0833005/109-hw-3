import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class AllClient {
    static Socket socket;
    public static void main(String[] args) throws IOException {
        try{
            socket = new Socket("localhost",3000);
            System.out.println("connect Server");
            Sender send = new Sender(socket);
            Receiver receive = new Receiver("Server",socket);
            send.start();
            receive.start();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public static class Sender extends Thread{
        public DataOutputStream dos;
        public Socket socket;

        public Sender(Socket socket){
            this.socket = socket;
        }

        @Override
        public void run(){
            try{
                dos = new DataOutputStream(socket.getOutputStream());
                Scanner sc = new Scanner(System.in);

                while(socket.isConnected()){
                    String output = sc.next();
                    dos.writeUTF(output);
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
    public static class Receiver extends Thread{
        public String name;
        public DataInputStream dis;
        public Socket socket;

        public Receiver(String name, Socket socket){
            this.name = name;
            this.socket = socket;
        }
        @Override
        public void run(){

            try{
                dis = new DataInputStream(socket.getInputStream());

                while(socket.isConnected()){
                    System.out.println(this.name+":"+dis.readUTF());
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }
}
