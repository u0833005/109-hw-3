import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
//Client端-------------------------------------------------------------------
public class Main {

    public static void main(String[] args) throws IOException {

//        Socket socket = new Socket("www.google.com",80);
//        System.out.println("本地IP位址:"+socket.getLocalAddress());
//        System.out.println("本地Port:"+socket.getLocalPort());
//        System.out.println("遠端IP位址:"+socket.getInetAddress());
//        System.out.println("遠端Port:"+socket.getPort());

        Socket socket = new Socket("127.0.0.1",3000);

        DataInputStream dis = new DataInputStream(socket.getInputStream());
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

        dos.writeUTF("Hello, Server");  //UTF=>字串
        System.out.println(dis.readUTF());

        dis.close();
        dos.close();
        socket.close();
    }
}
