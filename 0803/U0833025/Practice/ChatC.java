import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class ChatC {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1",3000);

        DataInputStream dis = new DataInputStream(socket.getInputStream());
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

        Scanner sc = new Scanner(System.in);

        boolean isStarted = true;

        while (isStarted){
//            System.out.println("請輸入:");
            System.out.println("Server:"+dis.readUTF());
            dos.writeUTF(sc.next());
        }
    }
}
