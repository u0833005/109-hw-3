import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GameServer {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(3000);
        Socket socket;

        socket = serverSocket.accept();

        DataInputStream dis = new DataInputStream(socket.getInputStream());
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());


        List<Integer> computer = new ArrayList<Integer>();
        for (int i = 0; i < 4; i++) {
            int random = (int) (Math.random() * 9 + 1);
            if (!computer.contains(random)) {
                computer.add(random);
            } else {
                i--;
            }
        }
        System.out.println(computer);
        dos.writeUTF("請輸入不重複四數:");
        Scanner sc = new Scanner(System.in);

        boolean isPlayed = true;

        while (isPlayed) {
            String player = dis.readUTF();
            int a = 0, b = 0;
            if (player.length() == 4) {
                for (int m = 0; m < 4; m++) {
                    for (int n = 0; n < 4; n++) {
                        if (String.valueOf(player.charAt(m)).equals(computer.get(n).toString())) {
                            b++;
                            if (m == n) {
                                b--;
                                a++;
                            }
                        }
                    }
                }
                dos.writeUTF(a + "A" + b + "B");
                if (a == 4) {
                    System.out.println("END");
                    dos.close();
                    dis.close();
                    socket.close();
                    break;
                }
            } else {
                dos.writeUTF("請輸入有效數字");
            }
        }
    }
}