import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class GameClient {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1",3000);

        DataInputStream dis = new DataInputStream(socket.getInputStream());
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());



        Scanner sc = new Scanner(System.in);

        boolean isPlayed = true;

        while (isPlayed){
            String answer = dis.readUTF();
            System.out.println(answer);
            if(answer.charAt(0)==4){
                dos.close();
                dis.close();
                socket.close();
            }else{
                dos.writeUTF(sc.next());
            }
        }
    }
}
