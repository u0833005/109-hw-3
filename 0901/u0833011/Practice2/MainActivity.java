package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private BluetoothLeAdvertiser advertiser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnBtBroadcast=findViewById(R.id.btnBtBroadcast);

        BluetoothManager manager=(BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter adapter=manager.getAdapter();

        btnBtBroadcast.setOnClickListener(view -> {
            if(adapter==null)
                Toast.makeText(MainActivity.this,"Device doesn't support Bluetooth.",Toast.LENGTH_SHORT).show();
            else if(!adapter.isEnabled())
                Toast.makeText(MainActivity.this,"Bluetooth is not enabled.",Toast.LENGTH_SHORT).show();
            else
            {
                advertiser=adapter.getBluetoothLeAdvertiser();
                AdvertiseSettings.Builder settingsBuilder=new AdvertiseSettings.Builder();
                settingsBuilder
                        .setConnectable(true)
                        .setTimeout(0)
                        .setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY)
                        .setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH);
                AdvertiseSettings settings=settingsBuilder.build();
                AdvertiseData.Builder dataBuilder=new AdvertiseData.Builder();
                dataBuilder
                        .setIncludeDeviceName(true)
                        .setIncludeTxPowerLevel(true);
                AdvertiseData data=dataBuilder.build();
                adapter.setName("Test");
                advertiser.startAdvertising(settings, data, new AdvertiseCallback() {
                    @Override
                    public void onStartSuccess(AdvertiseSettings settingsInEffect) {
                        super.onStartSuccess(settingsInEffect);
                        Log.d("Bluetooth","Advertisement added!");
                    }

                    @Override
                    public void onStartFailure(int errorCode) {
                        super.onStartFailure(errorCode);
                        Log.e("Bluetooth","Failed to add advertisement.");
                    }
                });
            }
        });
    }
}