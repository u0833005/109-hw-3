package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    static class LvItem
    {
        TextView tvName;
        TextView tvMAC;
        TextView tvRssi;
    }
    private final ArrayList<BluetoothDevice> devicesArray=new ArrayList<>();
    private final ArrayList<Integer> devicesRssi=new ArrayList<>();
    private final BaseAdapter adapter=new BaseAdapter() {
        @Override
        public int getCount() {
            return devicesArray.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LvItem viewHolder;
            if(view==null)
            {
                view=View.inflate(MainActivity.this,R.layout.lvitem,null);
                viewHolder=new LvItem();
                viewHolder.tvName=view.findViewById(R.id.tvName);
                viewHolder.tvMAC=view.findViewById(R.id.tvMAC);
                viewHolder.tvRssi=view.findViewById(R.id.tvRssi);
                view.setTag(viewHolder);
            }
            else
                viewHolder=(LvItem) view.getTag();

            viewHolder.tvName.setText(devicesArray.get(i).getName());
            viewHolder.tvMAC.setText(devicesArray.get(i).getAddress());
            viewHolder.tvRssi.setText(String.valueOf(devicesRssi.get(i)));
            return view;
        }
    };

    private final BroadcastReceiver receiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch(intent.getAction())
            {
                case BluetoothDevice.ACTION_FOUND:
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    int rssi=intent.getShortExtra(BluetoothDevice.EXTRA_RSSI,(short) 1);
                    if(devicesArray.contains(device))
                        devicesRssi.set(devicesArray.indexOf(device),rssi);
                    else
                    {
                        devicesArray.add(device);
                        devicesRssi.add(rssi);
                    }
                    adapter.notifyDataSetChanged();
                    break;
                case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                    Toast.makeText(MainActivity.this,"Scan Starting.",Toast.LENGTH_SHORT).show();
                    break;
                case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                    Toast.makeText(MainActivity.this,"Finished",Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnOpenBT=findViewById(R.id.btnOpenBT);
        Button btnSearch=findViewById(R.id.btnSearch);
        ListView lvOutput=findViewById(R.id.lvOutput);
        lvOutput.setAdapter(adapter);

        BluetoothManager manager=(BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter adapter=manager.getAdapter();

        IntentFilter filter=new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        registerReceiver(receiver,filter);

        btnOpenBT.setOnClickListener(view -> {
            Intent intent=new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(intent);
        });
        btnSearch.setOnClickListener(view -> {
            if(adapter==null)
                Toast.makeText(MainActivity.this,"null",Toast.LENGTH_SHORT).show();
            else if(!adapter.isEnabled())
                Toast.makeText(MainActivity.this,"not enabled.",Toast.LENGTH_SHORT).show();
            else
                adapter.startDiscovery();
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}