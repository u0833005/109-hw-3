import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Hw01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in) ;
        Random rand = new Random();
        System.out.println("請輸入個數:");
         int num = sc.nextInt() ;
        int[] array = new int[num];
        for (int i = 0 ; i < num ; i++){
            array[i]= rand.nextInt(100);
        }
        System.out.println("排序前為:");
        for (Integer n : array) {
            System.out.print(n + " ");
        }
        System.out.println();
        for (int j = 0 ; j < num ; j++){
            for (int k = 0 ; k < num-1-j ; k++){
                if(array[k] > array[k+1]){
                    int temp = array[k];
                    array[k] = array [k+1];
                    array[k+1] = temp;
                }
            }
        }
        System.out.println("氣泡排序後為:");
        for (Integer n : array) {
            System.out.print(n + " ");
        }
        System.out.println();

        //隨機，排序

        System.out.println("請輸入個數:");
        int num2 = sc.nextInt() ;
        Random rand2 = new Random();
        int[] array2 = new int[num2];
        for (int i = 0 ; i < num2 ; i++){
            array2[i]= rand.nextInt(9)+1;
        }
        System.out.println("排序前為:");
        for (Integer n : array2) {
            System.out.print(n + " ");
        }
        System.out.println();
        int a=0,b=0,c=0,d=0,e=0,f=0,g=0,h=0,i=0;
        //int na=0,nb=0,nc=0,nd=0,ne=0,nf=0,ng=0,nh=0,ni=0;
        for (int m = 0 ; m < num2 ; m++) {
            switch (array2[m]) {
                case 1:
                    a ++;
                    break;
                case 2:
                    b ++;
                    break;
                case 3:
                    c ++;
                    break;
                case 4:
                    d ++;
                    break;
                case 5:
                    e ++;
                    break;
                case 6:
                    f ++;
                    break;
                case 7:
                    g ++;
                    break;
                case 8:
                    h ++;
                    break;
                case 9:
                    i ++;
                    break;
            }
        }int na=1*a;int nb=2*b;int nc=3*c;int nd=4*d;int ne=5*e;int nf=6*f;int ng=7*g;int nh=8*h;int ni=9*i;

        System.out.println("1有:"+a+"個，2有:"+b+"個，3有:"+c+"個，4有:"+d+"個，5有:"+e+"個，6有:"+f+"個，7有:"+g+"個，8有:"+h+"個，9有:"+i+"個");
        System.out.println("1=>"+na+"，2=>"+nb+"，3=>"+nc+"，4=>"+nd+"，5=>"+ne+"，6=>"+nf+"，7=>"+ng+"，8=>"+nh+"，9=>"+ni);

        //隨機，計數

        List<Integer> array3 = new ArrayList<Integer>();
        System.out.println("請輸入範圍:");
        int num3 = sc.nextInt() ;
        for (int m = 0; m<num3 ;m++){
            array3.add(m);
        }
        for (Integer n3 : array3) {
            System.out.print(n3+" ");
        }
        System.out.println();
        for (int l = 0 ; l < array3.size() ; l++){
            if(array3.get(l)%4 == 0){
                if(array3.get(l) !=0) {
                    array3.remove(l);
                }
            }
        }
        for (Integer n3 : array3) {
            System.out.print(n3+" ");
        }
        System.out.println();

        //刪除4倍數

        List<Integer> array4 = new ArrayList<Integer>();
        System.out.println("隨機產生=>");
        int num4=(int)(Math.random()*10);
        while(num4 !=0){
            array4.add(num4);
            num4= (int)(Math.random()*10);
        }
        System.out.print(array4);
        System.out.println();
        int sum = 0;
        for (Integer s : array4) {
            sum+=s;
        }
        System.out.println("總和為:"+sum);
    }
}
