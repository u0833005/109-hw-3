import java.util.Scanner;

public class AB {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("電腦=>");
        int[] arrayC = new int[4];
        for (int i = 0 ; i < 4 ; i++){
            arrayC[i]=(int)(Math.random()*9+1);
            if(i > 0){
                for (int j = 0 ; j < i ; j++) {
                    while (arrayC[i]==arrayC[j]){
                        arrayC[i]=(int)(Math.random()*9+1);
                    }
                }
            }
        }
        for (int i : arrayC) {
            System.out.print(i+" ");
        }
        System.out.println();
        boolean isPlayed = true;

        while(isPlayed) {
            System.out.println("玩家=>");
            int ansP = sc.nextInt();
            int[] arrayP = new int[4];

            arrayP[0] = ansP / 1000;
            arrayP[1] = (ansP % 1000)/100;
            arrayP[2] = ((ansP % 1000)%100)/10;
            arrayP[3] = ansP % 10;
            if(ansP > 9999 || ansP < 1023 ||
                arrayP[0] == arrayP[1] || arrayP[0] == arrayP[2] || arrayP[0] == arrayP[3] ||
                    arrayP[1] == arrayP[2] || arrayP[1] == arrayP[3])
            {
                System.out.println("請輸入有效數字");
            }else{
                int a = 0,b = 0;
                for( int m = 0 ; m < 4 ;m++){
                    for ( int n = 0 ; n < 4 ;n++){
                        if(arrayC[m]==arrayP[n]&& m==n){
                            a++;
                        }else if (arrayC[m]==arrayP[n]){
                            b++;
                        }
                    }
                }
                System.out.println(a+"A"+b+"B");
                if(a==4){
                    System.out.println("恭喜猜對");
                    isPlayed=false;
                }
            }
        }
    }
}
