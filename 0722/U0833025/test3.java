import java.util.Scanner;

public class test3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("請輸入數字：");
        int num = sc.nextInt();
        if (num % 2 == 0) {
            //String str=sc.next()+"此為偶數";
            System.out.println(num+"為偶數");
        } else {
            //String str=sc.next()+"此為奇數";
            System.out.println(num+"為奇數");
        }

       System.out.println("請輸入年份以及月份:");
                int year = sc.nextInt();
                int month =sc.nextInt();
                if(year%4==0||year%400==0){
                    if(year%100!=0) System.out.println(year+"年為閏年");
                    if(month==2){
                        System.out.println(month+"月有29天");
                    }else if(month==4 || month==6 || month==9 || month==11){
                        System.out.println(month+"月有30天");
                    }else{
                        System.out.println(month+"月有31天");
                    }
                }else{
                    System.out.println(year+"年為平年");
                    if(month==2){
                        System.out.println(month+"月有28天");
                    }else if(month==4||month==6||month==9||month==11){
                        System.out.println(month+"月有30天");
                    }else{
                        System.out.println(month+"月有31天");
                    }
                }

       System.out.println("請輸入成績：");
        int score = sc.nextInt() /10;
        switch (score) {
            case 10:
                System.out.println("A");
                break;
            case 9:
                System.out.println("B");
                break;
            case 8:
                System.out.println("C");
                break;
            case 7:
                System.out.println("D");
                break;
            case 6:
                System.out.println("E");
                break;
            default:
                System.out.println("F");
        }
        System.out.println("請輸入時間；");
        int str = sc.nextInt();
        int pm=str-12;
        String a = "下午"+pm+"點";
        String b = "上午"+str+"點";
        System.out.println(str>12?a:b);

    }
}