import java.util.Scanner;

public class practice2
{
    public static void main(String[] args)
    {
        Scanner myscanner=new Scanner(System.in);
        //Question 1
        System.out.println("請輸入數字");
        int myinteger=myscanner.nextInt();
        System.out.print("您輸入的"+myinteger+"為");
        System.out.println(((myinteger%2)==0)?"偶數":"奇數");

        System.out.println();

        //Question 2
        System.out.println("請輸入年份");
        int myyear=myscanner.nextInt();
        System.out.println("請輸入月份");
        int mymonth=myscanner.nextInt();
        int day;
        boolean isLeapYear;
        /*if(myyear%4 < 1)
        {
            if(myyear%100<1)
            {
                if(myyear%400<1)
                {
                    isLeapYear=true;
                }
                else
                    isLeapYear=false;
            }
            else
                isLeapYear=true;
        }
        else
            isLeapYear=false;*/
        if((myyear%4)==0 && (myyear%100)!=0 || (myyear%400)==0)
            isLeapYear=true;
        else
            isLeapYear=false;
        if(mymonth<=7)
        {
            if(mymonth==2)
                day=(isLeapYear)?29:28;
            else
                day=(mymonth%2==0)?30:31;
        }
        else //mymonth>=8
        {
            day=(mymonth%2==0)?31:30;
        }
        System.out.println(myyear+"年"+mymonth+"月有"+day+"天");
        //Question 3
        System.out.println("請輸入成績");
        int mygrade=myscanner.nextInt();
        String message;
        switch(mygrade/10)
        {
            case 10:
            case 9:
            case 8:
                message="A";
                break;
            case 7:
                message="B";
                break;
            case 6:
                message="C";
                break;
            case 5:
                message="D";
                break;
            default:
                message="Failed";
        }
        System.out.println(message);
        //Question 4
        System.out.println("請輸入時間");
        int mytime=myscanner.nextInt();
        System.out.println((mytime>12)?"下午"+(mytime-12)+"點":"早上"+mytime+"點");
        myscanner.close();
    }
}
