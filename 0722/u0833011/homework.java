import java.util.Scanner;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;

public class homework
{
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        //Question1
        System.out.println("Question1\n請輸入隨機數個數");
        int myRandomNum = myScanner.nextInt();
        ArrayList<Integer> myList = new ArrayList<Integer>();
        for (int i = 0; i < myRandomNum; i++)
            myList.add((int)(Math.random() * 100));
        myList.sort(Comparator.naturalOrder());
        System.out.println(myList);
        //Question2
        System.out.println("Question2");
        ArrayList<Integer> myList2= new ArrayList<Integer>();
        for(int i=0;i<999;i++)
            myList2.add((int)(Math.random()*9)+1);
        System.out.println(myList2);

        ArrayList<Integer> myCounter=new ArrayList<Integer>();
        for(int i=0;i<9;i++)
            myCounter.add(0);
        //System.out.println(myCounter);
        for(int i=0;i<myList2.size();i++)
        {
            int previous=myCounter.get(myList2.get(i).intValue()-1).intValue();
            myCounter.set(myList2.get(i).intValue()-1,Integer.valueOf(previous+1));
        }
        System.out.println(myCounter);
        for(int i=0;i<myCounter.size();i++)
            System.out.println("所有"+(i+1)+"的總和為"+(myCounter.get(i).intValue()*(i+1)));
        //Question3
        System.out.println("Question3");
        String question="123456789";
        for(int i=0;i<question.length();i++)
        {
            char[] temp=question.toCharArray();
            char tempChar=temp[i];
            int random=(int)(Math.random()*question.length());
            temp[i]=temp[random];
            temp[random]=tempChar;
            question=String.valueOf(temp);
        }
        question=question.substring(0,4);
        System.out.println(question);
        boolean isWin=false;
        while(!isWin)
        {
            String userAns=myScanner.next();
            int a=0,b=0;
            for(int i=0;i<question.length();i++)
            {
                for(int j=0;j<question.length();j++)
                {
                    if(question.charAt(i)==userAns.charAt(j))
                    {
                        if(i==j)
                            a++;
                        else
                            b++;
                    }
                }
            }
            System.out.println(a+"A"+b+"B");
            if(a==4)
            {
                System.out.println("恭喜猜對");
                isWin=true;
            }
        }
        //Question4
        System.out.println("Question4");
        List<Integer> myList4=new ArrayList<Integer>();
        for(int i=0;i<100;i++)
            myList4.add((int)(Math.random()*100)+1);
        System.out.println(myList4);
        /*for(int i=0;i<myList4.size();i++)
        {
            if(myList4.get(i)%4==0)
            {
                myList4.remove(i);
                i--;
            }
        }*/
        for(int i=myList4.size()-1;i>=0;i--)
        {
            if(myList4.get(i)%4==0)
                myList4.remove(i);
        }
        System.out.println(myList4);
        //Question5
        System.out.println("Question5");
        List<Integer> myList5=new ArrayList<Integer>();
        int randomNum=(int)(Math.random()*100);
        while(randomNum!=0)
        {
            myList5.add(randomNum);
            randomNum=(int)(Math.random()*100);
        }
        System.out.println(myList5);
        int sum=0;
        for(Integer i:myList5)
            sum+=i.intValue();
        System.out.println("總和為"+sum);
        myScanner.close();
    }
}
